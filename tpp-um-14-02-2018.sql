/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : tpp-um

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2018-02-14 06:13:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(16) NOT NULL AUTO_INCREMENT,
  `admin_nm` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `repassword` varchar(64) NOT NULL,
  `position` varchar(16) NOT NULL,
  `login` datetime DEFAULT NULL,
  `logout` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'admin@admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin', '2017-11-22 15:29:33', '2017-11-20 09:26:25');

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `user_agent` varchar(192) NOT NULL,
  `last_activity` int(10) NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  KEY `ci_sessions_timestamp` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('c3dd144867679e1ef3a82d79dad0a895', '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36', '1518562993', 'a:8:{s:9:\"user_data\";s:0:\"\";s:10:\"currentURL\";s:41:\"admin/to_reviewer/administrators_reviewer\";s:7:\"backURL\";s:29:\"admin/to_reviewer/save_review\";s:5:\"email\";s:22:\"bonekberdasi@gmail.com\";s:4:\"role\";s:1:\"1\";s:7:\"id_user\";s:1:\"6\";s:8:\"position\";s:1:\"1\";s:24:\"flash:old:successMessage\";s:24:\"Data berhasil disimpan !\";}');
INSERT INTO `ci_sessions` VALUES ('1c3bc17c52042228c0acc7357b670fca', '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36', '1518561713', 'a:3:{s:9:\"user_data\";s:0:\"\";s:10:\"currentURL\";s:16:\"paper/submission\";s:22:\"flash:new:errorMessage\";s:24:\"Maaf, anda belum login !\";}');
INSERT INTO `ci_sessions` VALUES ('835f9f96dbf659118fafb33672f18916', '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36', '1518562266', 'a:8:{s:9:\"user_data\";s:0:\"\";s:10:\"currentURL\";s:30:\"paper/submission/show_detail/1\";s:7:\"backURL\";s:32:\"paper/submission/send_to_admin/1\";s:5:\"email\";s:23:\"adm.tppum2018@gmail.com\";s:4:\"role\";s:1:\"4\";s:7:\"id_user\";s:2:\"44\";s:8:\"position\";s:1:\"1\";s:24:\"flash:old:successMessage\";s:24:\"Data berhasil disimpan !\";}');

-- ----------------------------
-- Table structure for data_privilage
-- ----------------------------
DROP TABLE IF EXISTS `data_privilage`;
CREATE TABLE `data_privilage` (
  `no` int(16) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(16) NOT NULL,
  `privilage_id` varchar(16) NOT NULL,
  `status` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_privilage
-- ----------------------------
INSERT INTO `data_privilage` VALUES ('1', '3', '2', '1');
INSERT INTO `data_privilage` VALUES ('3', '9', '5', '1');
INSERT INTO `data_privilage` VALUES ('8', '41', '6', '1');
INSERT INTO `data_privilage` VALUES ('9', '42', '2', '1');
INSERT INTO `data_privilage` VALUES ('18', '6', '2', '1');
INSERT INTO `data_privilage` VALUES ('21', '44', '4', '1');
INSERT INTO `data_privilage` VALUES ('22', '44', '5', '1');
INSERT INTO `data_privilage` VALUES ('23', '44', '6', '1');

-- ----------------------------
-- Table structure for fakultas
-- ----------------------------
DROP TABLE IF EXISTS `fakultas`;
CREATE TABLE `fakultas` (
  `id_fakultas` int(5) NOT NULL,
  `fakultas` varchar(30) NOT NULL,
  PRIMARY KEY (`id_fakultas`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fakultas
-- ----------------------------
INSERT INTO `fakultas` VALUES ('1000', 'Fakultas Ilmu Pendidikan');
INSERT INTO `fakultas` VALUES ('2000', 'Fakultas Sastra');
INSERT INTO `fakultas` VALUES ('3000', 'Fakultas Matematika dan IPA');
INSERT INTO `fakultas` VALUES ('4000', 'Fakultas Ekonomi');
INSERT INTO `fakultas` VALUES ('5000', 'Fakultas Teknik');
INSERT INTO `fakultas` VALUES ('6000', 'Fakultas Ilmu Keolahrgaan');
INSERT INTO `fakultas` VALUES ('7000', 'Fakultas Ilmu Sosial');
INSERT INTO `fakultas` VALUES ('8000', 'Fakultas Pendidikan Psikologi');
INSERT INTO `fakultas` VALUES ('9000', 'PS. Jenjang Magister Interdisi');

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `id_gallery` int(16) NOT NULL AUTO_INCREMENT,
  `url_photo` text NOT NULL,
  `caption` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery
-- ----------------------------

-- ----------------------------
-- Table structure for jurusan
-- ----------------------------
DROP TABLE IF EXISTS `jurusan`;
CREATE TABLE `jurusan` (
  `id_jurusan` int(5) NOT NULL,
  `jurusan` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jurusan
-- ----------------------------
INSERT INTO `jurusan` VALUES ('1100', 'Jurusan Bimbingan dan Konseling');
INSERT INTO `jurusan` VALUES ('1200', 'Jurusan Teknologi Pendidikan');
INSERT INTO `jurusan` VALUES ('1300', 'Jurusan Administrasi Pendidikan');
INSERT INTO `jurusan` VALUES ('1400', 'Jurusan Pendidikan Luar Sekolah');
INSERT INTO `jurusan` VALUES ('1500', 'Jurusan Kependidikan Sekolah Dasar dan Prasekolah');
INSERT INTO `jurusan` VALUES ('1600', 'Jurusan Pendidikan Luar Biasa');
INSERT INTO `jurusan` VALUES ('2100', 'Jurusan Sastra Indonesia');
INSERT INTO `jurusan` VALUES ('2200', 'Jurusan Sastra Inggris');
INSERT INTO `jurusan` VALUES ('2300', 'Jurusan Sastra Arab');
INSERT INTO `jurusan` VALUES ('2400', 'Jurusan Sastra Jerman');
INSERT INTO `jurusan` VALUES ('2500', 'Jurusan Seni dan Desain');
INSERT INTO `jurusan` VALUES ('3100', 'Jurusan Matematika');
INSERT INTO `jurusan` VALUES ('3200', 'Jurusan Fisika');
INSERT INTO `jurusan` VALUES ('3300', 'Jurusan Kimia');
INSERT INTO `jurusan` VALUES ('3400', 'Jurusan Biologi');
INSERT INTO `jurusan` VALUES ('4100', 'Jurusan Manajemen');
INSERT INTO `jurusan` VALUES ('4200', 'Jurusan Akutansi');
INSERT INTO `jurusan` VALUES ('4300', 'Jurusan Ekonomi Pembangunan');
INSERT INTO `jurusan` VALUES ('5100', 'Jurusan Teknik Mesin');
INSERT INTO `jurusan` VALUES ('5200', 'Jurusan Teknik Sipil');
INSERT INTO `jurusan` VALUES ('5300', 'Jurusan Teknik Elektro');
INSERT INTO `jurusan` VALUES ('5400', 'Jurusan Teknik Industri');
INSERT INTO `jurusan` VALUES ('6100', 'Jurusan Pendidikan Jasmani dan Kesehatan');
INSERT INTO `jurusan` VALUES ('6200', 'Jurusan Pendidikan Kepelatihan Olahraga');
INSERT INTO `jurusan` VALUES ('6300', 'Jurusan Ilmu Keolahragaan');
INSERT INTO `jurusan` VALUES ('6400', 'Jurusan Ilmu Kesehatan Masyarakat');
INSERT INTO `jurusan` VALUES ('7100', 'Jurusan Hukum dan Kwarganegaraan');
INSERT INTO `jurusan` VALUES ('7200', 'Jurusan Geografi');
INSERT INTO `jurusan` VALUES ('7300', 'Jurusan Sejarah');
INSERT INTO `jurusan` VALUES ('7400', 'Jurusan Sosiologi');
INSERT INTO `jurusan` VALUES ('8100', 'Jurusan Psikologi');
INSERT INTO `jurusan` VALUES ('9100', 'Program Studi Pendidikan Dasar');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `id_submission` varchar(8) NOT NULL,
  `member_name` varchar(192) DEFAULT NULL,
  `member_affiliation` varchar(192) DEFAULT NULL,
  `member_email` varchar(192) DEFAULT NULL,
  `member_status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', '1', 'M Annafia O', 'Universitas Negeri Malang', 'bonekberdasi', '2');

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id_notifications` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  PRIMARY KEY (`id_notifications`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of notifications
-- ----------------------------

-- ----------------------------
-- Table structure for personal_data
-- ----------------------------
DROP TABLE IF EXISTS `personal_data`;
CREATE TABLE `personal_data` (
  `id_user` varchar(16) NOT NULL,
  `id_personal` varchar(32) DEFAULT NULL,
  `full_name` varchar(128) DEFAULT NULL,
  `id_fakultas` varchar(32) DEFAULT NULL,
  `id_jurusan` varchar(32) DEFAULT NULL,
  `id_prodi` varchar(32) DEFAULT NULL,
  `no_user` varchar(8) DEFAULT NULL,
  `address` text,
  `gender` tinyint(2) DEFAULT NULL,
  `phone_number` varchar(16) DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `position` tinyint(1) DEFAULT NULL,
  `url_photo` text,
  `proccess_time` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of personal_data
-- ----------------------------
INSERT INTO `personal_data` VALUES ('3', null, null, 'teknik', 'teknik', null, '24112017', 'asd', '1', '1234567890', '2017-11-24', '2', null, '0');
INSERT INTO `personal_data` VALUES ('40', '160533611412', 'qwewqerqer', '2000', '3100', '1503', '15012018', 'asd', '1', '085735619093', '2018-01-16', '1', null, '0');
INSERT INTO `personal_data` VALUES ('41', '123', 'junico dwi chandra', '4000', '1400', '1602', '15012018', 'qwe', '1', '085735619093', '2018-01-16', '1', null, '0');
INSERT INTO `personal_data` VALUES ('42', '12', 'adskjlsa', '6000', '4200', '5503', '27012018', 'ass', '2', '901992', '1997-11-30', '2', null, '0');
INSERT INTO `personal_data` VALUES ('43', '12', 'adskjlsa', '2000', '2300', '2301', '01022018', 'aaa', '2', '901992', '2000-12-29', '2', null, '0');
INSERT INTO `personal_data` VALUES ('44', '12', 'Moch Annafia Oktafian', '6000', '6100', '6101', '04022018', 'sfa', '2', '901992', '1996-11-29', '1', null, '1');
INSERT INTO `personal_data` VALUES ('6', '902192', 'Translator Annafia', '1000', '1200', '1103', '29112017', 'ffkalsdak', '2', '079797666', '2015-09-27', '1', '1517450961.jpg', '0');
INSERT INTO `personal_data` VALUES ('9', '160533', 'junico dwi chandra', 'teknik', 'teknik', null, '15122017', 'asd', '1', '123', '1997-12-08', '1', null, '0');

-- ----------------------------
-- Table structure for plagiarism
-- ----------------------------
DROP TABLE IF EXISTS `plagiarism`;
CREATE TABLE `plagiarism` (
  `id_plagiarism` int(16) NOT NULL AUTO_INCREMENT,
  `id_submission` varchar(16) NOT NULL,
  `id_user` varchar(16) NOT NULL,
  `comment` text,
  `persen_plag` int(11) DEFAULT NULL,
  `fp_file` varchar(255) DEFAULT NULL,
  `plagiarism_file` varchar(255) DEFAULT NULL,
  `status_plag` tinyint(2) NOT NULL DEFAULT '0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `finish` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_plagiarism`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of plagiarism
-- ----------------------------
INSERT INTO `plagiarism` VALUES ('1', '1', '44', 'Plagiasi Rendah', '10', 'fp_1_Moch_Annafia_Oktafian.docx', 'plag1_1_Moch_Annafia_Oktafian.docx', '1', '2018-02-11 07:28:52', '2018-02-11 07:29:45', '0');
INSERT INTO `plagiarism` VALUES ('2', '1', '44', 'Plagiasi rendah', '10', 'fp_rev1_1_Moch_Annafia_Oktafian.docx', 'plag2_1_Moch_Annafia_Oktafian.docx', '1', '2018-02-11 11:22:30', '2018-02-11 11:23:28', '1');

-- ----------------------------
-- Table structure for privilage
-- ----------------------------
DROP TABLE IF EXISTS `privilage`;
CREATE TABLE `privilage` (
  `privilage_id` int(16) NOT NULL AUTO_INCREMENT,
  `privilage_nm` varchar(32) NOT NULL,
  PRIMARY KEY (`privilage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of privilage
-- ----------------------------
INSERT INTO `privilage` VALUES ('1', 'superuser');
INSERT INTO `privilage` VALUES ('2', 'admin');
INSERT INTO `privilage` VALUES ('3', 'author');
INSERT INTO `privilage` VALUES ('4', 'check plagiarism');
INSERT INTO `privilage` VALUES ('5', 'reviewer');
INSERT INTO `privilage` VALUES ('6', 'translator');

-- ----------------------------
-- Table structure for prodi
-- ----------------------------
DROP TABLE IF EXISTS `prodi`;
CREATE TABLE `prodi` (
  `id_prodi` int(5) NOT NULL,
  `program_studi` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prodi
-- ----------------------------
INSERT INTO `prodi` VALUES ('1101', 'Program Studi S1 Bimbingan dan Konseling');
INSERT INTO `prodi` VALUES ('1102', 'Program Studi S2 Bimbingan dan Konseling');
INSERT INTO `prodi` VALUES ('1103', 'Program Studi S3 Bimbingan dan Konseling');
INSERT INTO `prodi` VALUES ('1201', 'Program Studi S1 Teknologi Pendidikan');
INSERT INTO `prodi` VALUES ('1202', 'Program Studi S2 Teknologi Pembelajaran');
INSERT INTO `prodi` VALUES ('1203', 'Program Studi S3 Teknologi Pembelajaran');
INSERT INTO `prodi` VALUES ('1301', 'Program Studi S1 Administrasi Pendidikan');
INSERT INTO `prodi` VALUES ('1302', 'Program Studi S2 Manajemen Pendidikan');
INSERT INTO `prodi` VALUES ('1303', 'Program Studi S3 Manajemen Pendidikan');
INSERT INTO `prodi` VALUES ('1401', 'Program Studi S1 Pendidikan Luar Sekolah');
INSERT INTO `prodi` VALUES ('1402', 'Program Studi S2 Pendidikan Luar Sekolah');
INSERT INTO `prodi` VALUES ('1403', 'Program Studi S3 Pendidikan Luar Sekolah');
INSERT INTO `prodi` VALUES ('1501', 'Program Studi S1 Pendidikan Guru Sekolah Dasar');
INSERT INTO `prodi` VALUES ('1502', 'Program Studi S1 Pendidikan Guru Pendidikan Anak Usia Dini');
INSERT INTO `prodi` VALUES ('1503', 'Program Studi S2 Pendidikan Dasar');
INSERT INTO `prodi` VALUES ('1504', 'Program Studi S2 Pendidikan Anak Usia Dini');
INSERT INTO `prodi` VALUES ('1601', 'Program Studi S1 Pendidikan Luar Biasa');
INSERT INTO `prodi` VALUES ('1602', 'Program Studi S2 Pendidikan Khusus');
INSERT INTO `prodi` VALUES ('2101', 'Program Studi S1 Pendidikan Bahasa Sastra Indonesia dan Daerah');
INSERT INTO `prodi` VALUES ('2102', 'Program Studi S1 Bahasa dan Sastra Indonesia');
INSERT INTO `prodi` VALUES ('2103', 'Program Studi S2 Pendidikan Bahasa Indonesia');
INSERT INTO `prodi` VALUES ('2104', 'Program Studi S3 Pendidikan Bahasa Indonesia');
INSERT INTO `prodi` VALUES ('2105', 'Program Studi S2 Keguruan Bahasa');
INSERT INTO `prodi` VALUES ('2106', 'Program Studi S1 Ilmu Perpustakaan');
INSERT INTO `prodi` VALUES ('2107', 'Program Studi D3 Perpustakaan');
INSERT INTO `prodi` VALUES ('2201', 'Program Studi S1 Pendidikan Bahasa Inggris');
INSERT INTO `prodi` VALUES ('2202', 'Program Studi S1 Bahasa dan Sastra Inggris');
INSERT INTO `prodi` VALUES ('2203', 'Program Studi S2 Pendidikan Bahasa Inggris');
INSERT INTO `prodi` VALUES ('2204', 'Program Studi S3 Pendidikan Bahasa Inggris');
INSERT INTO `prodi` VALUES ('2301', 'Program Studi S1 Pendidikan Bahasa Arab');
INSERT INTO `prodi` VALUES ('2302', 'Program Studi S2 Pendidikan Bahasa Arab');
INSERT INTO `prodi` VALUES ('2401', 'Program Studi S1 Pendidikan Bahasa Jerman');
INSERT INTO `prodi` VALUES ('2402', 'Program Studi S1 Pendidikan Bahasa Mandarin');
INSERT INTO `prodi` VALUES ('2501', 'Program Studi S1 Pendidikan Seni Rupa');
INSERT INTO `prodi` VALUES ('2502', 'Program Studi S2 Keguruan Seni Rupa');
INSERT INTO `prodi` VALUES ('2503', 'Program Studi S1 Pendidikan Seni Tari dan Musik');
INSERT INTO `prodi` VALUES ('2504', 'Program Studi S1 Desain Komunikasi Visual');
INSERT INTO `prodi` VALUES ('2505', 'Program Studi D3 Game Animasi');
INSERT INTO `prodi` VALUES ('3101', 'Program Studi S1 Pendidikan Matematika');
INSERT INTO `prodi` VALUES ('3102', 'Program Studi S1 Matematika');
INSERT INTO `prodi` VALUES ('3103', 'Program Studi S2 Pendidikan Matematika');
INSERT INTO `prodi` VALUES ('3104', 'Program Studi S3 Pendidikan Matematika');
INSERT INTO `prodi` VALUES ('3201', 'Program Studi S1 Pendidikan Fisika');
INSERT INTO `prodi` VALUES ('3202', 'Program Studi S1 Fisika');
INSERT INTO `prodi` VALUES ('3203', 'Program Studi S2 Pendidikan Fisika');
INSERT INTO `prodi` VALUES ('3204', 'Program Studi S2 Fisika');
INSERT INTO `prodi` VALUES ('3301', 'Program Studi S1 Pendidikan Kimia');
INSERT INTO `prodi` VALUES ('3302', 'Program Studi S1 Kimia');
INSERT INTO `prodi` VALUES ('3303', 'Program Studi S2 Pendidikan Kimia');
INSERT INTO `prodi` VALUES ('3304', 'Program Studi S3 Pendidikan Kimia');
INSERT INTO `prodi` VALUES ('3401', 'Program Studi S1 Pendidikan Biologi');
INSERT INTO `prodi` VALUES ('3402', 'Program Studi S1 Biologi');
INSERT INTO `prodi` VALUES ('3403', 'Program Studi S2 Biologi');
INSERT INTO `prodi` VALUES ('3404', 'Program Studi S2 Pendidikan Biologi ');
INSERT INTO `prodi` VALUES ('3405', 'Program Studi S3 Pendidikan Biologi');
INSERT INTO `prodi` VALUES ('3001', 'Program Studi S1 Pendidikan Ilmu Pengetahuan Alam');
INSERT INTO `prodi` VALUES ('4101', 'Program Studi S1 Pendidikan Tata Niaga');
INSERT INTO `prodi` VALUES ('4102', 'Program Studi S1 Pendidikan Administrasi Perkantoran');
INSERT INTO `prodi` VALUES ('4103', 'Program Studi S1 Manajemen');
INSERT INTO `prodi` VALUES ('4104', 'Program Studi D3 Manajemen Pemasaran');
INSERT INTO `prodi` VALUES ('4105', 'Program Studi S2 Pendidikan Bisnis dan Manajemen');
INSERT INTO `prodi` VALUES ('4106', 'Program Studi S2 Manajemen');
INSERT INTO `prodi` VALUES ('4201', 'Program Studi S1 Pendidikan Akutansi');
INSERT INTO `prodi` VALUES ('4202', 'Program Studi S1 Akutansi');
INSERT INTO `prodi` VALUES ('4203', 'Program Studi S2 Akutansi');
INSERT INTO `prodi` VALUES ('4204', 'Program Studi D3 Akutansi');
INSERT INTO `prodi` VALUES ('4205', 'Program Profesi Akutansi');
INSERT INTO `prodi` VALUES ('4301', 'Program Studi S1 Pendidikan Ekonomi');
INSERT INTO `prodi` VALUES ('4302', 'Program Studi S1 Ekonomi dan Studi Pembangunan');
INSERT INTO `prodi` VALUES ('4303', 'Program Studi S2 Ilmu Ekonomi');
INSERT INTO `prodi` VALUES ('4304', 'Program Studi S2 Pendidikan Ekonomi');
INSERT INTO `prodi` VALUES ('4305', 'Program Studi S3 Pendidikan Ekonomi');
INSERT INTO `prodi` VALUES ('5101', 'Program Studi S1 Pendidikan Teknik Mesin');
INSERT INTO `prodi` VALUES ('5102', 'Program Studi S1 Pendidikan Teknik Otomotif');
INSERT INTO `prodi` VALUES ('5103', 'Program Studi S1 Teknik Mesin');
INSERT INTO `prodi` VALUES ('5104', 'Program Studi S2 Teknik Mesin');
INSERT INTO `prodi` VALUES ('5105', 'Program Studi D3 Teknik Mesin');
INSERT INTO `prodi` VALUES ('5106', 'Program Studi D3 Mesin Otomotif');
INSERT INTO `prodi` VALUES ('5107', 'Program Studi S1 Teknik Industri');
INSERT INTO `prodi` VALUES ('5201', 'Program Studi S1 Pendidikan Teknik Bangunan');
INSERT INTO `prodi` VALUES ('5202', 'Program Studi D3 Teknik Sipil dan Bangunan');
INSERT INTO `prodi` VALUES ('5203', 'Program Studi S1 Teknik Sipil');
INSERT INTO `prodi` VALUES ('5204', 'Program Studi S2 Teknik Sipil');
INSERT INTO `prodi` VALUES ('5301', 'Program Studi D3 Teknik Elektro');
INSERT INTO `prodi` VALUES ('5302', 'Program Studi D3 Teknik Elektronika');
INSERT INTO `prodi` VALUES ('5303', 'Program Studi S1 Pendidikan Teknik Informatika');
INSERT INTO `prodi` VALUES ('5304', 'Program Studi S1 Pendidikan Teknik Elektro');
INSERT INTO `prodi` VALUES ('5305', 'Program Studi S1 Teknik Elektro');
INSERT INTO `prodi` VALUES ('5306', 'Program Studi S1 Teknik Informatika');
INSERT INTO `prodi` VALUES ('5401', 'Program Studi D3 Tata Boga');
INSERT INTO `prodi` VALUES ('5402', 'Program Studi D3 Tata Busana');
INSERT INTO `prodi` VALUES ('5403', 'Program Studi S1 Pendidikan Tata Boga');
INSERT INTO `prodi` VALUES ('5405', 'Program Studi S1 Pendidikan Tata Busana');
INSERT INTO `prodi` VALUES ('5501', 'Konsentrasi Teknik Mesin');
INSERT INTO `prodi` VALUES ('5502', 'Konsentrasi Teknik Sipil dan Bangunan');
INSERT INTO `prodi` VALUES ('5503', 'Konsentrasi Teknik Elektro');
INSERT INTO `prodi` VALUES ('5504', 'Konsentrasi Teknik Informatika');
INSERT INTO `prodi` VALUES ('5505', 'Konsentrasi Tata Busana dan Tata Boga');
INSERT INTO `prodi` VALUES ('5001', 'Program Studi S3 Pendidikan Kejuruan');
INSERT INTO `prodi` VALUES ('6101', 'Program Studi S1 Pendidikan Jasmani dan Kesehatan');
INSERT INTO `prodi` VALUES ('6201', 'Program Studi S1 Pendidikan Kepelatihan Olahraga');
INSERT INTO `prodi` VALUES ('6301', 'Program Studi S1 Ilmu Keolahragaan');
INSERT INTO `prodi` VALUES ('6302', 'Program Studi S2 Pendidikan Olahraga');
INSERT INTO `prodi` VALUES ('6401', 'Program Studi S1 Ilmu Kesehatan Masyarakat');
INSERT INTO `prodi` VALUES ('7101', 'Program Studi S1 Pendidikan Pancasila dan Kewarganegaraan');
INSERT INTO `prodi` VALUES ('7102', 'Program Studi S2 Pendidikan Pancasila dan Kewarganegaraan');
INSERT INTO `prodi` VALUES ('7201', 'Program Studi S1 Geografi');
INSERT INTO `prodi` VALUES ('7202', 'Program Studi S1 Pendidikan Geografi');
INSERT INTO `prodi` VALUES ('7203', 'Program Studi S2 Pendidikan Geografi');
INSERT INTO `prodi` VALUES ('7204', 'Program Studi S3 Pendidikan Geografi');
INSERT INTO `prodi` VALUES ('7301', 'Program Studi S1 Pendidikan Sejarah');
INSERT INTO `prodi` VALUES ('7302', 'Program Studi S2 Pendidikan Sejarah');
INSERT INTO `prodi` VALUES ('7304', 'Program Studi S1 Ilmu Sejarah');
INSERT INTO `prodi` VALUES ('7401', 'Program Studi S1 Pendidikan Sosiologi');
INSERT INTO `prodi` VALUES ('7001', 'Program Studi S1 Pendidikan Ilmu Pengetahuan Sosial');
INSERT INTO `prodi` VALUES ('8101', 'Program Studi S1 Psikologi');
INSERT INTO `prodi` VALUES ('8102', 'Program Studi S3 Pendidikan Psikologi');
INSERT INTO `prodi` VALUES ('9101', 'Program Studi Pendidikan Dasar');

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `id_review` int(16) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(16) NOT NULL,
  `id_submission` varchar(16) NOT NULL,
  `fp_file` text,
  `review_file` text,
  `general_comment` text,
  `abstract_comment` text,
  `intro_comment` text,
  `methodology_comment` text,
  `result_comment` text,
  `discussion_comment` text,
  `reference` text,
  `other_comment` text,
  `originality` tinyint(1) DEFAULT NULL,
  `contribution_in_science` tinyint(1) DEFAULT NULL,
  `writing_technique` tinyint(1) DEFAULT NULL,
  `depth_of_research` tinyint(1) DEFAULT NULL,
  `novelty_reference` tinyint(1) DEFAULT NULL,
  `decission_status` tinyint(4) DEFAULT NULL,
  `status_review` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  `review_as` tinyint(4) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finish_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_review`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of review
-- ----------------------------
INSERT INTO `review` VALUES ('1', '9', '1', 'fp_rev1_1_Moch_Annafia_Oktafian.docx', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', '0', '1', '2018-02-12 14:12:14', '2018-02-13 23:01:50');
INSERT INTO `review` VALUES ('2', '44', '1', 'fp_rev1_1_Moch_Annafia_Oktafian.docx', 'review1Moch_Annafia_Oktafian_1_Moch_Annafia_Oktafian.doc', 'Bagus  Sekali', 'Bagus Sekali', 'Bagus Sekali', 'Bagus Sekali', 'Bagus Sekali', 'Bagus Sekali', 'Bagus Sekali', 'Bagus Sekali', '2', '3', '1', '1', '2', '2', '1', '0', '1', '2018-02-12 14:12:18', '2018-02-13 23:01:50');
INSERT INTO `review` VALUES ('3', '6', '1', 'fp_rev1_1_Moch_Annafia_Oktafian.docx', 'reviewAdm1_1_Moch_Annafia_Oktafian.doc', 'Bagus', 'Bagus', 'Bagus', 'Bagus', 'Bagus', 'Bagus', 'Bagus', 'Bagus', '2', '2', '1', '2', '2', '2', '1', '0', '2', '2018-02-12 14:12:22', '2018-02-13 23:01:50');
INSERT INTO `review` VALUES ('4', '44', '1', 'fp_rev2_1_Moch_Annafia_Oktafian.doc', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', '0', '1', '2018-02-14 00:37:28', '2018-02-14 06:03:13');
INSERT INTO `review` VALUES ('5', '6', '1', 'fp_rev2_1_Moch_Annafia_Oktafian.doc', 'reviewAdm2_1_Moch_Annafia_Oktafian.doc', 'Review Komentar UMUM', '', '', '', '', '', '', '', null, null, null, null, null, '1', '1', '0', '2', '2018-02-14 00:37:33', '2018-02-14 06:03:13');
INSERT INTO `review` VALUES ('6', '44', '1', 'fp_rev2_1_Moch_Annafia_Oktafian1.doc', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1', '0', '1', '2018-02-14 05:55:53', '2018-02-14 06:03:13');
INSERT INTO `review` VALUES ('7', '6', '1', 'fp_rev2_1_Moch_Annafia_Oktafian1.doc', 'reviewAdm3_1_Moch_Annafia_Oktafian.doc', 'sjldkfjkda', '', '', '', '', '', '', '', null, null, null, null, null, '2', '1', '0', '2', '2018-02-14 05:55:56', '2018-02-14 06:03:13');

-- ----------------------------
-- Table structure for revision
-- ----------------------------
DROP TABLE IF EXISTS `revision`;
CREATE TABLE `revision` (
  `id_revision` int(16) NOT NULL AUTO_INCREMENT,
  `id_submission` varchar(16) NOT NULL,
  `id_user` varchar(16) NOT NULL,
  `comment` text,
  `revision_file` text,
  `status_revision` tinyint(2) DEFAULT '0',
  `ntf` tinyint(2) NOT NULL DEFAULT '0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_revision`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of revision
-- ----------------------------

-- ----------------------------
-- Table structure for submission
-- ----------------------------
DROP TABLE IF EXISTS `submission`;
CREATE TABLE `submission` (
  `id_submission` int(16) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(16) NOT NULL,
  `id_topic` varchar(16) NOT NULL,
  `tittle` text,
  `abstract` text NOT NULL,
  `keyword` varchar(256) NOT NULL,
  `file` varchar(255) NOT NULL,
  `submitted_proof` text,
  `verified` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `status_plagiarism` tinyint(2) DEFAULT '0',
  `status_reviewer` tinyint(2) DEFAULT '0',
  `status_translator` tinyint(2) DEFAULT '0',
  `notif_plag` tinyint(2) NOT NULL DEFAULT '0',
  `notif_rev` tinyint(2) NOT NULL DEFAULT '0',
  `notif_trans` tinyint(4) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id_submission`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of submission
-- ----------------------------
INSERT INTO `submission` VALUES ('1', '44', '5', 'Sosialisasi pemrograman untuk khalayak umum', '<p>Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.</p>\r\n\r\n<p>Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.Silahkan isi abstrak anda di sini.</p>\r\n', 'this,is,my,keyword', 'fp_rev2_1_Moch_Annafia_Oktafian1.doc', null, '2', '0', '1', '1', '0', '0', '0', '0', '2018-02-14 05:54:38', '2018-02-11');

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(16) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time_expired` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_token`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES ('1', '44', 'QZn5a0hw9T', '2018-02-11 07:01:54', '99999999');
INSERT INTO `token` VALUES ('2', '44', '1QuW08WRgj', '2018-02-11 07:03:57', '99999999');
INSERT INTO `token` VALUES ('3', '44', 'y2ptJ7Wgzn', '2018-02-11 07:06:24', '99999999');
INSERT INTO `token` VALUES ('4', '44', '6txCwjngsP', '2018-02-11 07:08:23', '99999999');
INSERT INTO `token` VALUES ('5', '44', '9QNbCsbfMv', '2018-02-11 07:10:05', '99999999');
INSERT INTO `token` VALUES ('6', '44', 'ABMmjc3nPJ', '2018-02-11 07:25:42', '0');

-- ----------------------------
-- Table structure for topic
-- ----------------------------
DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `id_topic` int(16) NOT NULL AUTO_INCREMENT,
  `nm_topic` varchar(64) NOT NULL,
  PRIMARY KEY (`id_topic`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of topic
-- ----------------------------
INSERT INTO `topic` VALUES ('3', 'test');
INSERT INTO `topic` VALUES ('5', 'qwe 123');
INSERT INTO `topic` VALUES ('6', 'chemistry on the bottle');

-- ----------------------------
-- Table structure for translation
-- ----------------------------
DROP TABLE IF EXISTS `translation`;
CREATE TABLE `translation` (
  `id_translation` int(255) NOT NULL AUTO_INCREMENT,
  `id_submission` varchar(16) DEFAULT NULL,
  `id_user` varchar(16) DEFAULT NULL,
  `translation_file` text,
  `status_translation` tinyint(4) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_translation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of translation
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(16) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `repassword` varchar(128) NOT NULL,
  `auth_key` varchar(64) DEFAULT NULL,
  `role` tinyint(2) DEFAULT '4',
  `active` tinyint(4) DEFAULT '0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `login` datetime DEFAULT NULL,
  `logout` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('3', 'admin@admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '(NULL)', '1', '1', '2017-11-24 19:13:04', '2018-02-05 04:37:30', '2018-02-05 04:37:39');
INSERT INTO `user` VALUES ('6', 'bonekberdasi@gmail.com', '7558ac6f42ec0326353fe0e23047644d', 'juli2323', '(NULL)', '1', '1', '2017-11-29 06:49:41', '2018-02-14 05:42:30', '2018-02-08 04:39:32');
INSERT INTO `user` VALUES ('9', 'dwi.chandra@um.ac.id', '25d55ad283aa400af464c76d713c07ad', '12345678', '(NULL)', '2', '1', '2017-12-15 09:28:09', '2018-01-15 15:20:44', '2018-01-15 16:03:54');
INSERT INTO `user` VALUES ('40', 'tes@um.ac.id', '25d55ad283aa400af464c76d713c07ad', '12345678', null, '4', '1', '2018-01-15 12:37:30', '2018-01-15 16:53:53', '2018-01-15 15:02:35');
INSERT INTO `user` VALUES ('41', 'dosenasd@um.ac.id', '25d55ad283aa400af464c76d713c07ad', '12345678', null, '4', '1', '2018-01-15 12:44:06', null, null);
INSERT INTO `user` VALUES ('42', 'bonekberdasi@um.ac.id', 'a3dcb4d229de6fde0db5686dee47145d', 'asdasdasd', null, '4', '1', '2018-01-27 11:59:23', null, null);
INSERT INTO `user` VALUES ('43', 'b@um.ac.id', 'e219b56989281a7846dd836161d7a2bd', 'asasasas', null, '4', '1', '2018-02-01 08:54:24', null, null);
INSERT INTO `user` VALUES ('44', 'adm.tppum2018@gmail.com', '8d4ee0521b58a00b324e19dc6663d186', 'juli2324', null, '4', '1', '2018-02-04 07:51:27', '2018-02-14 05:52:04', '2018-02-11 06:18:15');
