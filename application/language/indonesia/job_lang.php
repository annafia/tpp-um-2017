<?php 

$lang = array(
	// BREADCRUMP
	'list' => 'Daftar Tugas',

	// DATA TABLE
	'email' => 'Email',
	'status' => 'Status',
	'judul' => 'Judul',
	'kerjakan' => 'KERJAKAN',
	'detail_karya' => 'Detail Karya',
	'pemeriksaan_selesai' => 'Pemeriksaan Telah Selesai',

	'tulis_revisi' => 'Tulis Revisi',
	'tulis_komentar' => 'Tulis Komentar',
	'tulis_abstrak' => 'Tulis Abstrak',
	'tulis_pengantar' => 'Tulis Pengantar',
	'tulis_metodologi' => 'Tulis Metodologi',
	'tulis_hasil' => 'Tulis Hasil',
	'tulis_diskusi' => 'Tulis Diskusi',
	'tulis_refrensi' => 'Tulis Refrensi',
	'tulis_lainnya' => 'Tulis Lainnya',
	'tulis_diskusi' => 'Tulis Diskusi',
	'tulis_putusan' => 'Tulis Putusan',

	'unggah_file_revisi' => 'Unggah File Revisi',
	'dibuat' => 'Dibuat pada',
	'tervalidasi' => 'Tervalidasi',
	'sudah_dikerjakan_rekan_anda' => 'SUDAH DIKERJAKAN REKAN ANDA',	
	'simpulan' => 'SIMPULAN KUALITAS KARYA',
	'pilih' => 'Pilih',
	'upload_file' => 'UPLOAD FILE REVISI',
	'bagian1' => 'Komentar per Bagian dari Manuskrip Bagian 1',
	'bagian2' => 'Komentar per Bagian dari Manuskrip Bagian 2',
	'bagian3' => 'Simpulan Kualitas Paper: (4 = Sangat Bagus) (3 = Bagus) (2 = Cukup) (1 =Jelek)',

	'file_harus_diisi' => 'Berkas hasil revisi tidak boleh kosong',
	'harus_docx' => 'Format file harus .docx',
	'detail' => 'DETAIL',

	// PETUNJUK
	'tombol_selesai' => 'Tombol selesai digunakan untuk menyatakan status karya lolos tahap plagiasi.',
	'tombol_revisi' => 'Tombol revisi digunakan untuk menyatakan status karya mempunyai plagiasi yang tinggi dan beri masukan kepada Author untuk memperbaikinya.',
	'isi_semua_form' => 'Tekan tombol simpan jika semua form telah terisi.',
	'tidak_revisi' => 'Tidak Revisi',
	'unduh_revisi' => 'UNDUH REVISI',
	'member_pembimbing' => 'Anggota/Pembimbing',
	'daftar_member_pembimbing' => 'Daftar Anggota/Pembimbing',
	'tambah_member_pembimbing' => 'Tambah Anggota/Pembimbing',
	'edit_member_pembimbing' => 'Edit Anggota/Pembimbing',
	'member_affiliation' => 'Afiliasi Anggota',
	'member_name' => 'Nama Anggota',
	'member_email' => 'Email Anggota',
	'member_status' => 'Status Anggota',
	'tanpa_revisi' => 'Tanpa Revisi',
);

?>