<?php 

$lang = array(
	// BREADCRUMP
	'ubah_akun' => 'Edit Account',
	'unggah_foto' => 'Unggah Foto',
	'letakkan_file' => 'Letakkan file disini atau klik unggah.',
	'ubah_kata_sandi' => 'Ubah Kata Sandi',
	'ubah_email' => 'Ubah Email',
	'ph_password_lama' => 'Password Lama',
	'data_akun' => 'Data Akun',
	'nm_hak_akses' => 'Nama Hak Akses',
	'daftar_pengajuan_hak_akses' => 'Daftar Pengajuan Hak Akses',
	'ajukan_hak_akses' => 'Ajukan Hak Akses',
	'ubah_data' => 'UBAH DATA',
	'status' => 'Status',
	'password_baru' => 'Password Baru',
	'nm_pengguna' => 'Nama Pengguna',
	'daftar_pegawai' => 'Daftar Pegawai'
);

?>