<?php  

$lang = array(
	// BREADCRUMP
	'list' => 'Pengajuan Artikel',

	// DATA TABLE
	'judul' => 'Judul',
	'karya_sedang_diproses_tidak_bisa_menghapus_atau_mengubah_karya' => 'Artikel sedang diproses. Tidak bisa menghapus atau mengubah karya',
	'detail_karya' => 'Detail Artikel',
	'judul' => 'Judul',
	'isi_abstrak' => 'Silahkan isi abstrak anda di sini.',
	'upload_revisi_plagiarism' => 'Upload Revisi Plagiasi',
	'upload_revisi_reviewer' => 'Upload Revisi Artikel',
	
	'komentar_plagiasi' => 'Komentar Plagiasi',
	'komentar_revisi' => 'Komentar Revisi',
	'ditulis_oleh' => 'Ditulis Oleh',
	'unduh' => 'Unduh File',
	'unggah_bukti_submit'=>'Unggah Bukti Submit',
	'bukti_submit' => 'Bukti Submit',

	// FORM
	'abstrak' => 'Abstrak',
	'pisahkan_dengan_enter' => 'Pisahkan Dengan Enter',
	'upload_karyamu' => 'Unggah Artikel',
	'file_dengan_format_docx' => 'Silahkan kirim artikel terbaik Anda !',
	'silahkan_isi_form_pengajuan' => 'Silahkan isi form pengajuan',
	'letakkan_file_di_sini_atau_klik_untuk_diunggah' => 'Letakkan file di sini atau klik untuk diunggah',
	'isi_abstrak' => 'Silahkan isi abstrak anda di sini.',

	'penerjemahan' => 'Penerjemahan',
	'belum' => 'BELUM DIPERIKSA',
	'ket1' => 'Jika status akhir menunjukkan <b>PROSES PEMERIKSAAN</b> maka karya dalam proses pemeriksaan oleh admin dan anda belum mengunggah bukti submit ke jurnal internasional atau nasional. Untuk upload bukti submit silahkan <a href="'.base_url().'paper/submission/submit"> klik di sini </a>',
	'catatan_mhs1' => 'Artikel Anda tidak akan diproses sebelum mendapatkan persetujuan dari pembimbing 1 <i>(Hanya Mahasiswa)</i>.',
	'catatan_mhs2' => 'Pastikan Anda memasukkan setidaknya 1 Pembimbing sebelum menekan <i>Kirim Kode Verifikasi</i>.',
	'ctt_submit' => 'Artikel Anda hanya bisa disubmit setelah Lolos Semua Seleksi (Plagiasi, Review, dan Translasi) !',
	'catatan_all1' => 'Jumlah maksimal artikel Anda yang sedang kami proses adalah 2.',
	'catatan_all2' => 'Apabila jumlah artikel anda yang sedang kami proses telah mencapai jumlah maksimal, artikel anda selanjutnya tidak akan diproses sebelum mendapat persetujuan dari Admin.',
	'tambah_artikel' => 'Tambah Artikel',
	'klik_lihat' => 'Hal-hal mengenai Hapus Artikel, Edit Artikel, Anggota dan Pembimbing, serta Revisi terdapat pada Detail Artikel.',
	'masukkan_judul' => 'Masukkan Judul Artikel ...',
	'member_pembimbing' => 'Anggota/Pembimbing',
	'daftar_member_pembimbing' => 'Daftar Anggota/Pembimbing',
	'tambah_member_pembimbing' => 'Tambah Anggota/Pembimbing',
	'edit_member_pembimbing' => 'Edit Anggota/Pembimbing',
	'member_affiliation' => 'Afiliasi Anggota',
	'member_name' => 'Nama Anggota',
	'member_email' => 'Email Anggota',
	'member_status' => 'Status Anggota',
	'pilih_member_status' => 'Pilih Status Anggota',
	'success_add_member' => 'Data Member Berhasil Ditambahkan !',
	'success_del_member' => 'Data Member Berhasil Dihapus !',	
	'unverified' => 'Artikel Anda belum mendapat persetujuan dari Pembimbing 1.<br> Silahkan tekan <i>Kirim Kode Verifikasi</i> untuk meminta persetujuan ke Pembimbing 1.',
	'kirim_kode_verifikasi' => 'Kirim Kode Verifikasi !',
	'edit_article' => 'Edit Artikel',
	'hapus_article' => 'Hapus Artikel',
	'catatan_all3' => 'Anda tidak dapat mengedit artikel setelah artikel dikirim ke Admin.',
);

?>