<?php 

$lang = array(
	"anda" => "Anda",
	"aksestpp_um" => "AksesTPP UM",
	"notifikasi" => "Notifikasi",
	"yth" => "Yth. ",
	"to" => "Dear ",
	"salam" => "Salam hangat dari ",
	"intro" => "Ini merupakan notifikasi otomatis ",
	"layanan_tppum" => "layanan TPP UM Anda",
	"informasikan" => "Kami informasikan bahwa ",
	"berhasil" => "berhasil",
	"informasi_karya" => "Informasi Karya",
	"id_karya" => "ID Karya",
	"judul_karya" => "Judul Karya",
	"bidang_riset" => "Bidang Riset",
	"penutup" => "Untuk informasi lebih lanjut mengenai layanan kami, Anda dapat menghubungi TPP UM melalui e-mail ke tpp@um.ac.id atau Silahkan datang langsung ke Kantor Sekretariat TPP UM di Graha Rektorat Lantai 5 Universitas Negeri Malang.",
	"footer" => "Developer Website TPP UM © 2017 Workshop Elektro UM.",
);

?>