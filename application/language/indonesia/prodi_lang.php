<?php 

$lang = array(
	'id_fakultas' => 'Kode Fakultas',
	'id_jurusan' => 'Kode Jurusan',
	'id_prodi' => 'Kode Prodi',
	'nama_prodi' => 'Nama Program Studi',
	'nama_fakultas' => 'Nama Fakultas',
	'nama_jurusan' => 'Nama Jurusan',
	'catatan_prodi1' => 'Mengedit atau Menghapus data Program Studi, Jurusan, dan Fakultas akan berdampak kepada seluruh pengguna Website TPP UM !',
	'catatan_prodi2' => 'Cermati aturan pengkodean Program Studi, Jurusan, dan Fakultas sebelum melakukan perubahan data !',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
);

?>