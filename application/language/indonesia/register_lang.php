<?php 

$lang = array(
	// BREADCRUMP
	'list' => 'Kategori',

	// DATA TABLE
	'silahkan_daftar' => 'Registrasi akun baru',
	'ph_nama_lengkap' => 'Nama Lengkap',
	'ph_email' => 'Email',
	'ph_password' => 'Password',
	'ph_konfirmasi' => 'Konfirmasi Password',
	'ph_birth' => 'Tanggal Lahir',
	'ph_faculty' => 'Fakultas',
	'ph_department' => 'Jurusan',
	'ph_addr' => 'Alamat Lengkap',
	'ph_phone' => 'Nomor Telepon',
	'ph_gender' => 'Pilih Jenis Kelamin',
	'ph_pria' => 'Pria',
	'ph_wanita' => 'Wanita',
	'ph_nim_nidn' => 'NIP/NIDK/NIM',
	'ph_jabatan' => 'Pilih Status',
	'daftar' => 'Buat Akun',
	'punya_akun' => 'Sudah punya akun? Log in ',
	'disini' => 'disini',
	'terms' => 'Dengan mengeklik Buat Akun, saya menyetujui Persyaratan Layanan dan Kebijakan Privasi',
	'failed_email' => 'Email Sudah Terdaftar',
	'pass_char' => 'Password minimal 8 karakter',
	'pass_notmatch' => 'Password anda tidak sesuai',
	'umur_kurang' => 'Umur minimal 15 Tahun!',
	"hanya_huruf" => "Nama tidak boleh mengandung simbol/angka",
	"jurusan" => "Jurusan",
	"prodi" => "Program Studi",
	"fakultas" => "Fakultas",
);

?>