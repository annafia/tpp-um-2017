<?php 

$lang = array(
	// BREADCRUMP
	'list' => 'Beri Hak Akses',

	// DATA TABLE
	'email' => 'Email',

	// FORM
	'tambah_hak_akses' => 'Tambah Hak Akses',
	'hak_akses_untuk' => 'Hak Akses Untuk Pengguna',
	'hasil' => 'Hasil',
	'pilih_hak_akses' => 'Pilih Hak Akses',
	'hak_akses_yang_dipilih' => 'Hak Akses Yang Dipilih',
	'pilih_posisi' => 'Pilih Posisi Pengguna',
	'superuser' => 'Superuser',
	'admin' => 'Admin',
	'reviewer' => 'Reviewer',
	'check_plagiasi' => 'Check Plagiarism',
	'author' => 'Author',
	'detail' => 'DETAIL',

);

?>