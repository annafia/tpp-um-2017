<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_m extends CI_Model {
	public function find($id_plagiarism){
		$res = $this->db->join("submission s","p.id_submission = s.id_submission","left")
						->join("personal_data pd", "p.id_user = pd.id_user","left")
						->where('id_plagiarism', $id_plagiarism)
						->get('plagiarism p');
		return $res->row_array();
	}
	public function count_plag_subm($id_submission){
		$res = $this->db->select("count(*) as jml")
						->where("id_submission",$id_submission)
						->get("plagiarism");
		return $res->row_array();
	}
	public function save($data){
		$res = $this->db->where("id_plagiarism",$data['id_plagiarism'])
						->update("plagiarism",$data);
		return $res;
	}
	public function send($id_plagiarism){
		$res = $this->db->where("id_plagiarism",$id_plagiarism)
						->set("finish",date("Y-m-d H:i:s"))
						->set("status_plag",1)
						->update("plagiarism");
		return $res;
	}

	public function get_not($id_user){
		$res = $this->db->join('submission s', 's.id_submission = p.id_submission')
						->join('topic t', 't.id_topic = s.id_topic')
						->where('status_plag', 0)
						->where('p.id_user', $id_user)
						->where("active",1)
						->get('plagiarism p');
		return $res->result_array();
	}

	public function get_finish($id_user){
		$res = $this->db->select('*, p.create_at as tervalidasi')
						->join('submission s', 's.id_submission = p.id_submission')
						->join('topic t', 't.id_topic = s.id_topic')
						->where('status_plag', 1)
						->where('p.id_user', $id_user)
						->where("active",1)
						->get('plagiarism p');
		return $res->result_array();
	}

	public function get_rev($id_user){
		$res = $this->db->select('*, plagiarism.create_at as tervalidasi')
						->join('submission', 'submission.id_submission = plagiarism.id_submission')
						->join('topic', 'topic.id_topic = submission.id_topic')
						->where('status_plag', 2)
						->where('plagiarism.id_user', $id_user)
						->get('plagiarism');
		return $res->result_array();
	}
}

?>