<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_m extends CI_Model {
	public function find_submission($id_submission){
		$res = $this->db->join("topic t","s.id_topic=t.id_topic","left")
						->join("user u","u.id_user=s.id_user","left")
						->join("personal_data p","s.id_user=p.id_user","left")
						->where("s.id_submission",$id_submission)
						->get("submission s");
		return $res->row_array();
	}
	public function find_plagiarism($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where('active',1)
						->get("plagiarism p")
						->row_array();
		return $this->get_current_user($res['id_user']);
	}
	public function find_reviewer($id_review){
		$res = $this->db->where("id_review",$id_review)
						->get("review")
						->row_array();
		return $this->get_current_user($res['id_user']);
	}
	public function find_translator($id_submission){
		$res = $this->db->join("user u","u.id_user = t.id_user","left")
						->where("id_submission",$id_submission)
						->get("translation t")
						->row_array();
		return $this->get_current_user($res['id_user']);
	}
	public function get_admin(){
		$sql = "select *from data_privilage dp left outer join user u on dp.id_user = u.id_user left outer join personal_data p on u.id_user=p.id_user where dp.privilage_id =2 AND dp.status = 1";
		$res=$this->db->query($sql);

		return $res->result_array();
	}
	public function get_current_user($id_user){
		$res = $this->db->join("personal_data p","u.id_user=p.id_user","left")
						->where("u.id_user",$id_user)
						->get("user u");
		return $res->row_array();
	}
}

?>