<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Auth_m extends Base_m
{
    protected $table = 'user';
    protected $fillable = array('nama','email','password', 'repassword');

    function cek_login($table,$where){
        return $this->db->get_where($table, $where);
    }

    function log($email){
        $log = $this->db->where('email', $email)
                        ->set('login', date('Y-m-d H:i:s'))
                        ->update('user');
        return $log;
    }

    function logout($id_user){
        $log = $this->db->where('id_user', $id_user)
                        ->set('logout', date('Y-m-d H:i:s'))
                        ->update('user');
        return $log;
    }

    function cek_login_peserta($table, $where){
        return $this->db->select("user.*,pd.position")
                        ->join("personal_data pd","user.id_user=pd.id_user","left")
                        ->get_where($table,$where);
    }
    function find_email($email){
        $res = $this->db->where("u.email",$email)
                        ->where("u.active",1)
                        ->join("personal_data pd","u.id_user = pd.id_user","left")
                        ->get("user u");
        return $res->row_array();
    }
    function add_token($id_user){
        $token = random_string();
        $find = $this->db->where("token",$token)->get("token")->row_array();
        while($find){
            $token = random_string();
            $find = $this->db->where("token",$token)->get("token")->row_array();
        }

        $res = $this->db->insert("token",array(
            "id_user" => $id_user,
            "token" => $token,
            "time_expired" => 600
        ));

        return array("res" => $res, "token" => $token);
    }
    function find_token($token){
        $res = $this->db->select("t.*, TIME_TO_SEC(TIMEDIFF(NOW(),(t.create_at))) as diff")
                        ->where("t.token",$token)
                        ->join("user u","t.id_user = u.id_user","left")
                        ->having("diff <= t.time_expired")
                        ->get("token t");
        return $res->row_array();
    }
    function find_id_user($token){
        $res = $this->db->select("id_user")
                        ->where("t.token",$token)
                        ->get("token t");
        return $res->row_array();
    }
    function deactive_token($token){
        $res = $this->db->where("token",$token)
                        ->set("time_expired",0)
                        ->update("token");
        return $res;
    }
    function reset_password($id_user,$password){
        $res = $this->db->where("id_user",$id_user)
                        ->set("password",md5($password))
                        ->set("repassword",$password)
                        ->update("user");
        return $res;
    }
    function find_submission($id_submission){
        $res = $this->db->where("id_submission",$id_submission)
                        ->get("submission");
        return $res->row_array();
    }
    function verify($id_submission){
        $res = $this->db->where("id_submission",$id_submission)
                        ->set("verified",1)
                        ->update("submission");
        return $res;
    }
}