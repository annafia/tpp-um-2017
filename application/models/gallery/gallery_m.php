<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_m extends CI_Model {
	public function find($id_gallery){
		$res=$this->db->where("id_gallery", $id_gallery)->get("gallery");

		return $res->result_array();
	}

	public function add($data){
		$data['create_at']=date("Y-m-d H:i:s");
		$res=$this->db->insert("gallery",$data);

		return $res;
	}
	public function update($data){
		$data['create_at']=date("Y-m-d H:i:s");
		$where=array("id_gallery" => $data['id_gallery']);
		unset($data['id_gallery']);

		$res=$this->db->update("gallery",$data,$where);

		return $res;
	}
	public function show_gallery(){
		$res=$this->db->get("gallery");

		return $res->result_array();
	}
	public function del($id_gallery){
		$res=$this->db->where("id_gallery", $id_gallery)->delete("gallery");

		return $res;
	}
}

?>