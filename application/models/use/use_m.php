<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Use_m extends CI_Model {
	public function find($privilage_id){
		$res = $this->db->where('privilage_id', $privilage_id)->get('privilage');
		return $res->row_array();
	}	

	public function get(){
		$this->db->select('*')
				 ->from('user')
				 ->join('data_privilage', 'user.id_user = data_privilage.id_user')
				 ->group_by('user.email');
		$res = $this->db->get('');
		return $res->result_array();
	}

	public function proof(){
		$res = 	$this->db->join('data_privilage', 'user.id_user = data_privilage.id_user')
						 ->where('data_privilage.status', 0)
				 	 	 ->group_by('user.email')
				  		 ->get('user');
		return $res->result_array();
	}

	public function find_user($id_user){
		$res = $this->db->where('id_user', $id_user)->get('user');
		return $res->result_array();
	}

	public function find_role($id_user){
		$res = $this->db->where('id_user', $id_user)
						->get('user');
		return $res->result_array();
	}

	public function find_prev($id_user){
		$res = $this->db->where('id_user', $id_user)
						->get('data_privilage');
		return $res->result_array();
	}

	public function access($id_user){
		$this->db->select('*')
				 ->from('user')
				 ->join('data_privilage', 'user.id_user = data_privilage.id_user')
				 ->where('user.id_user', $id_user);
		$res = $this->db->get('');
		return $res->result_array();
	}

	public function get_privilage(){
		$res = $this->db->get('privilage');
		return $res->result_array();
	}

	public function create($data, $role){
		$res = $this->db->set('status', 1)
						->insert('data_privilage', $data);

		return $res;
	}

	public function update($where, $data){
		$this->db->update('privilage', $data, $where);
		return $this->db->affected_rows();
	}

	public function update_role($id_user, $role){
		$res = $this->db->where('id_user', $id_user)
						->set('role', $role)
						->update('user');
		return $res;
	}

	public function delete($privilage_id){
		$res = $this->db->where('privilage_id', $privilage_id)
						->delete('privilage');
		return $res;
	}
}

?>