<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_m extends CI_Model {
	public function get_not(){
		$res = $this->db->select('t.*,pd.full_name,s.tittle,nm_topic,file')
						->join('submission s', 't.id_submission = s.id_submission')
						->join('personal_data pd','s.id_user=pd.id_user')
						->join('topic tp','tp.id_topic = s.id_topic')
						->where('status_translation', 0)
						->where('t.id_user', $this->session->userdata("id_user"))
						->get('translation t');
		return $res->result_array();
	}
	public function get_finish(){
		$res = $this->db->select('t.*,pd.full_name,s.tittle,nm_topic')
						->join('submission s', 't.id_submission = s.id_submission')
						->join('personal_data pd','s.id_user = pd.id_user')
						->join('topic tp','tp.id_topic = s.id_topic')
						->where('status_translation', 1)
						->where('t.id_user', $this->session->userdata("id_user"))
						->get('translation t');
		return $res->result_array();
	}
	public function find($id_submission){
		$res = $this->db->join('submission s', 't.id_submission = s.id_submission')
						->join('personal_data pd','pd.id_user=t.id_user')
						->where('t.id_user', $this->session->userdata("id_user"))
						->where('s.id_submission', $id_submission)
						->where('status_translation', 0)
						->get('translation t');
		return $res->row_array();
	}
	public function find_author($id_submission){
		$res = $this->db->join('personal_data pd','pd.id_user=s.id_user')
						->where('s.id_submission', $id_submission)
						->get('submission s');
		return $res->row_array();
	}
	public function save($data){
		$res = $this->db->where('id_submission',$data['id_submission'])
						->update('translation',$data);
		return $res;
	}
	public function submit($id_submission){
		$res = $this->db->where('id_submission',$id_submission)
						->set('finish_at',date('Y-m-d H:i:s'))
						->set('status_translation',1)
						->update('translation');
		return $res;
	}





	
	public function find_translated($id_submission){
		$res = $this->db->join('submission s', 't.id_submission = s.id_submission', 'left')
						->where('t.id_user', $this->session->userdata("id_user"))
						->where('s.id_submission', $id_submission)
						->where('status_translation', 1)
						->get('translation t');
		return $res->result_array();
	}
	
	public function create($data){
		$res = $this->db->set("create_at",date("Y-m-d H:i:s"))
						->set("translation_file",$data['translation_file'])
						->set("status_translation",1)
						->where("id_submission",$data['id_submission'])
						->update('translation');
		$upd = $this->db->set("status_translator", 1)
						->set("status", 1)
						->set("notif_trans",2)
						->where("id_submission",$data['id_submission'])
						->update("submission");

		return $res;
	}
	public function back($id_submission){
		$res = $this->db->set("status_translation", 0)
						->where("id_submission", $id_submission)
						->update("translation");
		$upd = $this->db->set("status_translator", 3)
						->set("notif_trans",1)
						->where("id_submission", $id_submission)
						->update("submission");
		return $res;
	}
}

?>