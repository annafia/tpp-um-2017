<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_m extends CI_Model {
	public function find($id_user){
		$res = $this->db->join("user u","u.id_user=p.id_user","left")
						->where('p.id_user', $id_user)
						->get('personal_data p');
		return $res->row_array();
	}
	public function find_privilege($id_user, $no){
		$res = $this->db->where('id_user', $id_user)
						->where('no', $no)
						->get('data_privilage');
		return $res;
	}
	public function privilage(){
		$res = $this->db->get('privilage');
		return $res->result_array();
	}
	public function get_user_data($id_user){
		$sql="Select *from user u left join personal_data p on u.id_user = p.id_user where u.id_user='$id_user'";
		$res = $this->db->query($sql)->result();
		return $res;
	}
	public function edit_personal_data($data,$where){
		$res = $this->db->update("personal_data",$data,$where);
		return $res;
	}
	public function validate_password($id_user,$password){
		$sql = "select *from user where id_user='$id_user' && password='$password'";
		$res = $this->db->query($sql);
		return $res->row_array();
	}
	public function ubah_data_user($data,$where){
		$res = $this->db->set("active",0)
						->update("user",$data,$where);
		return $res;
	}
	public function get_privilege(){
		$id_user = $this->session->userdata('id_user');
		$res = $this->db->join('privilage', 'privilage.privilage_id = data_privilage.privilage_id')
						->join('personal_data', 'personal_data.id_user = data_privilage.id_user')
						->where('data_privilage.id_user', $id_user)
						->get('data_privilage');
		return $res->result_array();
	}
	public function delete_privilege($id_user, $no){
		$res = $this->db->where('id_user', $id_user)
						->where('no', $no)
						->delete('data_privilage');
		return $res;
	}
	public function create_previlege($data, $id_user){
		$res = $this->db->insert('data_privilage', $data);
		return $res;
	}
	public function find_prev($id_user){
		$res = $this->db->where('id_user', $id_user)
						->get('data_privilage');
		return $res->result_array();
	}
	public function del_emp($id_user){
		$res = $this->db->where('id_user', $id_user)->delete('user');
		$res = $this->db->where('id_user', $id_user)->delete('translation');
		$res = $this->db->where('id_user', $id_user)->delete('submission');
		$res = $this->db->where('id_user', $id_user)->delete('revision');
		$res = $this->db->where('id_user', $id_user)->delete('plagiarism');
		$res = $this->db->where('id_user', $id_user)->delete('personal_data');
		$res = $this->db->where('id_user', $id_user)->delete('detail_submission');
		$res = $this->db->where('id_user', $id_user)->delete('detail_notif');		
		$res = $this->db->where('id_user', $id_user)->delete('data_privilage');
		return $res;
	}
}