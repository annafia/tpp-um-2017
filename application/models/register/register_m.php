<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_m extends CI_Model {
	public function find($email){
		$res = $this->db->where('email', $email)->get('user');
		return $res->row_array();
	}
	public function create_user($data_user){
		$res = $this->db->insert('user', $data_user);
		return $this->db->insert_id();;
	}
	public function create_personal_data($data_personal){
		$res = $this->db->insert('personal_data', $data_personal);
		return $res;
	}
	public function active($email){
		$res = $this->db->where("email",$email)
						->set("active",1)
						->update("user");
		return $res;
	}
}

?>