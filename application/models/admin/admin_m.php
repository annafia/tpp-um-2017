<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_m extends CI_Model {
	/*------------------------------ Start Universal ------------------------------*/
	public function find_author($id_submission){
		$res = $this->db->join("personal_data pd","pd.id_user=s.id_user","left")
						->where("id_submission",$id_submission)
						->get("submission s");
		return $res->row_array();
	}
	public function find_submission($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->join("topic t","s.id_topic = t.id_topic","left")
						->join('personal_data pd','pd.id_user=s.id_user')
						->get('submission s');
		return $res->row_array();
	}
	public function get_member($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->get("member");

		return $res->result_array();
	}
	public function get_detail_plagiarism($id_submission){
		$res = $this->db->select("p.*,pd.full_name,s.tittle,s.file")
						->join("personal_data pd", "pd.id_user = p.id_user", "left")
						->join("submission s","s.id_submission=p.id_submission","left")
						->where("p.id_submission",$id_submission)
						->where("status_plag",1)
						->get("plagiarism p");

		return $res->result_array();
	}
	public function get_detail_review($id_submission){
		$res = $this->db->select("r.*,pd.full_name,s.tittle,s.file")
						->join("personal_data pd", "pd.id_user = r.id_user", "left")
						->join("submission s","s.id_submission=r.id_submission","left")
						->where("r.id_submission",$id_submission)
						->where("status_review",1)
						->get("review r");

		return $res->result_array();
	}
	public function find_translasi($id_submission){
		$res = $this->db->select('t.*')
						->join('translation t','t.id_submission = s.id_submission')
						->where("s.id_submission",$id_submission)
						->where("status_translator",1)
						->get("submission s");
		return $res->row_array();
	}
	/*------------------------------ End Universal ------------------------------*/

	/*------------------------------ Start Plagiarism ------------------------------*/
	public function create_plagiarism($data){
		$deactive = $this->db->where("id_submission",$data['id_submission'])
							->set("active",0)
							->update("plagiarism");
		$res = $this->db->insert('plagiarism', $data);
		$id_plagiarism = $this->db->insert_id();
		
		$update = $this->db->where('id_submission', $data['id_submission'])
						   ->set('status_plagiarism', 3)
						   ->update('submission');
		$author = $this->find_author($data['id_submission']);
		$add = $this->db->where("id_user",$author['id_user'])
						->set("proccess_time", $author['proccess_time']+1)
						->update("personal_data");
		$save_fp = $this->db->where("id_plagiarism",$id_plagiarism)
							->set("fp_file",$author['file'])
							->update("plagiarism");
		return $res;
	}
	public function get_job_plagiarism(){
		$res = $this->db->select('*, p.create_at as date_rev')
						->order_by('status_plagiarism', 'ASC')
						->where("p.active",1)
						->join('plagiarism p', 'p.id_user = u.id_user')
						->join('submission s', 's.id_submission = p.id_submission')
						->join('personal_data pd', 'pd.id_user = p.id_user')
						->get('user u');
		return $res->result_array();
	}
	public function get_send_job_plagiarism(){
		$res = $this->db->where("status_plagiarism",0)
						->where("verified",2)
						->join("personal_data pd","pd.id_user = s.id_user","left")
						->get('submission s');
		return $res->result_array();
	}
	public function find_finish_job_plagiarism($id_submission){
		$res = $this->db->where("s.id_submission",$id_submission)
						->where("status_plagiarism",3)
						->where("status_plag",1)
						->where("active",1)
						->join("submission s","s.id_submission = p.id_submission")
						->get('plagiarism p');
		return $res->row_array();
	}
	public function find_job_plagiarism($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("status_plag",0)
						->where("active",1)
						->get('plagiarism p');
		return $res->row_array();
	}
	public function find_send_job_plagiarism($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("status_plagiarism",0)
						->where("verified",2)
						->get('submission');
		return $res->row_array();
	}
	public function back_check_plagiarism($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->where("active",1)
						->set('status_plag', 0)
						->update('plagiarism');
		return $res;
	}
	public function back_author_plagiarism($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->set('status_plagiarism', 2)
						->set('status', 2)
						->set('verified',1)
						->update('submission');
		$author = $this->find_author($id_submission);
		$min = $this->db->where("id_user",$author['id_user'])
						->set("proccess_time", $author['proccess_time']-1)
						->update("personal_data");
		return $res;
	}
	public function next_plagiarism($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->set('status_plagiarism', 1)
						->update('submission');
		$author = $this->find_author($id_submission);
		$min = $this->db->where("id_user",$author['id_user'])
						->set("proccess_time", $author['proccess_time']+1)
						->update("personal_data");
		return $res;
	}
	public function cancel_job_plagiarism($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("status_plag",0)
						->where("active",1)
						->delete('plagiarism');
		$update = $this->db->where('id_submission', $id_submission)
						   ->set('status_plagiarism', 0)
						   ->update('submission');
		$author = $this->find_author($id_submission);
		$add = $this->db->where("id_user",$author['id_user'])
						->set("proccess_time", $author['proccess_time']-1)
						->update("personal_data");
		return $res;
	}
	/*------------------------------ End Plagiarism ------------------------------*/

	/*------------------------------ Start Reviewer ------------------------------*/
	public function get_send_job_rev(){
		$res = $this->db->join('personal_data pd', 'pd.id_user = s.id_user')
					    ->where('status_plagiarism', 1)
						->where('status_reviewer', 0)
						->where('verified',2)
						->get('submission s');
		return $res->result_array();
	}
	public function find_job_rev($id_submission){
		$res = $this->db->join('personal_data pd', 'pd.id_user = s.id_user')
						->join("topic t","s.id_topic = t.id_topic","left")
						->where('id_submission',$id_submission)
					    ->where('status_plagiarism', 1)
						->where('status_reviewer', 3)
						->get('submission s');
		return $res->row_array();
	}
	public function get_job_rev(){
		$res = $this->db->join('personal_data pd', 'pd.id_user = s.id_user')
					    ->where('status_plagiarism', 1)
						->where('status_reviewer', 3)
						->get('submission s');
		return $res->result_array();
	}
	public function find_send_job_rev($id_submission){
		$res = $this->db->join('personal_data pd', 'pd.id_user = s.id_user')
					    ->where('status_plagiarism', 1)
						->where('status_reviewer', 0)
						->where('verified',2)
						->where('id_submission',$id_submission)
						->get('submission s');
		return $res->row_array();
	}
	public function create_job_reviewer($data){
		$res = $this->db->insert('review', $data);

		$update = $this->db->where('id_submission', $data['id_submission'])
						   ->set('status_reviewer', 3)
						   ->update('submission');
		return $res;
	}
	public function deactive_job_reviewer($id_submission){
		$res = $this->db->where('id_submission',$id_submission)
						->set('active',0)
						->update('review');
		return $res;
	}
	public function get_active_review($id_submission){
		$res = $this->db->where('id_submission',$id_submission)
						->where('active',1)
						->join('personal_data pd','pd.id_user = r.id_user')
						->get('review r');
		return $res->result_array();
	}
	public function count_review_adm($id_submission){
		$res = $this->db->select('count(*) as jml')
						->where('id_submission',$id_submission)
						->where('review_as',2)
						->get('review')
						->row_array();
		return $res['jml'];
	}
	public function find_review_adm($id_submission){
		$res = $this->db->where('id_submission',$id_submission)
						->where('active',1)
						->where('review_as',2)
						->get('review')
						->row_array();
		return $res;
	}
	public function save_review($data){
		$res = $this->db->where('id_submission',$data['id_submission'])
						->where('review_as',2)
						->where('active',1)
						->update('review',$data);
		return $res;
	}
	public function submit_review($id_submission,$decission_status){
		$res = $this->db->where('id_submission',$id_submission)
							->where('finish_at is NULL')
							->set('finish_at',date('Y-m-d H:i:s'))
							->set('status_review',1)
							->update('review');

		if($decission_status==2){
			$subm = $this->db->where('id_submission',$id_submission)
							->set('status_reviewer',1)
							->update('submission');
		} else if($decission_status==1){
			$this->deactive_job_reviewer($id_submission);
			$subm = $this->db->where('id_submission',$id_submission)
							->set('status_reviewer',2)
							->set('status',2)
							->set('verified',1)
							->update('submission');
		}
		return $res;
	}
	public function find_finish_review($id_review){
		$res = $this->db->where("id_review",$id_review)
						->where('status_review',1)
						->get("review");
		return $res->row_array();
	}
	public function back_review($id_review){
		$res = $this->db->where('id_review',$id_review)
						->set('status_review',0)
						->update('review');
		return $res;
	}
	/*------------------------------ End Reviewer ------------------------------*/

	/*------------------------------ Start Translator ------------------------------*/
	public function get_not_yet_trans(){
		$res = $this->db->join('personal_data pd','pd.id_user=s.id_user')
						->where('status_reviewer', 1)
						->where('status_translator', 0)
						->get('submission s');
		return $res->result_array();
	}
	public function get_already_trans(){
		$res = $this->db->join('submission s', 'u.id_user = s.id_user')
						->join('translation t', 't.id_submission = t.id_submission')
						->where('status_translator', 1)
						->get('user u');
		return $res->result_array();
	}
	public function assignment_translator(){
		$res = $this->db->select('t.*,pd.full_name,s.tittle,s.file')
						->join('personal_data pd','t.id_user=pd.id_user')
						->join('submission s', 't.id_submission = s.id_submission')
						->where('status_translator',3)
						->get('translation t');
		return $res->result_array();
	}
	public function find_translation($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->where('status_reviewer',1)
						->where('status_translator',0)
						->get('submission');
		return $res->row_array();
	}
	public function find_translated($id_submission){
		$res = $this->db->where('t.id_submission', $id_submission)
						->where('status_translation',1)
						->get('translation t');
		return $res->row_array();
	}
	public function send_job_translator($data){
		$data['create_at']=date("Y-m-d H:i:s");
		$res = $this->db->insert("translation", $data);

		$update = $this->db->where("id_submission", $data['id_submission'])
							->set("status_translator", 3)
							->update("submission");
		return $res;
	}
	public function back_translation($id_submission){
		$res = $this->db->set("status_translation", 0)
						->where("id_submission", $id_submission)
						->update("translation");
		return $res;
	}
	public function next_translation($id_submission){
		$res = $this->db->set("status_translator", 1)
						->set('status',1)
						->where("id_submission", $id_submission)
						->update("submission");
		return $res;
	}
	public function find_assign_trans($id_submission){
		$res = $this->db->where('id_submission',$id_submission)
						->where('status_translation',0)
						->get('translation t');
		return $res->row_array();
	}
	public function cancel_job_trans($id_submission){
		$res = $this->db->where('id_submission',$id_submission)
						->delete('translation');
		$update = $this->db->where('id_submission', $id_submission)
						   ->set('status_translator', 0)
						   ->update('submission');
		return $res;
	}
	/*------------------------------ End Translator ------------------------------*/

	/*------------------------------ Start Employee ------------------------------*/
	public function find($id_user){
		$res = $this->db->where('id_user', $id_user)->get('user');
		return $res->row_array();
	}
	public function get_employee(){
		$res = $this->db->join('personal_data', 'personal_data.id_user = user.id_user')
						->or_where('role', 2)
						->or_where('role', 3)
						->or_where('role', 5)
						->get('user');
		return $res->result_array();
	}
	/*------------------------------ End Employee ------------------------------*/

	/*------------------------------ Start Submit ------------------------------*/
	public function valid_submission($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->set('status', 3)
						->update('submission');

		$usr = $this->find_author($id_submission);

		$finish = $this->db->where('id_user',$usr['id_user'])
							->set('proccess_time',$usr['proccess_time']-1)
							->set('finished_submission',$usr['finished_submission']+1)
							->update('personal_data');
		return $res;
	}

	public function not_valid($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->set('status', 4)
						->update('submission');
		return $res;
	}

	public function back_stat_submission($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->set('status', 6)
						->update('submission');
		$usr = $this->db->select("s.id_user,pd.position")
						->where("s.id_submission",$id_submission)
						->join("personal_data pd","pd.id_user = s.id_user")
						->get("submission s")
						->row_array();
		if($usr['position']==2){
			$this->db->query("update personal_data set proccess_time = proccess_time-1 where id_user=".$usr['id_user']);
		}
		return $res;
	}	

	public function get_list_submit(){
		$res = $this->db->where("s.status",6)
						->join("user u","u.id_user = s.id_user","left")
						->join("topic t","t.id_topic = s.id_topic")
						->join("personal_data p","p.id_user=s.id_user","left")
						->get("submission s");
		return $res->result_array();
	}
	public function get_finish_submit(){
		$res = $this->db->where("s.status",3)
						->join("user u","u.id_user = s.id_user","left")
						->join("topic t","t.id_topic = s.id_topic")
						->join("personal_data p","p.id_user=s.id_user","left")
						->get("submission s");
		return $res->result_array();
	}
	/*------------------------------ End Submit ------------------------------*/

	/*------------------------------ Start Report ------------------------------*/
	public function printout_submission(){
		$res = $this->db->select('s.*, full_name, email, nm_topic, file')
						->join('user u', 'u.id_user = s.id_user')
						->join('personal_data pd', 'pd.id_user = u.id_user')
						->join('topic t', 't.id_topic = s.id_topic')
						->get('submission s');

		return $res->result_array();
	}
	
	public function report($data){
		$start_date = $data['tgl1'];
		$end_date = $data['tgl2'];
		if($data['id_topic']!=""){
			$this->db->where('t.id_topic',$data['id_topic']);
		}

		if($data['id_fakultas']!=""){
			$this->db->where('id_fakultas',$data['id_fakultas']);
			if($data['id_jurusan']!=""){
				$this->db->where('id_jurusan',$data['id_jurusan']);
				if($data['id_prodi']!=""){
					$this->db->where('id_prodi',$data['id_prodi']);
				}
			}
		}
		if($data['status']<=6 && $data['status']!=""){
			$this->db->where('s.status',$data['status']);
		} else if($data['status']==7){
			$this->db->where_in('status_plagiarism',array(0,3));
		} else if($data['status']==8){
			$this->db->where('status_plagiarism',1);
			$this->db->where_in('status_reviewer',array(0,3));
		} else if($data['status']==9){
			$this->db->where('status_reviewer',1);
			$this->db->where_in('status_translator',array(0,3));
		}
		if ($data['author']!="") {
			$this->db->where('s.id_user',$data['author']);
		}
		$res = $this->db->join('topic t','t.id_topic=s.id_topic')
						->join('personal_data pd','pd.id_user=s.id_user')
						->join('user u','u.id_user=s.id_user')
						->where('s.create_at BETWEEN "'.$start_date. '" and "'.$end_date.'"')
						->select('s.*, full_name, email, nm_topic, file')
						->get('submission s');
		return $res;
	}
	/*------------------------------ End Report ------------------------------*/

}

?>