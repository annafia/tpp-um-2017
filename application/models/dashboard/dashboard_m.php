<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_m extends CI_Model {
	public function all_submission(){
		$res = $this->db->get('submission');
		return $res->num_rows();
	}
	public function onprocess_submission(){
		$res = $this->db->where('status_reviewer', 3)->get('submission');
		return $res->num_rows();
	}
	public function rejected_submission(){
		$res = $this->db->where('status_reviewer', 4)->get('submission');
		return $res->num_rows();
	}
	function show_gallery($number,$offset){
		$res = $this->db->get('gallery',$number,$offset)->result_array();

		return $res;
	}
	public function total_image(){
		$res=$this->db->get("gallery");

		return $res->num_rows();
	}
	public function get_visitors(){
		$v1 = $this->db->select('count(*) as jml')->where('page',1)->get('visitor')->row_array();
		$v2 = $this->db->select('count(*) as jml')->where('page',2)->get('visitor')->row_array();
		$v3 = $this->db->select('count(*) as jml')->where('page',3)->get('visitor')->row_array();
		$v4 = $this->db->select('count(*) as jml')->where('page',4)->get('visitor')->row_array();
		$v5 = $this->db->select('count(*) as jml')->where('page',5)->get('visitor')->row_array();
		$v6 = $this->db->select('count(*) as jml')->where('page',6)->get('visitor')->row_array();
		
		$data = array(
			'admin' => $v1['jml'],
			'plag' => $v2['jml'],
			'rev' => $v3['jml'],
			'auth' => $v4['jml'],
			'trans' => $v5['jml'],
			'dash' => $v6['jml'],
		);
		return $data;
	}
}
?>