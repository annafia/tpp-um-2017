<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi_m extends CI_Model {
	public function get_jurusan($id_fakultas){
		$id_fakultas = $id_fakultas/1000;
		$res = $this->db->like("id_jurusan","$id_fakultas","after")
						->get("jurusan");

		return $res->result_array();
	}
	public function get_prodi($id_jurusan){
		$id_jurusan = $id_jurusan/100;
		$res = $this->db->like("id_prodi","$id_jurusan","after")
						->get("prodi");

		return $res->result_array();
	}
}

?>