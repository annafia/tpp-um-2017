<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job_m extends CI_Model {
	public function get_job(){
		$res = $this->db->select('r.*,s.tittle,t.nm_topic')
						->join('submission s','s.id_submission=r.id_submission')
						->join('topic t','s.id_topic=t.id_topic')
						->where('r.id_user',$this->session->userdata('id_user'))
						->get('review r');
		return $res->result_array();
	}

	/*------------------------------ Start Detail Review ------------------------------*/
	public function find_job($id_submission){
		$res = $this->db->select('r.*,s.*,t.*,pd.full_name')
						->join('submission s','s.id_submission=r.id_submission')
						->join('topic t','s.id_topic=t.id_topic')
						->join('personal_data pd','pd.id_user=s.id_user')
						->where('r.id_user',$this->session->userdata('id_user'))
						->where('active',1)
						->where('status_review',0)
						->where('r.id_submission',$id_submission)
						->get('review r');
		return $res->row_array();
	}
	public function get_member($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->get("member");

		return $res->result_array();
	}
	public function get_detail_plagiarism($id_submission){
		$res = $this->db->select("p.*,pd.full_name,s.tittle,s.file")
						->join("personal_data pd", "pd.id_user = p.id_user")
						->join("submission s","s.id_submission=p.id_submission")
						->where("p.id_submission",$id_submission)
						->where("status_plag",1)
						->get("plagiarism p");

		return $res->result_array();
	}
	public function get_detail_review($id_submission){
		$res = $this->db->select("r.*,pd.full_name,s.tittle,s.file")
						->join("personal_data pd", "pd.id_user = r.id_user", "left")
						->join("submission s","s.id_submission=r.id_submission","left")
						->where("r.id_submission",$id_submission)
						->where("status_review",1)
						->get("review r");

		return $res->result_array();
	}
	public function count_review($id_submission){
		$res = $this->db->select('count(*) as jml, full_name')
						->join("personal_data pd", "pd.id_user = r.id_user")
						->where('id_submission',$id_submission)
						->where('r.id_user',$this->session->userdata('id_user'))
						->get('review r')
						->row_array();
		return $res;
	}
	public function save_review($data){
		$res = $this->db->where('id_submission',$data['id_submission'])
						->where('id_user',$this->session->userdata('id_user'))
						->where('active',1)
						->update('review',$data);
		return $res;
	}
	public function submit($id_submission){
		$res = $this->db->where('id_submission',$id_submission)
						->where('id_user',$this->session->userdata('id_user'))
						->where('active',1)
						->set('status_review',1)
						->set('finish_at',date('Y-m-d H:i:s'))
						->update('review');
		return $res;
	}
	/*------------------------------ End Universal ------------------------------*/
}

?>