<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privilage_m extends CI_Model {
	public function find($privilage_id){
		$res = $this->db->where('privilage_id', $privilage_id)->get('privilage');
		return $res->row_array();
	}	

	public function get(){
		$res = $this->db->get('privilage');
		return $res->result_array();
	}

	public function create($data){
		$res = $this->db->insert('privilage', $data);
		return $res;
	}

	public function update($where, $data){
		$this->db->update('privilage', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete($privilage_id){
		$res = $this->db->where('privilage_id', $privilage_id)
						->delete('privilage');
		return $res;
	}
}

?>