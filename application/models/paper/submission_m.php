<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submission_m extends CI_Model {
	/*--------------- Start Artikel ---------------*/
	public function get(){
		$res = $this->db->select('s.*,nm_topic')
						->where("id_user",$this->session->userdata("id_user"))
						->join('topic t', 't.id_topic = s.id_topic')
						->order_by('create_at', 'desc')
						->get('submission s');
		return $res->result_array();
	}
	public function find($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->join("topic t","s.id_topic = t.id_topic","left")
						->get("submission s");
		return $res->row_array();
	}
	public function create($data){
		$date = date('Y-m-d');
		if ($this->session->userdata("position")==2) {
			$data['verified'] = 1;
		}
		$res = $this->db->set('date', $date)->insert('submission', $data);

		$id_submission = $this->db->insert_id();
		
		return array(
			"res" => $res,
			"id_submission" => $id_submission
		);
	}
	public function save_update($data){
		$res = $this->db->where("id_submission",$data['id_submission'])
						->set("create_at",date("Y-m-d H:i:s"))
						->update("submission",$data);
		return $res;
	}
	public function find_updatable_submission($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->get("plagiarism")
						->row_array();
		if(!$res){
			$res = $this->db->where("id_submission",$id_submission)
							->where("verified  !=",2)
							->where("status_plagiarism",0)
							->join("personal_data pd","pd.id_user = s.id_user","left")
							->get("submission s")
							->row_array();
		}
		return $res;
	}
	public function save_file($id_submission,$file_name){
		$res = $this->db->set("file",$file_name)
						->where("id_submission",$id_submission)
						->update("submission");
		return $res;
	}
	public function delete($id_submission){
		$res = $this->db->where('id_submission', $id_submission)
						->delete('submission');
		return $res;
	}
	public function find_unverified_submission($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("verified",0)
						->join("personal_data pd","pd.id_user = s.id_user","left")
						->get("submission s");
		return $res->row_array();
	}
	public function find_verified_submission($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("verified",1)
						->join("personal_data pd","pd.id_user = s.id_user","left")
						->get("submission s");
		return $res->row_array();
	}
	public function find_translasi($id_submission){
		$res = $this->db->select('t.*')
						->join('translation t','t.id_submission = s.id_submission')
						->where("s.id_submission",$id_submission)
						->where("status_translator",1)
						->get("submission s");
		return $res->row_array();
	}
	public function send_to_admin($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->set("verified",2)
						->update("submission");
		return $res;
	}
	/*--------------- End Artikel ---------------*/

	/*--------------- Start Member ---------------*/
	public function get_member($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->get("member");

		return $res->result_array();
	}
	public function find_member($id_member){
		$res = $this->db->where("id_member",$id_member)
						->get("member");

		return $res->row_array();
	}
	public function find_member_status($id_submission,$member_status){
		$res = $this->db->where('id_submission',$id_submission)
						->where('member_status',$member_status)
						->get('member');
		return $res->row_array();
	}
	public function add_member($data){
		$res = $this->db->insert("member",$data);

		return $res;
	}
	public function edit_member($data){
		$res = $this->db->where("id_member",$data['id_member'])
						->update("member",$data);
		return $res;
	}
	public function del_member($id_member){
		$res = $this->db->where("id_member",$id_member)
						->delete("member");
		return $res;
	}
	public function find_first_mentor($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("member_status",2)
						->get("member");
		return $res->row_array();
	}
	public function find_other_mentor($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where_in("member_status",array(3,4))
						->get("member");
		return $res->result_array();
	}
	/*--------------- End Member ---------------*/

	/*--------------- Start Plagiarism ---------------*/
	public function get_plagiarism($id_submission){
		$res = $this->db->select("p.*,pd.full_name,s.tittle,s.file")
						->join("personal_data pd", "pd.id_user = p.id_user", "left")
						->join("submission s","s.id_submission=p.id_submission","left")
						->where("p.id_submission",$id_submission)
						->where("status_plag",1)
						->get("plagiarism p");

		return $res->result_array();
	}
	public function find_revision_plagiarism($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("status_plagiarism",2)
						->get("submission");
		return $res->row_array();
	}
	public function count_revision($id_submission){
		$plagiarism = $this->db->select("count(*) as jml")
								->where("id_submission",$id_submission)
								->get("plagiarism")
								->row_array();
		$review = $this->db->select("count(*) as jml")
							->where("id_submission",$id_submission)
							->where('review_as',2)
							->get("review")
							->row_array();
		return $plagiarism['jml']+$review['jml'];
	}
	public function save_revision_plagiarism($data){
		$res = $this->db->where("id_submission",$data['id_submission'])
						->set("status_plagiarism",0)
						->set("status",0)
						->update('submission',$data);
		return $res;
	}
	/*--------------- End Plagiarism ---------------*/

	/*--------------- Start Reviewer ---------------*/
	public function get_reviewer($id_submission){
		$res = $this->db->select("r.*,pd.full_name,s.tittle,s.file")
						->join("personal_data pd", "pd.id_user = r.id_user", "left")
						->join("submission s","s.id_submission=r.id_submission","left")
						->where("r.id_submission",$id_submission)
						->where("status_review",1)
						->where('review_as',2)
						->get("review r");
		return $res->result_array();
	}
	public function find_revision_reviewer($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->where("status_reviewer",2)
						->get("submission");
		return $res->row_array();
	}
	public function save_revision_reviewer($data){
		$res = $this->db->where("id_submission",$data['id_submission'])
						->set("status_reviewer",0)
						->set("status",0)
						->update('submission',$data);
		return $res;
	}
	public function get_detail_review($id_submission){
		$res = $this->db->select("r.*,pd.full_name,s.tittle,s.file")
						->join("personal_data pd", "pd.id_user = r.id_user", "left")
						->join("submission s","s.id_submission=r.id_submission","left")
						->where("r.id_submission",$id_submission)
						->where("status_review",1)
						->where('review_as',2)
						->get("review r");

		return $res->result_array();
	}
	/*--------------- End Reviewer ---------------*/

	/*--------------- Start Token ---------------*/
	function add_token($id_user){
        $token = random_string();
        $find = $this->db->where("token",$token)->get("token")->row_array();
        while($find){
            $token = random_string();
            $find = $this->db->where("token",$token)->get("token")->row_array();
        }

        $res = $this->db->insert("token",array(
            "id_user" => $id_user,
            "token" => $token,
            "time_expired" => 99999999
        ));

        return array("res" => $res, "token" => $token);
    }
    function find_token($token){
        $res = $this->db->select("t.*, TIME_TO_SEC(TIMEDIFF(NOW(),(t.create_at))) as diff")
                        ->where("t.token",$token)
                        ->join("user u","t.id_user = u.id_user","left")
                        ->having("diff <= t.time_expired")
                        ->get("token t");
        return $res->row_array();
    }
    /*--------------- End Token ---------------*/

    /*--------------- Start Submit ---------------*/
	public function submit_paper($data){
		$date = date('Y-m-d');
		$res = $this->db->where('id_submission', $data['id_submission'])
						->set('date', $date)
						->set('status',6)
						->update('submission', $data);
		return $res;
	}
	public function get_ready_submit_paper(){
		$res = $this->db->where_in('status',array(1,4,6))
						->where('id_user',$this->session->userdata('id_user'))
						->get('submission');
		return $res->result_array();
	}
	public function find_ready_submit_paper($id_submission){
		$res = $this->db->join('personal_data pd','pd.id_user=s.id_user')
						->where_in('status',array(1,4,6))
						->where('s.id_user',$this->session->userdata('id_user'))
						->where('id_submission',$id_submission)
						->get('submission s');
		return $res->row_array();
	}

	/*--------------- End Submit ---------------*/
}

?>