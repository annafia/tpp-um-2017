<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminder_m extends CI_Model {
	public function plag($days){
		$res = $this->db->select("p.*, s.tittle, u.email, pd.full_name, datediff(NOW(),(p.create_at)) as diff")
						->join("submission s","s.id_submission = p.id_submission","left")
						->join("user u","u.id_user = p.id_user","left")
						->join("personal_data pd","pd.id_user = p.id_user","left")
						->having("diff",$days)
						->where("p.status_plag", 0)
						->get("plagiarism p");
		return $res->result_array();
	}
	public function rev($days){
		$res = $this->db->select("r.*, s.tittle, u.email, pd.full_name, datediff(NOW(),(r.create_at)) as diff")
						->join("submission s","s.id_submission = r.id_submission","left")
						->join("user u","u.id_user = r.id_user","left")
						->join("personal_data pd","pd.id_user = r.id_user","left")
						->having("diff",$days)
						->where("r.status_review",0)
						->where('review_as',1)
						->get("review r");
		return $res->result_array();
	}
	public function trans($days){
		$res = $this->db->select("t.*, s.tittle, u.email, pd.full_name, datediff(NOW(),(t.create_at)) as diff")
						->join("submission s","s.id_submission = t.id_submission","left")
						->join("user u","u.id_user = t.id_user","left")
						->join("personal_data pd","pd.id_user = t.id_user","left")
						->having("diff",$days)
						->where("t.status_translation",0)
						->get("translation t");
		return $res->result_array();
	}
	public function finish($days){
		$res = $this->db->select("s.tittle,s.id_user,s.id_submission,u.email,pd.full_name,, datediff(NOW(),(t.finish_at)) as diff")
						->join("user u","u.id_user = s.id_user","left")
						->join("personal_data pd","pd.id_user = s.id_user","left")
						->join("translation t","t.id_submission=s.id_submission","left")
						->where("status",1)
						->having("diff",$days)
						->get("submission s");
		return $res->result_array();
	}
	public function plag_expired($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->delete("plagiarism");
		$res = $this->db->where("id_submission",$id_submission)
						->set("status_plagiarism",0)
						->set("notif_plag",0)
						->update("submission");
		return $res;
	}
	public function rev_expired($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->delete("revision");
		$res = $this->db->where("id_submission",$id_submission)
						->set("status_reviewer",0)
						->set("notif_rev",0)
						->update("submission");
		return $res;
	}
	public function trans_expired($id_submission){
		$res = $this->db->where("id_submission",$id_submission)
						->delete("translation");
		$res = $this->db->where("id_submission",$id_submission)
						->set("status_translator",0)
						->set("notif_trans",0)
						->update("submission");
		return $res;
	}
	public function get_admin(){
		$sql = "select *from data_privilage dp left outer join user u on dp.id_user = u.id_user left outer join personal_data p on u.id_user=p.id_user where dp.privilage_id =2 AND dp.status = 1";
		$res=$this->db->query($sql);

		return $res->result_array();
	}
}

?>