<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi_m extends CI_Model {
	public function get_prodi(){
		$sql = 'select distinct(id_prodi), p.*,j.jurusan,f.fakultas from prodi p 
					left outer join jurusan j ON floor(id_prodi/100) = floor(id_jurusan/100)
					left outer join fakultas f on floor(id_prodi/1000) = floor(id_fakultas/1000)';
		$res = $this->db->query($sql);

		return $res->result_array();
	}
	public function get_jurusan(){
		$sql = 'select distinct(id_jurusan), j.*, f.fakultas from jurusan j 
					left outer join fakultas f on floor(id_jurusan/1000) = floor(id_fakultas/1000)';
		$res = $this->db->query($sql);

		return $res->result_array();
	}
	public function get_fakultas(){
		$res = $this->db->get('fakultas');
		return $res->result_array();
	}
	public function find_fakultas($id_fakultas){
		$res = $this->db->where("id_fakultas",$id_fakultas)
						->get('fakultas');
		return $res->row_array();
	}
	public function find_jurusan($id_jurusan){
		$res = $this->db->where("id_jurusan",$id_jurusan)
						->get('jurusan');
		return $res->row_array();
	}
	public function find_prodi($id_prodi){
		$res = $this->db->where("id_prodi",$id_prodi)
						->get('prodi');
		return $res->row_array();
	}
	public function create_fakultas($data){
		$res = $this->db->insert('fakultas',$data);

		return $res;
	}
	public function create_jurusan($data){
		$res = $this->db->insert('jurusan',$data);
		
		return $res;
	}
	public function create_prodi($data){
		$res = $this->db->insert('prodi',$data);
		
		return $res;
	}
	public function edit_fakultas($data){
		$res = $this->db->where('id_fakultas',$data['id_fakultas'])
						->update('fakultas',$data);
		return $res;
	}
	public function edit_jurusan($data){
		$res = $this->db->where('id_jurusan',$data['id_jurusan'])
						->update('jurusan',$data);
		return $res;
	}
	public function edit_prodi($data){
		$res = $this->db->where('id_prodi',$data['id_prodi'])
						->update('prodi',$data);
		return $res;
	}
	public function del_fakultas($id_fakultas){
		$res = $this->db->where('id_fakultas',$id_fakultas)
						->delete('fakultas');
		return $res;
	}
	public function del_jurusan($id_jurusan){
		$res = $this->db->where('id_jurusan',$id_jurusan)
						->delete('jurusan');
		return $res;
	}
	public function del_prodi($id_prodi){
		$res = $this->db->where('id_prodi',$id_prodi)
						->delete('prodi');
		return $res;
	}
}

?>