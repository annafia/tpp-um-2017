<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topic_m extends CI_Model {
	public function find($id_topic){
		$res = $this->db->where('id_topic', $id_topic)->get('topic');
		return $res->row_array();
	}	

	public function get(){
		$res = $this->db->get('topic');
		return $res->result_array();
	}

	public function create($data){
		$res = $this->db->insert('topic', $data);
		return $res;
	}

	public function update($where, $data){
		$this->db->update('topic', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete($id_topic){
		$res = $this->db->where('id_topic', $id_topic)
						->delete('topic');
		return $res;
	}
}

?>