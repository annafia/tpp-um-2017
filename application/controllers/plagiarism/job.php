<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('plagiarism/job_m');
       	$this->load->model('notification/notification_m','nm');
		$this->load->language('general');
		$this->load->language('job');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',2)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>2,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }
    /*------------------------------ Start Local Function ------------------------------*/
    public function validation_plag(){
		$this->form_validation->set_rules('persen_plag', lang('persentasi_plag') ,'trim|numeric|required');

		return $this->form_validation->run()!=FALSE;
	}
	function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	function admin_notif_paper($data){
		$admin=$this->nm->get_admin();

		foreach ($admin as $a) {
			$data['user'] = $a;
			$content = $this->load->view("content/notification/paper",$data,TRUE);

		    $this->email->to($a['email']);
		    $this->email->from("tppum2017@gmail.com", 'TPP UM');
		    $this->email->subject("Notifikasi ".$data['subject']);
		    $this->email->message($content);

		    $this->email->send();
		}
	}
	/*------------------------------ End Local Function ------------------------------*/

	public function index(){
		$id_user = $this->session->userdata('id_user');
		$get_not = $this->job_m->get_not($id_user);
		$get_finish = $this->job_m->get_finish($id_user);

		$this->load->view('content/plagiarism/index', array(
			'get_not' => $get_not,
			'get_finish' => $get_finish
		));
	}

	public function save(){
		$data = $this->input->post();

		/* Save Button */
		$submit = $data['submit'];
		unset($data['submit']);

		$redirect = base_url("plagiarism/job");
		$this->is_success(isset($_FILES['plagiarism_file']['name']), $redirect, lang('file_harus_diisi'));
		$this->is_success($this->validation_plag(),$redirect,validation_errors());

		$find = $this->job_m->find($data['id_plagiarism']);
		$this->is_success($find,$redirect,lang("not_find"));

		$count = $this->job_m->count_plag_subm($data['id_submission']);

		/*--------------------- Start Upload File -----------------------*/
		$this->load->library('upload');
        $config['upload_path'] = "./dist/plagiarism/";
        $config['allowed_types'] = 'doc|docx|pdf';
        $config['max_size'] = '5120';
        $config['file_name'] = "TPP_Plag".$count['jml']."By".$find['full_name']."_".generate_id($data['id_submission'])."_".$find['full_name'];

        $this->upload->initialize($config);
        $upload = $this->upload->do_upload('plagiarism_file');
		$this->is_success($upload,$redirect,$this->upload->display_errors());
        $file = $this->upload->data();
        unlink("./dist/plagiarism/".$find['plagiarism_file']);
        /*---------------------- End Upload File -------------------------*/

        $data['plagiarism_file']=$file['file_name'];

		$save = $this->job_m->save($data);
		$this->is_success($save,$redirect,lang("failed"));

		/* ----------- Send to Admin -----------*/
		if($submit==1){
			$send = $this->job_m->send($data['id_plagiarism']);
			$this->is_success($send,$redirect,lang("failed"));
			/*------------------- Start Notifikasi Email -------------------*/
			$subm = $this->nm->find_submission($data['id_submission']);

			$body_admin = "Pemeriksaan plagiarism artikel dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name']." telah selesai.";

			$this->admin_notif_paper(array(
				"subm" => $subm,
				"body" => $body_admin,
				"subject" => "Pemeriksaan Plagiasi Selesai !"
			));
			/*------------------- End Notifikasi Email -------------------*/
		}

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
}
