<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('translator/job_m');
		$this->load->language('general');
		$this->load->language('job');
		$this->load->helper(array('form', 'url'));
		$this->load->model('notification/notification_m','nm');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',5)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>5,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }
    function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	function notif_finish_trans($id_submission){
		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);

		$body = "Penerjemahan paper dengan judul ".$subm['tittle']." telah selesai.";

		$this->admin_notif_paper(array(
			"subm" => $subm,
			"body" => $body,
			"subject" => "Translasi Artikel Selesai !"
		));
		/* End Notifikasi Email */
	}
	public function index(){
		$get_not = $this->job_m->get_not();
		$get_finish = $this->job_m->get_finish();
		$this->load->view('content/translator/index', array(
			'get_not' => $get_not,
			'get_finish' => $get_finish
		));
	}
	public function save_translate(){
		$data = $this->input->post();
		$redirect = base_url('translator/job');
		$submit = $data['submit']; unset($data['submit']);

		$find = $this->job_m->find($data['id_submission']);
		$this->is_success($find,$redirect,lang('not_find'));

		if($submit == 2){
			$save_submit = $this->job_m->submit($data['id_submission']);
			$this->is_success($save_submit,$redirect,lang('failed'));

			$this->notif_finish_trans($data['id_submission']);

			$this->session->set_flashdata('successMessage', lang('success'));
			redirect($redirect);
		}

		$this->is_success(isset($_FILES['translation_file']['name']),$redirect,'File translasi harus diisi !');

		$author = $this->job_m->find_author($data['id_submission']);

		$nmfile = "TPP_TransBy".$find['full_name']."_".generate_id($data['id_submission'])."_".$author['full_name'];
        $config['upload_path'] = './dist/translation';
        $config['allowed_types'] = 'docx|doc|pdf';
        $config['max_size'] = '10240';
        $config['overwrite'] = true;
        $config['file_name'] = $nmfile;

        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->is_success($this->upload->do_upload('translation_file'),$redirect,$this->upload->display_errors());
        
        $file = $this->upload->data();
        $data['translation_file']=$file['file_name'];

        $save = $this->job_m->save($data);

        if($submit == 1){
			$save_submit = $this->job_m->submit($data['id_submission']);
			$this->is_success($save_submit,$redirect,lang('failed'));

			$this->notif_finish_trans($data['id_submission']);
		}

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	function admin_notif_paper($data){
		$admin=$this->nm->get_admin();

		foreach ($admin as $a) {
			$data['user'] = $a;
			$content = $this->load->view("content/notification/paper",$data,TRUE);

		    $this->email->to($a['email']);
		    $this->email->from("tppum2017@gmail.com", 'TPP UM');
		    $this->email->subject($data['subject']);
		    $this->email->message($content);

		    $this->email->send();
		}
	}
}