<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('prodi/prodi_m','pm');       	
		$this->load->language('general');
		$this->load->language('prodi');

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }
    /*------------------------------ Start Local Function ------------------------------*/
	function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	public function validation_prodi(){
		$this->form_validation->set_rules('id_prodi', lang('id_prodi') ,'trim|required|is_unique[prodi.id_prodi]');
		$this->form_validation->set_rules('program_studi', lang('program_studi') ,'trim|required');

		return $this->form_validation->run()!=FALSE;
	}
	public function validation_jurusan(){
		$this->form_validation->set_rules('id_jurusan', lang('id_jurusan') ,'trim|required|is_unique[jurusan.id_jurusan]');
		$this->form_validation->set_rules('jurusan', lang('nama_jurusan') ,'trim|required');

		return $this->form_validation->run()!=FALSE;
	}
	public function validation_fakultas(){
		$this->form_validation->set_rules('id_fakultas', lang('id_fakultas') ,'trim|required|is_unique[fakultas.id_fakultas]');
		$this->form_validation->set_rules('fakultas', lang('nama_fakultas') ,'trim|required');

		return $this->form_validation->run()!=FALSE;
	}
	/*------------------------------ End Local Function ------------------------------*/
	public function index(){
		$prodi = $this->pm->get_prodi();
		$fakultas = $this->pm->get_fakultas();

		$this->load->view('content/prodi/index', array(
			'prodi' => $prodi,
			'fakultas' => $fakultas,
		));
	}
	public function jurusan(){
		$jurusan = $this->pm->get_jurusan();
		$this->load->view('content/prodi/jurusan', array(
			'jurusan' =>$jurusan
		));
	}
	public function fakultas(){
		$fakultas = $this->pm->get_fakultas();
		$this->load->view('content/prodi/fakultas', array(
			'fakultas' =>$fakultas
		));
	}
	public function create_fakultas(){
		$data = $this->input->post();
		$redirect = base_url('master/prodi/fakultas');
		$this->is_success($this->validation_fakultas(),$redirect,validation_errors());

		$create = $this->pm->create_fakultas($data);
		$this->is_success($create,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function create_jurusan(){
		$data = $this->input->post();
		$redirect = base_url('master/prodi/jurusan');
		$this->is_success($this->validation_jurusan(),$redirect,validation_errors());

		$create = $this->pm->create_jurusan($data);
		$this->is_success($create,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function create_prodi(){
		$data = $this->input->post();
		$redirect = base_url('master/prodi');
		$this->is_success($this->validation_prodi(),$redirect,validation_errors());

		$create = $this->pm->create_prodi($data);
		$this->is_success($create,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function edit_fakultas(){
		$data = $this->input->post();
		$redirect = base_url('master/prodi/fakultas');

		$find = $this->pm->find_fakultas($data['id_fakultas']);
		$this->is_success($find,$redirect,lang('not_find'));

		$edit = $this->pm->edit_fakultas($data);
		$this->is_success($edit,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function edit_jurusan(){
		$data = $this->input->post();
		$redirect = base_url('master/prodi/jurusan');

		$find = $this->pm->find_jurusan($data['id_jurusan']);
		$this->is_success($find,$redirect,lang('not_find'));

		$edit = $this->pm->edit_jurusan($data);
		$this->is_success($edit,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function edit_prodi(){
		$data = $this->input->post();
		$redirect = base_url('master/prodi');

		$find = $this->pm->find_prodi($data['id_prodi']);
		$this->is_success($find,$redirect,lang('not_find'));

		$edit = $this->pm->edit_prodi($data);
		$this->is_success($edit,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function del_fakultas($id_fakultas){
		$redirect = base_url('master/prodi/fakultas');

		$find = $this->pm->find_fakultas($id_fakultas);
		$this->is_success($find,$redirect,lang('not_find'));

		$del = $this->pm->del_fakultas($id_fakultas);
		$this->is_success($del,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function del_jurusan($id_jurusan){
		$redirect = base_url('master/prodi/jurusan');

		$find = $this->pm->find_jurusan($id_jurusan);
		$this->is_success($find,$redirect,lang('not_find'));

		$del = $this->pm->del_jurusan($id_jurusan);
		$this->is_success($del,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function del_prodi($id_prodi){
		$redirect = base_url('master/prodi');

		$find = $this->pm->find_prodi($id_prodi);
		$this->is_success($find,$redirect,lang('not_find'));

		$del = $this->pm->del_prodi($id_prodi);
		$this->is_success($del,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
}
