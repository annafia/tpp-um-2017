<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privilage extends CI_Controller {
	public function __construct(){
       parent::__construct();       
        $this->load->model('privilage/privilage_m');	
		$this->load->language('general');
		$this->load->language('privilage');

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

	public function index(){
		$privilage = $this->privilage_m->get();

		$this->load->view('content/privilage/index', array(
			'privilage' => $privilage
		));
	}

	public function create() {
		$data = $this->input->post();
		$create = $this->privilage_m->create($data);
		if($create > 0){
			$this->session->set_flashdata('successMessage', lang('success'));
			redirect('master/privilage');
		}else{
			$this->session->set_flashdata('errorMessage', lang('failed'));
			redirect('master/privilage');
		}
	}

	public function update(){
		$data = array(
				'privilage_id' => $this->input->post('privilage_id'),
				'privilage_nm' => $this->input->post('privilage_nm')
		);
		$find = $this->privilage_m->find($data['privilage_id']);
		if($find){
			$update = $this->privilage_m->update(array('privilage_id' => $this->input->post('privilage_id')), $data);
			if($update > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('master/privilage');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('master/privilage');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('master/privilage');
		}
		
	}

	public function delete($privilage_id){
		$find = $this->privilage_m->find($privilage_id);
		if($find){
			$delete = $this->privilage_m->delete($privilage_id);
			if($delete > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('master/privilage');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('master/privilage');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('master/privilage');
		}
	}
}

