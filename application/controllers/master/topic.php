<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topic extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('topic/topic_m');       	
		$this->load->language('general');
		$this->load->language('topic');

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

	public function index(){
		$topic = $this->topic_m->get();

		$this->load->view('content/topic/index', array(
			'topic' => $topic
		));
	}

	public function create() {
		$data = $this->input->post();
		$create = $this->topic_m->create($data);
		if($create > 0){
			$this->session->set_flashdata('successMessage', lang('success'));
			redirect('master/topic');
		}else{
			$this->session->set_flashdata('errorMessage', lang('failed'));
			redirect('master/topic');
		}
	}

	public function update(){
		$data = array(
				'id_topic' => $this->input->post('id_topic'),
				'nm_topic' => $this->input->post('nm_topic')
		);
		$find = $this->topic_m->find($data['id_topic']);
		if($find){
			$update = $this->topic_m->update(array('id_topic' => $this->input->post('id_topic')), $data);
			if($update > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('master/topic');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('master/topic');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('master/topic');
		}
		
	}

	public function delete($id_topic){
		$find = $this->topic_m->find($id_topic);
		if($find){
			$delete = $this->topic_m->delete($id_topic);
			if($delete > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('master/topic');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('master/topic');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('master/topic');
		}
	}
}
