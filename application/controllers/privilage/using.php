<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Using extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('use/use_m');       	
		$this->load->language('general');
		$this->load->language('use');
		$this->load->model('notification/notification_m');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

	public function index(){
		$use = $this->use_m->get();
		$proof = $this->use_m->proof();

		$this->load->view('content/privilage/use', array(
			'use' => $use,
			'proof' => $proof
		));
	}

	public function access($id_user){
		$find = $this->use_m->find_user($id_user);
		if($find){
			$get = $this->use_m->find_user($id_user);
			$access = $this->use_m->access($id_user);
			$get_privilage = $this->use_m->get_privilage();

			$find_prev = $this->use_m->find_prev($id_user);
			$select_prev = array();
			foreach($find_prev as $row) {
				$select_prev[] = $row['privilage_id'];
			}

			$find_role = $this->use_m->find_role($id_user);

			$select_role = array();
			foreach($find_role as $row) {
				$select_role[] = $row['role'];
			}

			$this->load->view('content/privilage/access', array(
				'get' => $get,
				'access' => $access,
				'get_privilage' => $get_privilage,
				'select_prev' => $select_prev,
				'select_role' => $select_role
			));
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('privilage/using');
		}
	}

	public function detail($id_user){
		$find = $this->use_m->find_user($id_user);
		if($find){
			$get = $this->use_m->find_user($id_user);
			$access = $this->use_m->access($id_user);
			$get_privilage = $this->use_m->get_privilage();

			$find_prev = $this->use_m->find_prev($id_user);
			$select_prev = array();
			foreach($find_prev as $row) {
				$select_prev[] = $row['privilage_id'];
			}

			$find_role = $this->use_m->find_role($id_user);

			$select_role = array();
			foreach($find_role as $row) {
				$select_role[] = $row['role'];
			}

			$this->load->view('content/privilage/access', array(
				'get' => $get,
				'access' => $access,
				'get_privilage' => $get_privilage,
				'select_prev' => $select_prev,
				'select_role' => $select_role
			));
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('privilage/using');
		}
	}

	public function create() {
		$id_user = $this->input->post('id_user');
		$role = $this->input->post('role');
		$this->db->where('id_user', $id_user)->delete('data_privilage');
		$privilage_id = $this->input->post('privilage_id');
		if($privilage_id){
			foreach($privilage_id as $privilage){
				$data_prev = array(
					'id_user' => $id_user,
					'privilage_id' => $privilage
				);
				$create = $this->use_m->create($data_prev);
				$update_role = $this->use_m->update_role($id_user, $role);
			}
			if($create > 0){
				/* Start Notifikasi Email */
				$user = $this->notification_m->get_current_user($id_user);

				$body = "Hak akses anda telah diubah. Silahkan login ke akun anda untuk memeriksa perubahan hak akses yang telah dilakukan.";

				$this->current_user_notif_privilage(array(
					"user" => $user,
					"body" => $body,
					"subject" => "Perubahan Hak Akses"
				));
				/* End Notifikasi Email */

				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('privilage/using/access/'.$id_user);
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('privilage/using/access/'.$id_user);
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('failed'));
			redirect('privilage/using/access', $id_user);
		} 
	}

	public function create_detail() {
		$id_user = $this->input->post('id_user');
		$role = $this->input->post('role');
		$this->db->where('id_user', $id_user)->delete('data_privilage');
		$privilage_id = $this->input->post('privilage_id');
		if($privilage_id){
			foreach($privilage_id as $privilage){
				$data_prev = array(
					'id_user' => $id_user,
					'privilage_id' => $privilage
				);
				$create = $this->use_m->create($data_prev);
				$update_role = $this->use_m->update_role($id_user, $role);
			}
			if($create > 0){
				/* Start Notifikasi Email */
				$user = $this->notification_m->get_current_user($id_user);

				$body = "Hak akses anda telah diubah. Silahkan login ke akun anda untuk memeriksa perubahan hak akses yang telah dilakukan.";

				$this->current_user_notif_privilage(array(
					"user" => $user,
					"body" => $body,
					"subject" => "Perubahan Hak Akses"
				));
				/* End Notifikasi Email */

					$this->session->set_flashdata('successMessage', lang('success'));
					redirect('privilage/using');
				}else{
					$this->session->set_flashdata('errorMessage', lang('failed'));
					redirect('privilage/using');
				}
		}else{
			$this->session->set_flashdata('errorMessage', lang('failed'));
			redirect('privilage/using');
		} 
	}

	public function update(){
		$data = array(
				'use_id' => $this->input->post('use_id'),
				'use_nm' => $this->input->post('use_nm')
		);
		$find = $this->use_m->find($data['use_id']);
		if($find){
			$update = $this->use_m->update(array('use_id' => $this->input->post('use_id')), $data);
			if($update > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('privilage/using');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('privilage/using');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('privilage/using');
		}
		
	}

	public function delete($id_user){
		$find = $this->use_m->find($id_user);
		if($find){
			$delete = $this->use_m->delete($id_user);
			if($delete > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('privilage/using');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('privilage/using');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('privilage/using');
		}
	}

	function current_user_notif_privilage($data){
		$content = $this->load->view("content/notification/privilage",$data,TRUE);

	    $this->email->to($data['user']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject("Notifikasi ".$data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
}
