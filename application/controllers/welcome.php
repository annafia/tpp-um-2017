<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
        parent::__construct();      
        $this->load->language('general');
        $this->load->model('auth_m');
        $this->load->language('notification');
		$this->load->library("email");
    }
    public function is_success($res,$error,$redirect){
    	if(!$res){
    		$this->session->set_flashdata('errorMessage', $error);
    		redirect($redirect);
    	}
    }
	private function email_validation(){
		$this->form_validation->set_rules('email', "Email", 'required');

		return $this->form_validation->run()!=FALSE;
	}

	public function index(){
		if($this->session->userdata('email')){
			$this->redirect->guest('dashboard');
		}else{
			$this->load->view('auth/login');
		}
	}
	public function forgot_password(){
		$this->load->view("auth/forgot_password");
	}
	public function send_reset_password(){
		$redirect = base_url("welcome/send_reset_password");
		$email = $this->input->post("email");
		$this->is_success($this->email_validation(),validation_errors(),$redirect);

		$find = $this->auth_m->find_email($email);
		$this->is_success($find,"Email Tidak Terdaftar !",base_url('welcome/forgot_password'));

		$add = $this->auth_m->add_token($find['id_user']);
		$this->is_success($add['res'],lang("failed"),$redirect);

		/* Start Notifikasi Email */
		$body = "Terimakasih telah menggunakan layanan TPP UM! <br><br>Klik pada tautan dibawah ini untuk mengatur kembali password Anda! Segera atur kembali password Anda, karena tautan reset password hanya berlaku 10 menit dimulai dari saat pesan ini terkirim! Tautan hanya dapat digunakan Satu Kali!";

		$this->notif_reset_password(array(
			"email" => $find['email'],
			'full_name' => $find['full_name'],
			'token' => $add['token'],
			"body" => $body,
			"subject" => "Reset Password !"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang("success_reset"));
		redirect(base_url());
	}
	public function reset_password($token){
		$redirect = base_url();

		$find = $this->auth_m->find_token($token);
		$this->is_success($find,lang("token_expired"),$redirect);

		$this->load->view("auth/reset_password",array(
			"user" => $find
		));
	}
	public function reset(){
		$redirect = base_url();

		$data = $this->input->post();
		$this->is_success($data['password']!="",lang("failed"),$redirect);

		$find = $this->auth_m->find_id_user($data['token']);

		$reset = $this->auth_m->reset_password($find['id_user'],$data['password']);
		$this->is_success($reset,lang("failed"),base_url("welcome/reset_password/".$data['token']));

		$deactive = $this->auth_m->deactive_token($token);

		$this->session->set_flashdata('successMessage', lang("success_reset_pass"));
		echo $data['token'];
		redirect(base_url());
	}

	function notif_reset_password($data){
		$content = $this->load->view("content/notification/reset_password",$data,TRUE);

	    $this->email->to($data['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	public function submission_verification($token,$id_submission){
		$redirect = base_url();

		$find_article = $this->auth_m->find_submission($id_submission);
		$this->is_success($find_article,lang("not_find"),$redirect);

		$find = $this->auth_m->find_token($token);
		$this->is_success($find,lang("token_expired"),$redirect);

		$this->is_success($find['id_user']==$find_article['id_user'],lang("not_find"),$redirect);

		$verify = $this->auth_m->verify($id_submission);
		$this->is_success($find_article,lang("not_find"),$redirect);

		$this->auth_m->deactive_token($token);

		$this->load->view("auth/success_submission_verification",array(
			"subm" => $find_article
		));
	}

	public function cek_login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$table = 'user';	
		$where = array(
			'email' => $email,
			'password' => md5($password),
			'active' => 1
		);
		$cek = $this->auth_m->cek_login_peserta($table,$where)->row_array();
		if($cek){
			$data_session = array(
				'email' => $email,
				'role' => $cek['role'],
				'id_user' => $cek['id_user'],
				'position' => $cek['position']
			);

			$cek = $this->auth_m->log($email);
			
			$this->session->set_userdata($data_session);
			$this->redirect->intended('dashboard/index');
		}else{
			$this->session->set_flashdata("post",$this->input->post());
			$this->session->set_flashdata('errorMessage', lang('invalid_login'));
			$this->redirect->guest();
		}
	}


	public function register(){
		$this->load->view('auth/register');
	}

	public function register_save(){
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$repassword = $this->input->post('repassword');
		$record = array(
			'nama' => $nama,
			'email' => $email,
			'password' => md5($password),
			'repassword' => $repassword
		);

		$this->auth_m->insert($record);
		$this->redirect->with('successMessage', 'Data berhasil disimban')->to('welcome');
	}

	public function logout($id_user){
		$cek = $this->auth_m->logout($id_user);
		$this->session->sess_destroy();
		redirect(base_url('welcome'));
	}
}
