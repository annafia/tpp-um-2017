<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submission extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->helper('back_logout');
		$this->load->helper(array('form', 'url'));
		$this->load->helper('directory');
		$this->load->helper('notification');
		backButtonHandle();

       	$this->load->model('paper/submission_m','sm');
       	$this->load->model('notification/notification_m');
		$this->load->language('general');
		$this->load->language('submission');
		$this->load->language('notification');

		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',4)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>4,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

    /*------------------------------ Start Local Function ------------------------------*/
    public function validation_paper(){
		$this->form_validation->set_rules('tittle', lang('judul') ,'required');
		$this->form_validation->set_rules('abstract', lang('abstrak') ,'required');
		$this->form_validation->set_rules('id_topic', lang('topik') ,'required');
		$this->form_validation->set_rules('keyword', lang('kata_kunci') ,'required');
	}
	function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	function admin_notif_paper($data){
		$admin=$this->notification_m->get_admin();

		foreach ($admin as $a) {
			$data['user'] = $a;
			$content = $this->load->view("content/notification/paper",$data,TRUE);

		    $this->email->to($a['email']);
		    $this->email->from("tppum2017@gmail.com", 'TPP UM');
		    $this->email->subject($data['subject']);
		    $this->email->message($content);

		    $this->email->send();
		}
	}
	function current_user_notif_paper($data){
		$content = $this->load->view("content/notification/paper",$data,TRUE);

	    $this->email->to($data['user']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	function notif_submission_verification($data){
		$content = $this->load->view("content/notification/submission_verification",$data,TRUE);

	    $this->email->to($data['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	function notif_reminder($data){
		$content = $this->load->view("content/notification/reminder",$data,TRUE);

	    $this->email->to($data['reminder']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	public function upload_paper($file_name,$path,$field,$redirect){
		$this->load->library('upload');

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'docx|doc|pdf';
        $config['max_size'] = '10240';
        $config['file_name'] = $file_name;

        $this->upload->initialize($config);
        $this->is_success($this->upload->do_upload($field),$redirect,$this->upload->display_errors());
        $file = $this->upload->data();

        return $file['file_name'];
	}
	/*------------------------------ End Local Function ------------------------------*/

	/*------------------------------ Start Artikel ------------------------------*/
	public function index(){
		$this->load->view('content/paper/index', array(
			'get' => $this->sm->get()
		));
	}
	public function update($id_submission){
		$redirect = base_url('paper/submission/detail/'.$id_submission);

		$find = $this->sm->find_updatable_submission($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$this->load->view('content/paper/update', array('sub' => $find));
	}
	public function save_update(){
		$data = $this->input->post();
		$success = base_url('paper/submission/show_detail/'.$data['id_submission']);
		$failed = base_url('paper/submission/update/'.$data['id_submission']);

		$find = $this->notification_m->get_current_user($this->session->userdata("id_user"));
		$find_subm = $this->sm->find_updatable_submission($data['id_submission']);
		$this->is_success($find_subm,$failed,lang("not_find"));

		$this->validation_paper();
		$this->is_success($this->form_validation->run()===TRUE, $failed, validation_errors());
		$this->is_success(isset($_FILES['file']['name']), $failed, lang('file_harus_diisi'));

        $save = $this->sm->save_update($data);
        $this->is_success($save, $failed, lang("failed"));

        unlink("./dist/submissions/".$find_subm['file']);

        $file_name = "TPP_FP"."_".generate_id($data['id_submission'])."_".$find['full_name'];
        $data['file']=$this->upload_paper($file_name,"./dist/submissions/","file",$failed);
        $save_file = $this->sm->save_file($data['id_submission'],$data['file']);

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($success);
	}
	public function create(){
		$this->load->view('content/paper/create');
	}
	public function del($id_submission){
		$failed = base_url('paper/submission/show_detail/'.$id_submission);
		$success = base_url('paper/submission/');

		$find = $this->sm->find_updatable_submission($id_submission);
		$this->is_success($find,$success,lang("not_find"));

		unlink('./dist/submissions/'.$find['file']);

		$del = $this->sm->delete($id_submission);
        $this->is_success($del, $failed, lang("failed"));

        $this->session->set_flashdata('successMessage', lang('success'));
		redirect($success);
	}
	public function show_detail($id_submission){
		$redirect = base_url("paper/submission/");
		
		$find = $this->sm->find($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$this->load->view("content/paper/detail", array(
			"submission" => $find,
			"member" => $this->sm->get_member($id_submission),
			"plagiarism" => $this->sm->get_plagiarism($id_submission),
			'hst_review' => $this->sm->get_detail_review($id_submission),
			'trans' => $this->sm->find_translasi($id_submission),
		));
	}
	public function save() {
		$failed = base_url('paper/submission/create');
		$this->session->set_flashdata("post",$this->input->post());

		$this->validation_paper();
		$this->is_success($this->form_validation->run()===TRUE, $failed, validation_errors());
		$this->is_success(isset($_FILES['file']['name']), $failed, lang('file_harus_diisi'));

		$data = $this->input->post();
		$find = $this->notification_m->get_current_user($this->session->userdata("id_user"));
		
        $data['id_user']=$this->session->userdata("id_user");

        $create = $this->sm->create($data);
        $this->is_success($create['res'], $failed, lang("failed"));

        $file_name = "TPP_FP"."_".generate_id($create['id_submission'])."_".$find['full_name'];
        $data['file']=$this->upload_paper($file_name,"./dist/submissions/","file",$failed);
        $save_file = $this->sm->save_file($create['id_submission'],$data['file']);

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect(base_url('paper/submission/show_detail/'.$create['id_submission']));
	}
	public function send_verification($id_submission){
		$redirect = base_url('paper/submission/show_detail/'.$id_submission);
		
		$find = $this->sm->find_unverified_submission($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$mentor = $this->sm->find_first_mentor($id_submission);
		$other_mentor = $this->sm->find_other_mentor($id_submission);
		$this->is_success($mentor,$redirect,lang("not_find"));

		$add = $this->sm->add_token($find['id_user']);
		$this->is_success($add['res'],$redirect,lang("failed"));

		/* Start Notifikasi Email */
		$body = $find['full_name']." telah meminta persetujuan Anda untuk memproses artikelnya yang berjudul <i>'".$find['tittle']."'</i> di Tim Percepatan Publikasi (TPP) Universitas Negeri Malang dengan Anda sebagai Pembimbing.";

		$this->notif_submission_verification(array(
			"id_submission" => $id_submission,
			"email" => $mentor['member_email'],
			'full_name' => $mentor['member_name'],
			'token' => $add['token'],
			"body" => $body,
			"subject" => "Persetujuan Pembimbing !"
		));
		foreach ($other_mentor as $om) {
			$om['full_name'] = $om['member_name'];
			$om['email'] = $om['member_email'];
			$body = $find['full_name']." telah mencantumkan Anda sebagai pembimbing artikelnya yang berjudul <i>'".$find['tittle']."'</i> di Tim Percepatan Publikasi (TPP) Universitas Negeri Malang.";

			$this->notif_reminder(array(
				"reminder" => $om,
				"body" => $body,
				"subject" => "Notifikasi Pembimbing Artikel"
			));
		}
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang("success_send_verify_submission"));
		redirect($redirect);
	}
	public function send_to_admin($id_submission){
		$redirect = base_url('paper/submission/show_detail/'.$id_submission);
		
		$find = $this->sm->find_verified_submission($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$send = $this->sm->send_to_admin($id_submission);
		$this->is_success($send,$redirect,lang("failed"));

		/*--------------- start notifikasi ---------------*/
		$subm = $this->notification_m->find_submission($id_submission);
		$body = "Telah masuk paper dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name']." pada tanggal ".date("d-m-Y")." jam ".date("H:i:s").".";

		$this->admin_notif_paper(array(
			"subm" => $subm,
			"body" => $body,
			"subject" => "Unggah Karya"
		));
		/*--------------- end notifikasi ---------------*/

		$this->session->set_flashdata('successMessage', lang("success"));
		redirect($redirect);
	}
	/*------------------------------ End Artikel ------------------------------*/

	/*------------------------------ Start Membber ------------------------------*/
	public function add_member(){
		$data = $this->input->post();
		$data['member_email'] = strtolower($data['member_email']);
		$redirect = base_url('paper/submission/show_detail/'.$data['id_submission']);
		$this->is_success($this->validation_member(),$redirect,validation_errors());

		$find = $this->sm->find_member_status($data['id_submission'],$data['member_status']);
		$this->is_success(!$find,$redirect,status_member2($data['member_status'])." Sudah Ada !");

		if($data['member_status']==2){
			$email = explode('@',$data['member_email']);

			$this->is_success($email[1]=="um.ac.id",$redirect,"Email Pembimbing 1 harus @um.ac.id !");
		}

		$add = $this->sm->add_member($data);
		$this->is_success($add,$redirect,lang("failed"));

		$this->session->set_flashdata('successMessage', lang('success_add_member'));
		redirect($redirect);
	}
	public function edit_member(){
		$data = $this->input->post();
		$redirect = base_url('paper/submission/show_detail/'.$data['id_submission']);
		$this->is_success($this->validation_member(),$redirect,validation_errors());

		$find = $this->sm->find_member($data['id_member']);
		$this->is_success($find,$redirect,lang("not_find"));

		if($data['member_status']!=$find['member_status']){
			$find = $this->sm->find_member_status($data['id_submission'],$data['member_status']);
			$this->is_success(!$find,$redirect,status_member2($data['member_status'])." Sudah Ada !");
		}
		if($data['member_status']==2){
			$email = explode('@',$data['member_email']);

			$this->is_success($email[1]=="um.ac.id",$redirect,"Email Pembimbing 1 harus @um.ac.id !");
		}

		$edit = $this->sm->edit_member($data);
		$this->is_success($edit,$redirect,lang("failed"));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function del_member($id_submission,$id_member){
		$data = $this->input->post();
		$redirect = base_url('paper/submission/show_detail/'.$id_submission);

		$find = $this->sm->find_member($id_member);
		$this->is_success($find,$redirect,lang("not_find"));

		$del = $this->sm->del_member($id_member);
		$this->is_success($del,$redirect,lang("failed"));

		$this->session->set_flashdata('successMessage', lang('success_del_member'));
		redirect($redirect);
	}
	public function validation_member(){
		$this->form_validation->set_rules('member_name', lang('member_name') ,'trim|required');
		$this->form_validation->set_rules('member_affiliation', lang('member_affiliation') ,'trim|required');
		$this->form_validation->set_rules('member_email', lang('member_email') ,'trim|required');
		$this->form_validation->set_rules('member_status', lang('member_status') ,'trim|required');

		return $this->form_validation->run()!=FALSE;
	}
	/*------------------------------ End Member ------------------------------*/

	/*------------------------------ Start Revision Plagiarism ------------------------------*/
	public function revision_plagiarism($id_submission){
		$redirect = base_url("paper/submission/show_detail/".$id_submission);

		$find = $this->sm->find_revision_plagiarism($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$this->load->view("content/paper/revision_plagiarism",array(
			"sub" => $find
		));
	}
	public function save_revision_plagiarism(){
		$data = $this->input->post();
		$failed = base_url("paper/submission/revision_plagiarism/".$data['id_submission']);
		$success = base_url("paper/submission/show_detail/".$data['id_submission']);

		$this->validation_paper();
		$this->is_success($this->form_validation->run()===TRUE, $failed, validation_errors());
		$this->is_success(isset($_FILES['file']['name']), $failed, lang('file_harus_diisi'));

		$find = $this->sm->find_revision_plagiarism($data['id_submission']);
		$this->is_success($find,$failed,lang("not_find"));

		$find_usr = $this->notification_m->get_current_user($find['id_user']);

		$revision = $this->sm->count_revision($data['id_submission']);
		$file_name = "TPP_FP_Rev".$revision."_".generate_id($data['id_submission'])."_".$find_usr['full_name'];
        $data['file'] = $this->upload_paper($file_name,"./dist/submissions/","file",$failed);

		$save = $this->sm->save_revision_plagiarism($data);
		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($success);
	}
	/*------------------------------ End Revision Plagiarism ------------------------------*/

	/*------------------------------ Start Revision Reviewer ------------------------------*/
	public function revision_reviewer($id_submission){
		$redirect = base_url("paper/submission/show_detail/".$id_submission);

		$find = $this->sm->find_revision_reviewer($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$this->load->view("content/paper/revision_reviewer",array(
			"sub" => $find
		));
	}
	public function save_revision_reviewer(){
		$data = $this->input->post();
		$failed = base_url("paper/submission/revision_reviewer/".$data['id_submission']);
		$success = base_url("paper/submission/show_detail/".$data['id_submission']);

		$this->validation_paper();
		$this->is_success($this->form_validation->run()===TRUE, $failed, validation_errors());
		$this->is_success(isset($_FILES['file']['name']), $failed, lang('file_harus_diisi'));

		$find = $this->sm->find_revision_reviewer($data['id_submission']);
		$this->is_success($find,$failed,lang("not_find"));

		$find_usr = $this->notification_m->get_current_user($find['id_user']);

		$revision = $this->sm->count_revision($data['id_submission']);
		$file_name = "TPP_FP_Rev".$revision."_".generate_id($data['id_submission'])."_".$find_usr['full_name'];
        $data['file'] = $this->upload_paper($file_name,"./dist/submissions/","file",$failed);

		$save = $this->sm->save_revision_reviewer($data);

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($success);
	}
	/*------------------------------ End Revision Reviewer ------------------------------*/

	public function submit(){
		$failed=base_url("paper/submission/submit");
		$data=$this->input->post();

		if($this->input->post("id_submission")!=null){
			$this->is_success(isset($_FILES['submitted_proof']),$failed,lang("file_harus_diisi"));
			
			$find=$this->sm->find_ready_submit_paper($data['id_submission']);
			$this->is_success($find,$failed,lang("not_find"));

			$this->load->library('upload');

			$nmfile = "TPP_BS_".generate_id($data['id_submission'])."_".$find['full_name'];
	        $config['upload_path'] = "./dist/submitted_proof/";
	        $config['allowed_types'] = 'jpg|png|jpeg|doc|docx|pdf';
	        $config['max_size'] = '2048';
	        $config['overwrite'] = true;
	        $config['file_name'] = $nmfile;

	        $this->upload->initialize($config);
	        
	        $upload=$this->upload->do_upload('submitted_proof');
			$this->is_success($upload,$failed,$this->upload->display_errors());
            
            $file = $this->upload->data();

            $data['submitted_proof'] = $file['file_name'];
            $submit = $this->sm->submit_paper($data);

            $this->is_success($submit,$failed,lang("failed"));

			/*start notifikasi*/
			$subm = $this->notification_m->find_submission($data['id_submission']);

			$body = "Telah masuk bukti submit paper dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name'].". Silahkan lakukan validasi bukti submit tersebut.";

			$this->admin_notif_paper(array(
				"subm" => $subm,
				"body" => $body,
				"subject" => "Submit Artikel"
			));
			/*end notifikasi*/
			
			$this->session->set_flashdata('successMessage', lang('success'));
			redirect(base_url('paper/submission/submit'));
		} else{
			$this->load->view('content/paper/submit',array("submission"=>$this->sm->get_ready_submit_paper()));
		}
	}
}
