<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class To_translator extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('admin/admin_m');
		$this->load->language('general');
		$this->load->language('admin');
		$this->load->model('notification/notification_m','nm');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }
    function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	public function index(){
		$this->load->view('content/admin/send_job_translator', array(
			'get_not_yet' => $this->admin_m->get_not_yet_trans()
		));
	}
	public function administrators_translator(){
		$this->load->view('content/admin/job_translator', array(
			'list' => $this->admin_m->assignment_translator()
		));
	}

	public function send_job_translator(){
		$data = $this->input->post();
		$redirect = base_url('admin/to_translator/');

		$find = $this->admin_m->find_translation($data['id_submission']);
		$this->is_success($find,$redirect,lang('not_find'));

		$send = $this->admin_m->send_job_translator($data);
		$this->is_success($send,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$user = $this->nm->get_current_user($data['id_user']);
		$admin = $this->nm->get_current_user($this->session->userdata("id_user"));

		$subm = $this->nm->find_submission($data['id_submission']);

		$body = "Anda telah mendapatkan tugas dari admin ".$admin['full_name']." untuk menerjemahkan paper dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name'].". Terimakasih dan Selamat Bekerja ^_^";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Job Translator"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect(base_url('admin/to_translator/administrators_translator'));
	}
	public function back($id_submission){
		$redirect = base_url('admin/to_translator/administrators_translator');

		$find = $this->admin_m->find_translated($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$back = $this->admin_m->back_translation($id_submission);
		$this->is_success($back,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$translator = $this->nm->find_translator($id_submission);

		$body = "Mohon maaf, terjemahan paper anda yang berjudul ".$subm['tittle']." telah dibatalkan oleh Administrator kami. Hal ini dapat dikarenakan terdapat beberapa kesalahan. Silahkan periksa kembali pekerjaan anda";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $translator,
			"body" => $body,
			"subject" => "Pengembalian Tugas Translasi !"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function next($id_submission){
		$redirect = base_url('admin/to_translator/administrators_translator');

		$find = $this->admin_m->find_submission($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$next = $this->admin_m->next_translation($id_submission);
		$this->is_success($next,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$user = $this->nm->get_current_user($subm['id_user']);

		$body = "Selamat! Translasi artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> telah selesai. Hasil Translasi bisa anda download di menu <i>'Detail Artikel'</i>. Dengan ini, artikel Anda telah siap untuk disubmit ke Jurnal Internasional. Segera kirimkan bukti submit kepada kami. Pengiriman bukti submit ada di menu <i>'Unggah Bukti Submit'</i>. Apabila bukti submit belum kami terima, maka artikel Anda akan kami anggap Belum Selesai.";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Translasi Selesai !"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function cancel($id_submission){
		$redirect = base_url('admin/to_translator/administrators_translator');

		$find = $this->admin_m->find_assign_trans($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$back = $this->admin_m->cancel_job_trans($id_submission);
		$this->is_success($back,$redirect,lang('failed'));

		unlink('./dist/translation/'.$find['translation_file']);

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$translator = $this->nm->find_translator($id_submission);

		$body = "Tugas Translasi Anda untuk artikel dengan judul <i>'".$subm['tittle']."'</i> dibatalkan. Silahkan tunggu informasi selanjutnya.";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $translator,
			"body" => $body,
			"subject" => "Pembatalan Tugas Translasi !"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect(base_url('admin/to_translator'));
	}
	function current_user_notif_paper($data){
		$content = $this->load->view("content/notification/paper",$data,TRUE);

	    $this->email->to($data['user']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
}