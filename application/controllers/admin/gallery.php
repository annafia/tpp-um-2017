<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('gallery/gallery_m');
		$this->load->language('general');
		$this->load->language('gallery');
		$this->load->library('image_lib');

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

	public function index(){
		if($this->input->post("create")!=null){
			if(isset($_FILES['url_photo']['name'])){
				$this->load->library('upload');

				$nmfile = "photo_".date("Y-m-d H i s")."_".$this->input->post("caption");
		        $config['upload_path'] = "./dist/images/gallery/";
		        $config['allowed_types'] = 'jpg|png|jpeg|bmp';
		        $config['max_size'] = '2048';
		        $config['max_width']  = '10576';
		        $config['max_height']  = '5536';
		        $config['file_name'] = $nmfile;

		        $this->upload->initialize($config);
		        if ($this->upload->do_upload('url_photo')){
					$this->image_lib->resize();
		        	$data_photo = $this->upload->data();
		        	$data=array(
		        		"url_photo"=>$data_photo['file_name'],
		        		"caption"=>$this->input->post("caption")
		        	);

		        	/*create tumb*/
		        	unset($config);
		        	$config['image_library'] = 'gd2';
					$config['source_image'] = './dist/images/gallery/'.$data_photo['file_name'];
					$config['new_image'] = './dist/images/gallery/tumb/tumb_'.$data_photo['file_name'];
					$config['maintain_ratio'] = FALSE;
					$config['width']         = 320;
					$config['height']       = 240;

					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					/*create tumb*/

		        	$create=$this->gallery_m->add($data);
		        	if($create){
		        		$this->session->set_flashdata('successMessage', lang('success'));
		        	}else{
		        		$this->session->set_flashdata('errorMessage', lang('failed'));
		        	}
		        } else{
		        	$this->session->set_flashdata('errorMessage', $this->upload->display_errors());
		        }
			} else{
				$this->session->set_flashdata('errorMessage', lang('photo_not_found'));
			}
			redirect(base_url("admin/gallery"));
		} else if($this->input->post("change")!=null){
			$find=$this->gallery_m->find($this->input->post("id_gallery"));
			if($find){
				if(isset($_FILES['url_photo']['name'])){
					$this->load->library('upload');

					$nmfile = "photo_".date("Y-m-d H i s")."_".$this->input->post("caption");
			        $config['upload_path'] = "./dist/images/gallery/";
			        $config['allowed_types'] = 'jpg|png|jpeg|bmp';
			        $config['max_size'] = '2048';
			        $config['max_width']  = '10576';
			        $config['max_height']  = '5536';
			        $config['file_name'] = $nmfile;

			        $this->upload->initialize($config);
			        if ($this->upload->do_upload('url_photo')){
			        	$data_photo = $this->upload->data();
			        	$data=array(
			        		"url_photo"=>$data_photo['file_name'],
			        		"caption"=>$this->input->post("caption"),
			        		"id_gallery"=>$this->input->post("id_gallery")
			        	);

			        	/*create tumb*/
			        	unset($config);
			        	$config['image_library'] = 'gd2';
						$config['source_image'] = './dist/images/gallery/'.$data_photo['file_name'];
						$config['new_image'] = './dist/images/gallery/tumb/tumb_'.$data_photo['file_name'];
						$config['maintain_ratio'] = FALSE;
						$config['width']         = 320;
						$config['height']       = 240;

						$this->image_lib->initialize($config);
						$this->image_lib->resize();
						/*create tumb*/

			        	$update=$this->gallery_m->update($data);
			        	if($update){
			        		foreach ($find as $res) {}
			        		$old=$res['url_photo'];
			        		unlink("./dist/images/gallery/$old");
			        		unlink("./dist/images/gallery/tumb/tumb_$old");
			        		$this->session->set_flashdata('successMessage', lang('success'));
			        	}else{
			        		$this->session->set_flashdata('errorMessage', lang('failed'));
			        	}
			        } else{
			        	$this->session->set_flashdata('errorMessage', $this->upload->display_errors());
			        }
				} else{
					$this->session->set_flashdata('errorMessage', lang('photo_not_found'));
				}
			} else{
				$this->session->set_flashdata('errorMessage', lang('not_find'));
			}
			redirect(base_url("admin/gallery"));
		}else{
			$data['gallery']=$this->gallery_m->show_gallery();
			$this->load->view('content/admin/gallery',$data);
		}
	}
	public function delete($id_gallery){
		$find=$this->gallery_m->find($id_gallery);
		if($find){
			$del=$this->gallery_m->del($id_gallery);
			if($del){
				foreach ($find as $res) {}
	    		$old=$res['url_photo'];
	    		unlink("./dist/images/gallery/$old");
	    		unlink("./dist/images/gallery/tumb/tumb_$old");
	    		$this->session->set_flashdata('successMessage', lang('success'));
	    	}else{
	    		$this->session->set_flashdata('errorMessage', lang('failed'));
	    	}
		} else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
		}
		redirect(base_url("admin/gallery"));
	}
}
