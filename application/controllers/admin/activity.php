<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('admin/admin_m');
       	$this->load->model('account/account_m');       	
		$this->load->language('general');
		$this->load->language('admin');
		$this->load->language('account');
		$this->load->language('register');
		$this->load->model('notification/notification_m');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

	public function index(){
		$this->load->view('content/admin/printout', array(
			'list' => $this->admin_m->printout_submission()
		));
	}

	public function report(){
		$this->load->view('content/admin/report', array(
			'list' => $this->admin_m->report($this->input->get()),
			'reviewer' => $this->input->get('reviewer'),
			'translator' => $this->input->get('translator')
		));
	}

	public function employee(){
		$list = $this->admin_m->get_employee();

		$this->load->view('content/admin/employee', array(
			'list' => $list
		));
	}

	public function submitted_proof(){
		$list = $this->admin_m->get_list_submit();
		$finish = $this->admin_m->get_finish_submit();

		$this->load->view('content/admin/submission', array(
			'list' => $list,
			'finish' => $finish
		));
	}

	public function valid($id_submission){
		$find = $this->admin_m->find_submission($id_submission);
		if($find){
			$check = $this->admin_m->valid_submission($id_submission);
			if($check > 0){
				/* Start Notifikasi Email */
				$subm = $this->notification_m->find_submission($id_submission);
				$user = $this->notification_m->get_current_user($subm['id_user']);

				$body = "Selamat atas dipublikasinya karya anda yang berjudul ".$subm['tittle']." dinyatakan valid. Terimakasih telah menggunakan layanan kami. Kami tunggu karya anda selanjutnya ! ^_^";

				$this->current_user_notif_paper(array(
					"subm" => $subm,
					"user" => $user,
					"body" => $body,
					"subject" => "Validasi Bukti Submit"
				));
				/* End Notifikasi Email */
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('admin/activity/submitted_proof');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('admin/activity/submitted_proof');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('admin/activity/submitted_proof');
		}
	}

	public function not_valid($id_submission){
		$find = $this->admin_m->find_submission($id_submission);
		if($find){
			$check = $this->admin_m->not_valid($id_submission);
			if($check > 0){
				/* Start Notifikasi Email */
				$subm = $this->notification_m->find_submission($id_submission);
				$user = $this->notification_m->get_current_user($subm['id_user']);

				$body = "Mohon maaf! Bukti submit karya anda yang berjudul ".$subm['tittle']." dinyatakan tidak valid. Silahkan unggah kembali bukti submit anda dan Pastikan anda mengunggah file yang benar! Terimakasih.";

				$this->current_user_notif_paper(array(
					"subm" => $subm,
					"user" => $user,
					"body" => $body,
					"subject" => "Validasi Bukti Submit"
				));
				/* End Notifikasi Email */
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('admin/activity/submitted_proof');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('admin/activity/submitted_proof');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('admin/activity/submitted_proof');
		}
	}

	public function back($id_submission){
		$find = $this->admin_m->find_submission($id_submission);
		if($find){
			$check = $this->admin_m->back_stat_submission($id_submission);
			if($check > 0){
				/* Start Notifikasi Email */
				$subm = $this->notification_m->find_submission($id_submission);
				$user = $this->notification_m->get_current_user($subm['id_user']);

				$body = "Status validasi bukti submit karya anda yang berjudul ".$subm['tittle']." dikembalikan ke status awal. Silahkan tunggu informasi selanjutnya dari kami !";

				$this->current_user_notif_paper(array(
					"subm" => $subm,
					"user" => $user,
					"body" => $body,
					"subject" => "Validasi Bukti Submit"
				));
				/* End Notifikasi Email */
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('admin/activity/submitted_proof');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('admin/activity/submitted_proof');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('admin/activity/submitted_proof');
		}
	}

	public function user($id_user){
		$search = $this->account_m->find($id_user);
		if(empty($search)){
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('admin/activity/employee');
		}else{		
			$where = array("id_user" => $id_user);

			if($this->input->post("full_name")!=null){
				//Form Validation
				$this->form_validation->set_rules('full_name', lang("ph_nama_lengkap"), 'trim|required|regex_match[/^[a-zA-Z ]+$/]');
			    $this->form_validation->set_rules('id_fakultas', lang("fakultas"), 'required');
			    $this->form_validation->set_rules('id_jurusan', lang("jurusan"), 'required');
			    $this->form_validation->set_rules('id_prodi', lang("prodi"), 'required');
			    $this->form_validation->set_rules('phone_number', lang("ph_phone"), 'trim|required|numeric');
			    $this->form_validation->set_rules('address', lang("ph_addr"), 'trim|required');
			    $this->form_validation->set_rules('gender', lang("ph_gender"), 'trim|required');
			    $this->form_validation->set_rules('position', lang("ph_jabatan"), 'trim|required');
			    $this->form_validation->set_rules('id_personal', lang("ph_nim_nidn"), 'trim|required|numeric');
			    $this->form_validation->set_rules('birth', lang("ph_birth"), 'required');
				
				if($this->form_validation->run()!=FALSE){
					$data=$this->input->post();
					if($this->account_m->find($id_user)){
						$where=array("id_user"=>$id_user);
						$this->session->set_flashdata('successMessage', lang('success'));
						$this->account_m->edit_personal_data($data,$where);
						redirect('admin/activity/employee');
					} else{
						$this->session->set_flashdata('errorMessage', lang('not_find'));
					}
				} else {
					$this->session->set_flashdata('errorMessage', validation_errors());
				}
				redirect(base_url("admin/activity/user/".$id_user));
			} else if($this->input->post("password")!=null){
				//Form validation
			    $this->form_validation->set_rules('password', lang("password_baru"), 'trim|required|min_length[8]');
			    $this->form_validation->set_rules('repassword', lang("ph_konfirmasi"), 'trim|required|matches[password]');

				if($this->form_validation->run()!=FALSE){
					$data=$this->input->post();
					$ubah=array(
						'password' => md5($data['password']),
						'repassword' => $data['repassword']
					);
					if($this->account_m->ubah_data_user($ubah,$where)){
						$this->session->set_flashdata('successMessage', lang('success'));
						redirect('admin/activity/employee');
					} else{
						$this->session->set_flashdata('errorMessage', lang('failed'));
					}
				} else {
					$this->session->set_flashdata('errorMessage', validation_errors());
				}
				redirect(base_url("admin/activity/user/".$id_user));
			} else if($this->input->post("email")!=null){
				//Form Validation
				$this->form_validation->set_rules('email', lang("ph_email"), 'required|valid_email|is_unique[user.email]');

				$valid_email=explode("@", $this->input->post('email'))[1];

				if($valid_email!="um.ac.id" && $valid_email!="students.um.ac.id"){
					$this->session->set_flashdata('errorMessage', lang("valid_email"));
					$this->session->set_flashdata($this->input->post());
					redirect(base_url("admin/activity/user/".$id_user));
				}

				if($this->form_validation->run()!=FALSE){
					$data=$this->input->post();
					if($this->account_m->ubah_data_user($data,$where)){
						$this->session->set_flashdata('successMessage', lang('success'));
						redirect('admin/activity/employee');
					} else{
						$this->session->set_flashdata('errorMessage', lang('failed'));
					}
				} else {
					$this->session->set_flashdata('errorMessage', validation_errors());
				}
				redirect(base_url("admin/activity/user/".$id_user));
			} else if(isset($_FILES['url_photo']['name'])){
				$this->load->library('upload');

				$nmfile = "TPP_PP_".time();
		        $config['upload_path'] = "./dist/pp_users/";
		        $config['allowed_types'] = 'jpg|png|jpeg|bmp';
		        $config['max_size'] = '2048';
		        $config['max_width']  = '10576';
		        $config['max_height']  = '5536';
		        $config['file_name'] = $nmfile;

		        $this->upload->initialize($config);
		        if ($this->upload->do_upload('url_photo')){
		        	$data_photo = $this->upload->data();
		        	$data=array("url_photo"=>$data_photo['file_name']);

		        	if($this->input->post("old_url_photo")!=null){
		        		$old_url_photo=$this->input->post("old_url_photo");
		        		unlink("./dist/pp_users/$old_url_photo");
		        	}

		        	if($this->account_m->edit_personal_data($data,array("id_user" => $id_user))){
		        		$this->session->set_flashdata('successMessage', lang('success'));
		        		redirect('admin/activity/employee');
		        	} else{
		        		$this->session->set_flashdata('errorMessage', lang('failed'));
		        	}
		        } else {
		        	$this->session->set_flashdata('errorMessage', $this->upload->display_errors());
		        }
		        redirect(base_url("admin/activity/user/".$id_user));
			} else {
				$data['user']=$this->account_m->get_user_data($id_user);
				$this->load->view('content/account/index_admin',$data);
			}
		}
	}

	public function del_emp($id_user){
		$find = $this->account_m->find($id_user);
		if($find){
			$delete = $this->account_m->del_emp($id_user);
			if($delete > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('admin/activity/employee');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('admin/activity/employee');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('admin/activity/employee');
		}
	}
	function current_user_notif_paper($data){
		$content = $this->load->view("content/notification/paper",$data,TRUE);

	    $this->email->to($data['user']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject("Notifikasi ".$data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
}
