<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class To_plagiarism extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('admin/admin_m');
		$this->load->language('general');
		$this->load->language('admin');
		$this->load->model('notification/notification_m','nm');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }
    function current_user_notif_paper($data){
		$content = $this->load->view("content/notification/paper",$data,TRUE);

	    $this->email->to($data['user']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	
    public function index(){
    	redirect(base_url("admin/to_plagiarism/send_job_plagiarism"));
    }
    public function send_job_plagiarism(){
		$list = $this->admin_m->get_send_job_plagiarism();
			
		$this->load->view('content/admin/send_job_plagiarism', array(
			'list' => $list,
		));
	}
	public function administrators_plagiarism(){
		$list = $this->admin_m->get_job_plagiarism();

		$this->load->view('content/admin/job_plagiarism', array(
			'list' => $list
		));
	}

	public function save_plagiarism(){
		$redirect = base_url("admin/to_plagiarism/");
		$data = $this->input->post();
		$find = $this->admin_m->find_send_job_plagiarism($data['id_submission']);
		$this->is_success($find,$redirect,lang("not_find"));

		$create = $this->admin_m->create_plagiarism($data);
		$this->is_success($create,$redirect,lang("failed"));

		/* Start Notifikasi Email */
		$user = $this->nm->get_current_user($data['id_user']);
		$subm = $this->nm->find_submission($data['id_submission']);

		$body = "Anda telah mendapatkan tugas dari admin untuk memeriksa plagiasi artikel dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name'].".  Terimakasih dan Selamat Bekerja ^_^";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Job Plagiarism"
		));
		/* End Notifikasi Email */
		$this->session->set_flashdata('successMessage', lang('success'));
		redirect('admin/to_plagiarism/administrators_plagiarism');
	}
	public function back_author($id_submission){
		$redirect = base_url("admin/to_plagiarism/administrators_plagiarism");

		$find = $this->admin_m->find_finish_job_plagiarism($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$back = $this->admin_m->back_author_plagiarism($id_submission);
		$this->is_success($back,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$user = $this->nm->get_current_user($subm['id_user']);

		$body = "Mohon Maaf! Artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> dinyatakan tidak lolos pada tahap pemeriksaan plagiasi. Hal ini dikarenakan paper Anda memiliki persentase plagiasi yang tinggi. Silahkan perbaiki artikel Anda dan segera lakukan <i>'Revisi'</i>. Tombol <i>'Revisi'</i> akan otomatis muncul pada menu Detail Artikel. Terimakasih telah menggunakan layanan kami.";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Revisi !!! (Plagiasi Tinggi)"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function back_check($id_submission){
		$redirect = base_url("admin/to_plagiarism/administrators_plagiarism");

		$find = $this->admin_m->find_finish_job_plagiarism($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$back = $this->admin_m->back_check_plagiarism($id_submission);
		$this->is_success($back,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$user = $this->nm->find_plagiarism($id_submission);

		$body = "Pemeriksaan plagiasi Anda untuk artikel dengan judul <i>'".$subm['tittle']."'</i> dikembalikan ke status awal oleh Admin. Hal ini biasanya dikarenakan pekerjaan Anda yang kurang sesuai. Silahkan perbaiki pekerjaan Anda.";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Pekerjaan Anda Kurang Sesuai !"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function next($id_submission){
		$redirect = base_url("admin/to_plagiarism/administrators_plagiarism");

		$find = $this->admin_m->find_submission($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$back = $this->admin_m->next_plagiarism($id_submission);
		$this->is_success($back,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$user = $this->nm->get_current_user($subm['id_user']);

		$body = "Selamat! Artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> dinyatakan LOLOS pada Tahap Pemeriksaan Plagiasi. Pemrosesan artikel Anda akan dilanjutkan ke Tahap Review. Silahkan tunggu informasi selanjutnya dari kami. Terimakasih ^_^";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Sukses !!!"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect(base_url('admin/to_reviewer'));
	}
	public function cancel($id_submission){
		$redirect = base_url("admin/to_plagiarism/administrators_plagiarism");

		$find = $this->admin_m->find_job_plagiarism($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$cancel = $this->admin_m->cancel_job_plagiarism($id_submission);
		$this->is_success($cancel,$redirect,lang('failed'));

		unlink('./dist/plagiarism/'.$find['plagiarism_file']);

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$user = $this->nm->find_plagiarism($id_submission);

		$body = "Tugas Pemeriksaan plagiasi Anda untuk artikel dengan judul <i>'".$subm['tittle']."'</i> dibatalkan. Silahkan tunggu informasi selanjutnya.";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Pekerjaan Dibatalkan !"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect(base_url('admin/to_plagiarism'));
	}
}
