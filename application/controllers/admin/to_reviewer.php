<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class To_reviewer extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('admin/admin_m','am');
		$this->load->language('general');
		$this->load->language('submission');
		$this->load->language('admin');
		$this->load->model('notification/notification_m','nm');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',1)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>1,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

    /*------------------------------ Start Local Function ------------------------------*/
    function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	public function validation_review(){
		$this->form_validation->set_rules('general_comment', lang('komentarUmum') ,'trim|required');
		$this->form_validation->set_rules('decission_status', lang('putusan') ,'trim|required');

		return $this->form_validation->run()!=FALSE;
	}
	public function upload_paper($file_name,$path,$field,$redirect){
		$this->load->library('upload');

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'docx|doc|pdf';
        $config['max_size'] = '10240';
        $config['file_name'] = $file_name;
        $config['overwrite'] = true;

        $this->upload->initialize($config);
        $this->is_success($this->upload->do_upload($field),$redirect,$this->upload->display_errors());
        $file = $this->upload->data();

        return $file['file_name'];
	}
	/*------------------------------ End Local Function ------------------------------*/

	public function index(){
		$get_not_yet = $this->am->get_send_job_rev();

		$this->load->view('content/admin/send_job_reviewer', array(
			'get_not_yet' => $get_not_yet
		));
	}
	public function administrators_reviewer(){
		$list = $this->am->get_job_rev();

		$this->load->view('content/admin/job_reviewer', array(
			'list' => $list
		));
	}
	public function show_detail($id_submission){
		$redirect = base_url("paper/submission/");
		
		$find = $this->am->find_submission($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$this->load->view("content/admin/detail_submission", array(
			"submission" => $find,
			"member" => $this->am->get_member($id_submission),
			"plagiarism" => $this->am->get_detail_plagiarism($id_submission),
			"review" => $this->am->get_detail_review($id_submission),
			'trans' => $this->am->find_translasi($id_submission),
		));
	}
	public function show_detail_review($id_submission){
		$redirect = base_url('admin/to_reviewer/administrators_reviewer');
		
		$find = $this->am->find_job_rev($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$this->load->view("content/admin/detail_review", array(
			"submission" => $find,
			"member" => $this->am->get_member($id_submission),
			"plagiarism" => $this->am->get_detail_plagiarism($id_submission),
			"review" => $this->am->get_active_review($id_submission),
			"hst_review" => $this->am->get_detail_review($id_submission),
		));
	}
	public function save_reviewer(){
		$redirect = base_url('admin/to_reviewer/administrators_reviewer');
		$id_submission = $this->input->post('id_submission');
		$id_user = $this->input->post('id_user');

		$find = $this->am->find_send_job_rev($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$this->is_success($id_user,$redirect,lang('failed'));
		foreach($id_user as $user){
			$data_rev = array(
				'id_submission' => $id_submission,
				'id_user' => $user,
				'fp_file' => $find['file'],
				'review_as' => 1
			);

			$create = $this->am->create_job_reviewer($data_rev);
			$this->is_success($create,$redirect,lang('failed'));

			/* Start Notifikasi Email */
			$user = $this->nm->get_current_user($user);
			$admin = $this->nm->get_current_user($this->session->userdata("id_user"));

			$subm = $this->nm->find_submission($id_submission);

			$body = "Anda telah mendapatkan tugas dari admin untuk mereview artikel dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name'].".  Terimakasih dan Selamat Bekerja ^_^";

			$this->current_user_notif_paper(array(
				"subm" => $subm,
				"user" => $user,
				"body" => $body,
				"subject" => "Job Reviewer"
			));
			/* End Notifikasi Email */
		}
		$data_rev = array(
			'id_submission' => $id_submission,
			'id_user' => $this->session->userdata('id_user'),
			'fp_file' => $find['file'],
			'review_as' => 2
		);
		$create = $this->am->create_job_reviewer($data_rev);
		$this->is_success($create,$redirect,lang('failed'));

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect(base_url('admin/to_reviewer/show_detail_review/'.$id_submission));
	}
	public function save_review(){
		$data = $this->input->post();
		$not_found = base_url('admin/to_reviewer/administrators_reviewer');
		$redirect = base_url('admin/to_reviewer/show_detail_review/'.$data['id_submission']);
		$submit = $data['submit']; unset($data['submit']);

		$find_review = $this->am->find_review_adm($data['id_submission']);
		$this->is_success($find_review,$not_found,lang("not_find"));

		$find = $this->am->find_job_rev($data['id_submission']);
		$this->is_success($find,$not_found,lang("not_find"));

		/*------------------ Hanya Submit ------------------*/
		if($submit==2){
			$save_submit = $this->am->submit_review($data['id_submission'],$find_review['decission_status']);
			$this->is_success($save_submit,$redirect,lang("failed"));

			/* Start Notifikasi Email */
			$subm = $this->nm->find_submission($data['id_submission']);
			$user = $this->nm->get_current_user($subm['id_user']);

			if ($find_review['decission_status']==1) {
				$body = "Mohon Maaf! Artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> dinyatakan tidak lolos pada Tahap Review. Silahkan perbaiki artikel Anda dan segera lakukan <i>'Revisi'</i>. Tombol <i>'Revisi'</i> akan otomatis muncul pada menu Detail Artikel. Terimakasih telah menggunakan layanan kami.";
				$subject = "Revisi Artikel !";
			}else if($find_review['decission_status']==2){
				$body = "Selamat! Artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> dinyatakan LOLOS pada Tahap Review. Pemrosesan artikel Anda akan dilanjutkan ke Tahap Translasi. Silahkan tunggu informasi selanjutnya dari kami. Terimakasih ^_^";
				$subject = "Sukses !!!";
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect($not_found);
			}

			$this->current_user_notif_paper(array(
				"subm" => $subm,
				"user" => $user,
				"body" => $body,
				"subject" => $subject
			));
			/* End Notifikasi Email */

			$this->session->set_flashdata('successMessage', lang('success'));
			redirect($not_found);
		}
		/*------------------ Hanya Submit ------------------*/

		$this->is_success($this->validation_review(),$redirect,validation_errors());

		$rev_ke = $this->am->count_review_adm($data['id_submission']);

		$file_name = "TPP_ReviewAdm".$rev_ke."_".generate_id($data['id_submission'])."_".$find['full_name'];
        $data['review_file']=$this->upload_paper($file_name,"./dist/review/","review_file",$redirect);

		$save = $this->am->save_review($data);
		$this->is_success($save,$redirect,lang("failed"));

		/*------------------ Simpan dan Submit ------------------*/
		if($submit==1){
			$save_submit = $this->am->submit_review($data['id_submission'], $data['decission_status']);

			/* Start Notifikasi Email */
			$subm = $this->nm->find_submission($data['id_submission']);
			$user = $this->nm->get_current_user($subm['id_user']);

			if ($data['decission_status']==1) {
				$body = "Mohon Maaf! Artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> dinyatakan tidak lolos pada Tahap Review. Silahkan perbaiki artikel Anda dan segera lakukan <i>'Revisi'</i>. Tombol <i>'Revisi'</i> akan otomatis muncul pada menu Detail Artikel. Terimakasih telah menggunakan layanan kami.";
				$subject = "Revisi Artikel !";
			}else if($data['decission_status']==2){
				$body = "Selamat! Artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> dinyatakan LOLOS pada Tahap Review. Pemrosesan artikel Anda akan dilanjutkan ke Tahap Translasi. Silahkan tunggu informasi selanjutnya dari kami. Terimakasih ^_^";
				$subject = "Sukses !!!";
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect($not_found);
			}

			$this->current_user_notif_paper(array(
				"subm" => $subm,
				"user" => $user,
				"body" => $body,
				"subject" => $subject
			));
			/* End Notifikasi Email */

			$this->session->set_flashdata('successMessage', lang('success'));
			redirect($not_found);
		}
		/*------------------ Simpan dan Submit ------------------*/

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	public function back($id_submission, $id_review){
		$redirect = base_url('admin/to_reviewer/show_detail_review/'.$id_submission);
		$find = $this->am->find_finish_review($id_review);
		$this->is_success($find,$redirect,lang('not_find'));

		$back = $this->am->back_review($id_review);
		$this->is_success($back,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$translator = $this->nm->find_reviewer($id_review);

		$body = "Mohon maaf, review artikel anda yang berjudul ".$subm['tittle']." telah dibatalkan oleh Administrator. Hal ini dapat disebabkan karena terdapat beberapa kesalahan pada pekerjaan anda. Silahkan periksa kembali pekerjaan anda";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $translator,
			"body" => $body,
			"subject" => "Pengembalian Tugas Review !"
		));
		/* End Notifikasi Email */

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
	function next($id_submission){
		$redirect = base_url("admin/to_reviewer/administrators_reviewer");

		$find = $this->am->find_submission($id_submission);
		$this->is_success($find,$redirect,lang('not_find'));

		$next = $this->am->submit_review($id_submission,2);
		$this->is_success($next,$redirect,lang('failed'));

		/* Start Notifikasi Email */
		$subm = $this->nm->find_submission($id_submission);
		$user = $this->nm->get_current_user($subm['id_user']);

		$body = "Selamat! Artikel Anda yang berjudul <i>'".$subm['tittle']."'</i> dinyatakan LOLOS pada Tahap Review. Pemrosesan artikel Anda akan dilanjutkan ke Tahap Translasi. Silahkan tunggu informasi selanjutnya dari kami. Terimakasih ^_^";

		$this->current_user_notif_paper(array(
			"subm" => $subm,
			"user" => $user,
			"body" => $body,
			"subject" => "Sukses !!!"
		));
		/* End Notifikasi Email */
		$this->session->set_flashdata('successMessage', lang('success'));
		redirect(base_url('admin/to_translator'));
	}
	function current_user_notif_paper($data){
		$content = $this->load->view("content/notification/paper",$data,TRUE);

	    $this->email->to($data['user']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
}
