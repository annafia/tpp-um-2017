<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminder extends CI_Controller {
	public function __construct(){
       parent::__construct();
		$this->load->language('general');
		$this->load->language('notification');
		$this->load->model("reminder_m","rm");
		$this->load->library("email");
    }
    public function index(){
    	if(isset($_SERVER['PHP_AUTH_USER'])){
	    	$auth = array(
	    		"username" => $_SERVER['PHP_AUTH_USER'],
	    		"password" => $_SERVER['PHP_AUTH_PW']
	    	);

    		if($auth['username']!='tpp-um' || $auth['password']!='tpp-um-2018'){
    			redirect("");
    		}
	    } else{
	    	redirect("");
	    }
    	// 3days job Reminder
    	for($i=3;$i<14;$i+=3){
    		$plag3 = $this->rm->plag($i);
	    	$rev3 = $this->rm->rev($i);
	    	$trans3 = $this->rm->trans($i);
	    	
	    	/* Start Notifikasi Email */
	    	foreach ($plag3 as $p) {
	    		$days = 14-$p['diff'];
	    		$body = "Kami informasikan bahwa masa aktif pengecekan plagiarism anda untuk paper dengan judul <i>".$p['tittle']."</i> tersisa ".$days." Hari. Apabila anda tidak melakukan pengerjaan sebelum berakhirnya masa aktif, pekerjaan akan dialihkan ke pegawai lainnya.";
				$this->notif_reminder(array(
					"reminder" => $p,
					"body" => $body,
					"subject" => "Job Expiration Date is ".$days." More Days"
				));
	    	}
	    	foreach ($rev3 as $r) {
	    		$days = 14-$r['diff'];
	    		$body = "Kami informasikan bahwa masa aktif pengerjaan review anda untuk paper dengan judul <i>".$r['tittle']."</i> tersisa ".$days." Hari. Apabila anda tidak melakukan pengerjaan sebelum berakhirnya masa aktif, pekerjaan akan dialihkan ke pegawai lainnya.";
				$this->notif_reminder(array(
					"reminder" => $r,
					"body" => $body,
					"subject" => "Job Expiration Date is ".$days." More Days"
				));
	    	}
	    	foreach ($trans3 as $t) {
	    		$days = 14-$t['diff'];
	    		$body = "Kami informasikan bahwa masa aktif penerjemahan anda untuk paper dengan judul <i>".$t['tittle']."</i> tersisa ".$days." Hari. Apabila anda tidak melakukan pengerjaan sebelum berakhirnya masa aktif, pekerjaan akan dialihkan ke pegawai lainnya.";
				$this->notif_reminder(array(
					"reminder" => $t,
					"body" => $body,
					"subject" => "Job Expiration Date is ".$days." More Days"
				));
	    	}
			/* End Notifikasi Email */
    	}
    	// End job reminder

    	// Start Expired job reminder
    	$plag_end = $this->rm->plag(14);
    	$rev_end = $this->rm->rev(14);
    	$trans_end = $this->rm->trans(14);

    	foreach ($plag_end as $p) {
    		// $this->rm->plag_expired($p['id_submission']);
    		$body = "Tugas pemeriksaan plagiarism artikel dengan judul <i>'".$p['tittle']."'</i> yang ditangani oleh ".$p['full_name']."(Petugas) telah melewati batas pengerjaan (14 hari).";
    		
			$this->admin_notif_reminder(array(
				"body" => $body,
				"subject" => "Job Expired"
			));
    	}
    	foreach ($rev_end as $r) {
    		// $this->rm->rev_expired($p['id_submission']);
    		$body = "Tugas review artikel dengan judul <i>'".$p['tittle']."'</i> yang ditangani oleh ".$p['full_name']."(Petugas) telah melewati batas pengerjaan (14 hari).";
			$this->admin_notif_reminder(array(
				"body" => $body,
				"subject" => "Job Expired"
			));
    	}
    	foreach ($trans_end as $t) {
    		// $this->rm->trans_expired($p['id_submission']);
    		$body = "Tugas translasi artikel dengan judul <i>'".$p['tittle']."'</i> yang ditangani oleh ".$p['full_name']."(Petugas) telah melewati batas pengerjaan (14 hari).";
			$this->admin_notif_reminder(array(
				"body" => $body,
				"subject" => "Job Expired"
			));
    	}
		// End Expired job reminder

		// Start Submit Paper Reminder
		for($i=7;$i<=90;$i+=7){
			$submit = $this->rm->finish($i);

			foreach ($submit as $s) {
	    		$body = "Kami ingatkan kembali untuk mengunggah bukti submit paper anda yang berjudul <i>".$p['tittle']."</i>. Terimakasih telah menggunakan layanan TPP UM. Kritik dan Saran dari anda akan sangat bermanfaat untuk membuat layanan TPP UM lebih baik lagi.";
				$this->notif_reminder(array(
					"reminder" => $p,
					"body" => $body,
					"subject" => "Job Expired"
				));
	    	}
		}
		// End Submit Paper Reminder
    }
    function notif_reminder($data){
		$content = $this->load->view("content/notification/reminder",$data,TRUE);

	    $this->email->to($data['reminder']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	function admin_notif_reminder($data){
		$admin=$this->reminder_m->get_admin();

		foreach ($admin as $a) {
			$data['reminder']=$a;
			$content = $this->load->view("content/notification/reminder",$data,TRUE);

		    $this->email->to($data['reminder']['email']);
		    $this->email->from("tppum2017@gmail.com", 'TPP UM');
		    $this->email->subject($data['subject']);
		    $this->email->message($content);

		    $this->email->send();
		}
	}
}
