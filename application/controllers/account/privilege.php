<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privilege extends CI_Controller {
	public function __construct(){
       	parent::__construct();
       	$this->load->model("account/account_m");
		$this->load->language('general');
		$this->load->language('dashboard');
		$this->load->language('register');
		$this->load->language('account');
		$this->load->model('notification/notification_m');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		
    }

	public function index(){
		$id_user = $this->session->userdata('id_user');
		$list = $this->account_m->get_privilege();
		$get_privilege = $this->account_m->privilage();

		$find_prev = $this->account_m->find_prev($id_user);
		$select_prev = array();
		foreach($find_prev as $row) {
			$select_prev[] = $row['privilage_id'];
		}


		$this->load->view('content/account/add_privilege', array(
			'list' => $list,
			'get_privilege' => $get_privilege,
			'select_prev' => $select_prev
		));
	}

	public function create() {
		$id_user = $this->input->post('id_user');
		$privilage_id = $this->input->post('privilage_id');
		if($privilage_id){
			$del = $this->db->where('id_user', $id_user)
						    ->delete('data_privilage');

			foreach($privilage_id as $privilage){
				$data_prev = array(
					'id_user' => $id_user,
					'privilage_id' => $privilage
				);
				$create = $this->account_m->create_previlege($data_prev, $id_user);
			}
			if($create > 0){
				/* Start Notifikasi Email */
				$user = $this->notification_m->get_current_user($id_user);

				$body = "Pengajuan hak akses baru oleh ".$user['full_name'].". Silahkan melakukan pengelolaan hak akses untuk user tersebut.";

				$this->admin_notif_privilage(array(
					"body" => $body,
					"subject" => "Pengajuan Hak Akses"
				));
				/* End Notifikasi Email */

					$this->session->set_flashdata('successMessage', lang('success'));
					redirect('account/privilege');
				}else{
					$this->session->set_flashdata('errorMessage', lang('failed'));
					redirect('account/privilege');
				}
		}else{
			$this->session->set_flashdata('errorMessage', lang('failed'));
			redirect('account/privilege');
		} 
	}

	public function update(){
		$data = array(
				'id_topic' => $this->input->post('id_topic'),
				'nm_topic' => $this->input->post('nm_topic')
		);
		$find = $this->account_m->find($data['id_topic']);
		if($find){
			$update = $this->account_m->update(array('id_topic' => $this->input->post('id_topic')), $data);
			if($update > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('account/privilege');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('account/privilege');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('account/privilege');
		}
		
	}

	public function delete($id_topic){
		$find = $this->account_m->find($id_topic);
		if($find){
			$delete = $this->account_m->delete($id_topic);
			if($delete > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('account/privilege');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('account/privilege');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('account/privilege');
		}
	}

	public function delete_privilege($id_user, $no){
		$find = $this->account_m->find_privilege($id_user,$no);
		if($find){
			$delete = $this->account_m->delete_privilege($id_user,$no);
			if($delete > 0){
				$this->session->set_flashdata('successMessage', lang('success'));
				redirect('account/privilege');
			}else{
				$this->session->set_flashdata('errorMessage', lang('failed'));
				redirect('account/privilege');
			}
		}else{
			$this->session->set_flashdata('errorMessage', lang('not_find'));
			redirect('account/privilege');
		}
	}
	function admin_notif_privilage($data){
		$admin=$this->notification_m->get_admin();

		foreach ($admin as $a) {
			$data['user'] = $a;
			$content = $this->load->view("content/notification/privilage",$data,TRUE);

		    $this->email->to($a['email']);
		    $this->email->from("tppum2017@gmail.com", 'TPP UM');
		    $this->email->subject("Notifikasi ".$data['subject']);
		    $this->email->message($content);

		    $this->email->send();
		}
	}
}