<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit extends CI_Controller {
	public function __construct(){
       	parent::__construct();
       	$this->load->model("account/account_m");
		$this->load->language('general');
		$this->load->language('dashboard');
		$this->load->language('register');
		$this->load->language('account');
		$this->load->language('notification');
		$this->load->library("email");
		$this->load->library("encrypt");
		$this->load->model('auth_m');

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
    }

	public function index(){
		$id_user=$this->session->userdata("id_user");
		$where = array("id_user" => $id_user);

		if($this->input->post("full_name")!=null){
			//Form Validation
			$this->form_validation->set_rules('full_name', lang("ph_nama_lengkap"), 'trim|required|regex_match[/^[a-zA-Z ]+$/]');
		    $this->form_validation->set_rules('id_fakultas', lang("fakultas"), 'required');
		    $this->form_validation->set_rules('id_jurusan', lang("jurusan"), 'required');
		    $this->form_validation->set_rules('id_prodi', lang("prodi"), 'required');
		    $this->form_validation->set_rules('phone_number', lang("ph_phone"), 'trim|required|numeric');
		    $this->form_validation->set_rules('address', lang("ph_addr"), 'trim|required');
		    $this->form_validation->set_rules('gender', lang("ph_gender"), 'trim|required');
		    $this->form_validation->set_rules('id_personal', lang("ph_nim_nidn"), 'trim|required|numeric');
		    $this->form_validation->set_rules('birth', lang("ph_birth"), 'required');
			
			if($this->form_validation->run()!=FALSE){
				$data=$this->input->post();
				if($this->account_m->find($id_user)){
					$where=array("id_user"=>$id_user);
					$this->session->set_flashdata('successMessage', lang('success'));
					$this->account_m->edit_personal_data($data,$where);
				} else{
					$this->session->set_flashdata('errorMessage', lang('not_find'));
				}
			} else {
				$this->session->set_flashdata('errorMessage', validation_errors());
			}
			redirect(base_url("account/edit"));
		} else if($this->input->post("password")!=null){
			//Form validation
			$this->form_validation->set_rules('password_lama', lang("ph_password"), 'trim|required');
		    $this->form_validation->set_rules('password', lang("ph_password"), 'trim|required|min_length[8]');
		    $this->form_validation->set_rules('repassword', lang("ph_konfirmasi"), 'trim|required|matches[password]');
		    
			if($this->form_validation->run()!=FALSE){
				$data=$this->input->post();
				if($this->account_m->validate_password($id_user,md5($data['password_lama']))){
					$ubah=array(
						'password' => md5($data['password']),
						'repassword' => $data['repassword']
					);
					if($this->account_m->ubah_data_user($ubah,$where)){
						$this->session->set_flashdata('successMessage', lang('success'));
					} else{
						$this->session->set_flashdata('errorMessage', lang('failed'));
					}
				} else{
					$this->session->set_flashdata('errorMessage', lang('invalid_pass'));
				}
			} else {
				$this->session->set_flashdata('errorMessage', validation_errors());
			}
			redirect(base_url("account/edit"));
		} else if($this->input->post("email")!=null){
			//Form Validation
			$this->form_validation->set_rules('email', lang("ph_email"), 'required|valid_email|is_unique[user.email]');

			$valid_email=explode("@", $this->input->post('email'))[1];

			if($valid_email!="um.ac.id" && $valid_email!="students.um.ac.id"){
				$this->session->set_flashdata('errorMessage', lang("valid_email"));
				$this->session->set_flashdata($this->input->post());
				redirect(base_url('account/edit'));
			}

			if($this->form_validation->run()!=FALSE){
				$data=$this->input->post();
				if($this->account_m->ubah_data_user($data,$where)){
					/* Start Notifikasi Email */
					$user = $this->account_m->find($data['id_user']);
		    		$body = "Terimakasih telah menggunakan layanan TPP UM! <br><br>Proses ini sebentar lagi selesai! Silahkan verifikasi alamat email Anda dan segera unggaah karya Anda sendiri.";

					$this->notif_email(array(
						"email" => $user['email'],
						'full_name' => $user['full_name'],
						'link' => urlencode($this->encrypt->encode($data['email'],'tpp-um-2018--wse')),
						"body" => $body,
						"subject" => "Verifikasi Email di TPP UM"
					));
					/* End Notifikasi Email */

					$cek = $this->auth_m->logout($this->session->userdata("id_user"));
					$this->session->unset_userdata('email');

					$this->session->set_flashdata('successMessage', lang('verify_your_email'));
					redirect();
				} else{
					$this->session->set_flashdata('errorMessage', lang('failed'));
				}
			} else {
				$this->session->set_flashdata('errorMessage', validation_errors());
			}
			redirect(base_url("account/edit"));
		} else if(isset($_FILES['url_photo']['name'])){
			$this->load->library('upload');

			$nmfile = "TPP_PP_".time();
	        $config['upload_path'] = "./dist/pp_users/";
	        $config['allowed_types'] = 'jpg|png|jpeg|bmp';
	        $config['max_size'] = '2048';
	        $config['max_width']  = '10576';
	        $config['max_height']  = '5536';
	        $config['file_name'] = $nmfile;

	        $this->upload->initialize($config);
	        if ($this->upload->do_upload('url_photo')){
	        	$data_photo = $this->upload->data();
	        	$data=array("url_photo"=>$data_photo['file_name']);

	        	if($this->input->post("old_url_photo")!=null){
	        		$old_url_photo=$this->input->post("old_url_photo");
	        		unlink("./dist/pp_users/$old_url_photo");
	        	}

	        	if($this->account_m->edit_personal_data($data,array("id_user" => $id_user))){
	        		$this->session->set_flashdata('successMessage', lang('success'));
	        	} else{
	        		$this->session->set_flashdata('errorMessage', lang('failed'));
	        	}
	        } else {
	        	$this->session->set_flashdata('errorMessage', $this->upload->display_errors());
	        }
	        redirect(base_url("account/edit"));
		} else {
			$data['user']=$this->account_m->get_user_data($id_user);
			$this->load->view('content/account/index',$data);
		}
	}
	function notif_email($data){
		$content = $this->load->view("content/notification/email_verification",$data,TRUE);

	    $this->email->to($data['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
}