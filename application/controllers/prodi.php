<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {
	public function __construct(){
       parent::__construct();
		$this->load->language('general');
		$this->load->model("prodi_m","pm");
    }

	public function get_jurusan(){
		$id_fakultas = $this->input->post('id_fakultas');
		
		$data = json_encode($this->pm->get_jurusan($id_fakultas));
		$this->output->set_content_type("application/json");
		$this->output->set_output($data);
	}
	public function get_prodi(){
		$id_jurusan = $this->input->post('id_jurusan');
		
		$data = json_encode($this->pm->get_prodi($id_jurusan));
		$this->output->set_content_type("application/json");
		$this->output->set_output($data);
	}
}
