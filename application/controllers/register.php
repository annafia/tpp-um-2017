<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('dashboard/dashboard_m');
       	$this->load->model('register/register_m');    	
		$this->load->language('general');
		$this->load->language('register');
		$this->load->language('notification');
		$this->load->library("email");
		$this->load->library("encrypt");
		/*
		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}*/
		
    }

	public function index(){
		if (!$this->input->post("full_name")) {
		    // $message = $this->load->view("partials/select/faculty",'',TRUE);
		    
			$this->load->view('auth/register');
		} else{
			$this->form_validation->set_rules('full_name', lang("ph_nama_lengkap"), 'trim|required|regex_match[/^[a-zA-Z ]+$/]');
		    $this->form_validation->set_rules('id_fakultas', lang("fakultas"), 'required');
		    $this->form_validation->set_rules('id_jurusan', lang("jurusan"), 'required');
		    $this->form_validation->set_rules('id_prodi', lang("prodi"), 'required');
		    $this->form_validation->set_rules('phone_number', lang("ph_phone"), 'trim|required|numeric');
		    $this->form_validation->set_rules('address', lang("ph_addr"), 'trim|required');
		    $this->form_validation->set_rules('gender', lang("ph_gender"), 'trim|required');
		    $this->form_validation->set_rules('position', lang("ph_jabatan"), 'trim|required');
		    $this->form_validation->set_rules('id_personal', lang("ph_nim_nidn"), 'trim|required|numeric');
		    $this->form_validation->set_rules('birth', lang("ph_birth"), 'required');
			$this->form_validation->set_rules('email', lang("ph_email"), 'required|trim');
			$this->form_validation->set_rules('password', lang("ph_password"), 'trim|required|min_length[8]');
		    $this->form_validation->set_rules('repassword', lang("ph_konfirmasi"), 'trim|required|matches[password]');
			$this->form_validation->set_message("regex_match",lang("hanya_huruf"));

			$data = $this->input->post();

			if($data['position']==1){
				$data['email'] .= "@students.um.ac.id";
			} else{
				$data['email'] .= "@um.ac.id";
			}
			$find = $this->register_m->find($data['email']);
			if($find){
				$this->session->set_flashdata('errorMessage', 'Email Sudah Terdaftar !');
				redirect(base_url('register'));
			}

			if($this->form_validation->run()!=FALSE){
				$id_user = $this->register_m->create_user(array(
					'email' => $data['email'],
					'password' => md5($data['password']),
					'repassword' => $data['repassword'],
				));
				$create = $this->register_m->create_personal_data(array(
					'id_user' => $id_user,
					'full_name' => $data['full_name'],
					'id_fakultas' => $data['id_fakultas'],
					'id_jurusan' => $data['id_jurusan'],
					'id_prodi' => $data['id_prodi'],
					'address' => $data['address'],
					'gender' => $data['gender'],
					'no_user' => date("dmY His"),
					'phone_number' => $data['phone_number'],
					'birth' => $data['birth'],
					'position' => $data['position'],
					'id_personal' => $data['id_personal']
				));
				if ($create) {
					/* Start Notifikasi Email */
		    		$body = "Terimakasih telah bergabung di TPP UM! <br><br>Proses ini sebentar lagi selesai! Silahkan verifikasi alamat email Anda dan segera unggaah karya Anda sendiri.";

					$this->notif_email(array(
						"email" => $data['email'],
						'full_name' => $data['full_name'],
						'link' => urlencode($this->encrypt->encode($data['email'],'tpp-um-2018--wse')),
						"body" => $body,
						"subject" => "Verifikasi Email di TPP UM"
					));
					/* End Notifikasi Email */
					$successRegister = 'Registrasi Berhasil! Email Verifikasi telah kami kirimkan ke '.$data['email'].'. Silahkan verifikasi email Anda untuk mengaktifkan akun Anda !';
					$this->session->set_flashdata('successMessage', $successRegister);
					redirect(base_url());
				}
				else {
					$this->session->set_flashdata('errorMessage', lang('failed'));
				}
			} else {
				$this->session->set_flashdata('errorMessage', validation_errors());
			}

			$this->session->set_flashdata("post",$this->input->post());
			redirect(base_url("register"));
		}
	}
	function notif_email($data){
		$content = $this->load->view("content/notification/email_verification",$data,TRUE);

	    $this->email->to($data['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	public function verify_email(){
		$enc_email = urldecode($this->input->get("enc"));
		$email = $this->encrypt->decode($enc_email,'tpp-um-2018--wse');
		$active = $this->register_m->active($email);
		if($active){
			$this->session->set_flashdata('successMessage', lang('success_verify'));
		} else{
			$this->session->set_flashdata('errorMessage', validation_errors());
		}
		redirect(base_url());
	}
}