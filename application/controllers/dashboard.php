<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('dashboard/dashboard_m','dm');
		$this->load->language('general');
		$this->load->language('dashboard');
		$this->load->language('gallery');
		$this->load->library('pagination');
		

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',6)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>6,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }

	public function index(){
		$jumlah_data = $this->dm->total_image();
		
		$config['base_url'] = base_url("dashboard/index");
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 12;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = '<i class="material-icons">chevron_right</i>';
		$config['prev_link'] = '<i class="material-icons">chevron_left</i>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';

		$from = $this->uri->segment(3);

		$this->pagination->initialize($config);

		$data= array(
			'all_paper' => $this->dm->all_submission(), 
			'onprocess' => $this->dm->onprocess_submission(), 
			'rejected' => $this->dm->rejected_submission(),
			'gallery' => $this->dm->show_gallery($config['per_page'],$from),
			'vis' => $this->dm->get_visitors()
		);
		$this->load->view('content/dashboard/index',$data);
	}
}
