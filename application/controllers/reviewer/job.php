<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job extends CI_Controller {
	public function __construct(){
       parent::__construct();
       	$this->load->model('reviewer/job_m');       	
		$this->load->language('general');
		$this->load->language('job');
		$this->load->helper(array('form', 'url'));
		$this->load->model('notification/notification_m','nm');
		$this->load->language('notification');
		$this->load->library("email");

		if(!$this->session->userdata('email')){
			$this->session->set_flashdata('errorMessage', lang('not_login'));
			redirect('welcome');
		}
		/*---------------- Visitor --------------------*/
		$ip_user = getClientIP();
		$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$find_ip = $this->db->select("TIME_TO_SEC(TIMEDIFF(NOW(), (create_at))) as diff")
							->where('ip_user',$ip_user)
							->where('page',3)
							->where('host',$host_name)
							->having('diff <',3600)
							->get('visitor')->row_array();
		if(!$find_ip){
			$this->db->insert('visitor',array('page'=>3,'ip_user'=>$ip_user,'host'=> $host_name));
		}
		/*---------------- Visitor --------------------*/
    }
    /*------------------------------ Start Local Function ------------------------------*/
    function is_success($res,$redirect,$errorMessage){
		if(!$res){
			$this->session->set_flashdata('errorMessage', $errorMessage);
			redirect($redirect);
		}
	}
	function current_user_notif_paper($data){
		$content = $this->load->view("content/notification/paper",$data,TRUE);

	    $this->email->to($data['user']['email']);
	    $this->email->from("tppum2017@gmail.com", 'TPP UM');
	    $this->email->subject($data['subject']);
	    $this->email->message($content);

	    $this->email->send();
	}
	public function validation_review(){
		$this->form_validation->set_rules('general_comment', lang('komentarUmum') ,'trim|required');
		$this->form_validation->set_rules('decission_status', lang('putusan') ,'trim|required');

		return $this->form_validation->run()!=FALSE;
	}
	public function upload_paper($file_name,$path,$field,$redirect){
		$this->load->library('upload');

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'docx|doc|pdf';
        $config['max_size'] = '10240';
        $config['file_name'] = $file_name;
        $config['overwrite'] = true;

        $this->upload->initialize($config);
        $this->is_success($this->upload->do_upload($field),$redirect,$this->upload->display_errors());
        $file = $this->upload->data();

        return $file['file_name'];
	}
	function admin_notif_paper($data){
		$admin=$this->nm->get_admin();

		foreach ($admin as $a) {
			$data['user'] = $a;
			$content = $this->load->view("content/notification/paper",$data,TRUE);

		    $this->email->to($a['email']);
		    $this->email->from("tppum2017@gmail.com", 'TPP UM');
		    $this->email->subject($data['subject']);
		    $this->email->message($content);

		    $this->email->send();
		}
	}
	/*------------------------------ End Local Function ------------------------------*/

	public function index(){
		$this->load->view('content/reviewer/index', array(
			'get' => $this->job_m->get_job()
		));
	}
	public function detail($id_submission){
		$redirect = base_url('reviewer/job');
		
		$find = $this->job_m->find_job($id_submission);
		$this->is_success($find,$redirect,lang("not_find"));

		$this->load->view("content/reviewer/detail", array(
			"submission" => $find,
			"member" => $this->job_m->get_member($id_submission),
			"plagiarism" => $this->job_m->get_detail_plagiarism($id_submission),
			"hst_review" => $this->job_m->get_detail_review($id_submission),
		));
	}

	public function save_review() {
		$data = $this->input->post();
		$not_found = base_url('reviewer/job/');
		$redirect = base_url('reviewer/job/detail/'.$data['id_submission']);
		$submit = $data['submit']; unset($data['submit']);

		$find = $this->job_m->find_job($data['id_submission']);
		$this->is_success($find,$not_found,lang("not_find"));

		/*------------------ Hanya Submit ------------------*/
		if($submit==2){
			if($find['review_file']!=""){
				$save_submit = $this->job_m->submit($data['id_submission']);
				$this->is_success($save_submit,$redirect,lang("failed"));

				/*------------------- Start Notifikasi Email -------------------*/
				$subm = $this->nm->find_submission($data['id_submission']);
				$user = $this->nm->get_current_user($this->session->userdata('id_user'));

				$body_admin = "Review artikel dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name']." telah selesai.";
				$body_admin = "Review Artikel dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name']." telah kami terima. Terimakasih atas kerja keras Anda untuk Me-Review artikel ini.";

				$this->admin_notif_paper(array(
					"subm" => $subm,
					"body" => $body_admin,
					"subject" => "Review Artikel Selesai !"
				));
				$this->current_user_notif_paper(array(
					"subm" => $subm,
					"user" => $user,
					"body" => $body,
					"subject" => "Terimakasih Telah Me-Review !"
				));
				/*------------------- End Notifikasi Email -------------------*/

				$this->session->set_flashdata('successMessage', lang('success'));
				redirect($not_found);
			}else{
				$this->session->set_flashdata('errorMessage', lang('not_find'));
				redirect($redirect);
			}
		} 
		/*------------------ Hanya Submit ------------------*/

		$this->is_success($this->validation_review(),$redirect,validation_errors());

		$rev_ke = $this->job_m->count_review($data['id_submission']);

		$file_name = "TPP_Review".$rev_ke['jml']."By".$rev_ke['full_name']."_".generate_id($data['id_submission'])."_".$find['full_name'];
        $data['review_file']=$this->upload_paper($file_name,"./dist/review/","review_file",$redirect);

		$save = $this->job_m->save_review($data);
		$this->is_success($save,$redirect,lang("failed"));

		/*------------------ Simpan dan Submit ------------------*/
		if($submit==1){
			$save_submit = $this->job_m->submit($data['id_submission']);
			$this->session->set_flashdata('successMessage', lang('success'));

			/*------------------- Start Notifikasi Email -------------------*/
			$subm = $this->nm->find_submission($data['id_submission']);

			$body_admin = "Review artikel dengan judul ".$subm['tittle']." yang ditulis oleh ".$subm['full_name']." telah selesai.";

			$this->admin_notif_paper(array(
				"subm" => $subm,
				"body" => $body_admin,
				"subject" => "Review Artikel Selesai !"
			));
			/*------------------- End Notifikasi Email -------------------*/
			redirect($not_found);
		}
		/*------------------ Simpan dan Submit ------------------*/

		$this->session->set_flashdata('successMessage', lang('success'));
		redirect($redirect);
	}
}
