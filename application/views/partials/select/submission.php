<div class="col-sm-12 col-md-12">
    <h2 class="card-inside-title"><?= lang('list') ?></h2>
    <select class="form-control show-tick" name="id_submission" data-live-search="true" required>
        <?php foreach($submission as $row){ ?>
            <option value="<?= $row['id_submission'] ?>" selected><?= ucwords($row['tittle']) ?></option>
        <?php } ?>
    </select>
</div>