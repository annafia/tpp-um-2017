<div class="col-sm-12 col-md-6">
    <h2 class="card-inside-title"><?= lang('bidang_riset') ?></h2>
    <select class="form-control show-tick" name="id_topic" data-live-search="true">
        <option value="" disabled="" selected="">-- <?= lang('bidang_riset') ?> --</option>
        <?php foreach($this->db->order_by('nm_topic', 'ASC')->get('topic')->result_array() as $data){ ?>
            <option value="<?= $data['id_topic'] ?>"><?= ucwords($data['nm_topic']) ?></option>
        <?php } ?>
    </select>
</div>