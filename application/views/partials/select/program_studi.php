<div class="col-sm-12 col-md-6">
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">account_balance</i>
        </span>
        <select class="form-control show-tick" name="id_fakultas" id="fakultas" data-live-search="true">
	        <option disabled="" selected="">-- <?= lang('fakultas') ?> --</option>
	        <?php foreach($this->db->order_by('fakultas', 'ASC')->get('fakultas')->result_array() as $row){ ?>
	       		<option value="<?= $row['id_fakultas'] ?>"><?= ucwords($row['fakultas']) ?></option>
	       	<?php } ?>
	    </select>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">business</i>
        </span>
        <select class="form-control show-tick" data-live-search="true" name="id_jurusan" id="jurusan" required="">
	        <option disabled="" selected="">-- <?= lang('jurusan') ?> --</option>
	    </select>
    </div>
</div>
<div class="col-sm-12 col-md-12">
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">view_quilt</i>
        </span>
        <select class="form-control show-tick" name="id_prodi" data-live-search="true" id="prodi" required="">
	        <option disabled="" selected="">-- <?= lang('prodi') ?> --</option>
	    </select>
    </div>
</div>