<div class="col-sm-12">
	<h2 class="card-inside-title"><?= lang('pilih_topik') ?> <span style="color:red">*</span></h2>
    <select class="form-control show-tick" name="id_topic" required="" data-live-search="true">
        <option value="" disabled="" selected="">-- <?= lang('pilih_topik') ?> --</option>
        <?php foreach($this->db->get('topic')->result_array() as $data){ ?>
        	<option value="<?= $data['id_topic'] ?>" <?= $topic==$data['id_topic']?"selected":"" ?> ><?= ucwords($data['nm_topic']) ?></option>
        <?php } ?>
    </select>
</div>
