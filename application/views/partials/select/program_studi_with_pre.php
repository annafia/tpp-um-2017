<?php 
    $id_prodi=$data['id_prodi'];
    $id_jurusan=$data['id_jurusan'];
    $id_fakultas=$data['id_fakultas'];
?>
<div class="col-sm-12 col-md-6">
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">account_balance</i>
        </span>
        <select class="form-control selectpicker show-tick" name="id_fakultas" data-live-search="true" id="fakultas" required="">
            <?php foreach($this->db->order_by('fakultas', 'ASC')->get('fakultas')->result_array() as $row){ ?>
                <option value="<?= $row['id_fakultas'] ?>" 
                    <?= $id_fakultas==$row['id_fakultas']?"selected":"" ?>
                    ><?= ucwords($row['fakultas']) ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">business</i>
        </span>
        <select class="form-control selectpicker show-tick" data-live-search="true" name="id_jurusan" id="jurusan" required="">
            <?php 
                $fakultas = $id_fakultas/1000;
                foreach($this->db->order_by('jurusan', 'ASC')
                                ->like("id_jurusan","$fakultas","after")
                                ->get('jurusan')
                                ->result_array() as $row){ 
            ?>
                <option value="<?= $row['id_jurusan'] ?>"
                    <?= $id_jurusan==$row['id_jurusan']?"selected":"" ?>
                    ><?= ucwords($row['jurusan']) ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="col-sm-12 col-md-12">
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">view_quilt</i>
        </span>
        <select class="form-control selectpicker show-tick" name="id_prodi" data-live-search="true" id="prodi" required="">
            <?php 
                $jurusan = $id_jurusan/100;
                foreach($this->db->order_by('program_studi', 'ASC')
                                ->like("id_prodi","$jurusan","after")
                                ->get('prodi')
                                ->result_array() as $row){ 
            ?>
                <option value="<?= $row['id_prodi'] ?>"
                    <?= $id_prodi==$row['id_prodi']?"selected":"" ?>
                    ><?= ucwords($row['program_studi']) ?></option>
            <?php } ?>
        </select>
    </div>
</div>