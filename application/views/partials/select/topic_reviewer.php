<select class="form-control show-tick" name="id_topic" id="topic" data-live-search="true">
    <option value="" disabled="" selected="">-- <?= lang('pilih_topik') ?> --</option>
    <?php foreach($this->db->order_by("nm_topic","ASC")->get('topic')->result_array() as $data){ ?>
    	<option value="<?= $data['id_topic'] ?>" <?= $topic==$data['id_topic']?"selected":"" ?> ><?= ucwords($data['nm_topic']) ?></option>
    <?php } ?>
</select>
