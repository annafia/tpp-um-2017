<div class="col-md-12">
    <p><b><?= lang('pilih_pegawai') ?></b></p>
    <select class="form-control show-tick" data-live-search="true" name="id_user[]" multiple>
         <?php foreach($this->db->join('data_privilage dp', 'dp.id_user = pd.id_user')->where('privilage_id', 5)->where('status', 1)->get('personal_data pd')->result_array() as $user){ ?>
            <option value="<?= $user['id_user'] ?>"><?= $user['full_name']." ~ ".$user['bidang_review'] ?></option>
        <?php } ?>
    </select>
</div>