<div class="col-sm-12 col-md-6">
    <h2 class="card-inside-title"><?= lang('fakultas') ?></h2>
    <div class="input-group">
        <select class="form-control selectpicker show-tick" name="id_fakultas" id="fakultas" data-live-search="true">
	        <option value="" selected="">-- <?= lang('semua_fakultas') ?> --</option>
	        <?php foreach($this->db->order_by('fakultas', 'ASC')->get('fakultas')->result_array() as $row){ ?>
	       		<option value="<?= $row['id_fakultas'] ?>"><?= ucwords($row['fakultas']) ?></option>
	       	<?php } ?>
	    </select>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <h2 class="card-inside-title"><?= lang('jurusan') ?></h2>
    <div class="input-group">
        <select class="form-control selectpicker show-tick" data-live-search="true" name="id_jurusan" id="jurusan">
	        <option value="" selected="">-- <?= lang('semua_jurusan') ?> --</option>
	    </select>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <h2 class="card-inside-title"><?= lang('prodi') ?></h2>
    <div class="input-group">
        <select class="form-control selectpicker show-tick" name="id_prodi" data-live-search="true" id="prodi">
	        <option value="" selected="">-- <?= lang('semua_prodi') ?> --</option>
	    </select>
    </div>
</div>