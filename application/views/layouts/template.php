<?php 
    $data_user=$this->db->get_where("personal_data",array("id_user" => $this->session->userdata("id_user")))->row_array();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>TPP UM | <?= date('Y') ?></title>
  <!-- Favicon-->
  <link rel="icon" type="image/png" href="<?= base_url('dist/images/logo-fix.png') ?>">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  <link href="<?= base_url('dist/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

  <!-- Waves Effect Css -->
  <link href="<?= base_url('dist/plugins/node-waves/waves.css') ?>" rel="stylesheet" />

  <!-- Animation Css -->
  <link href="<?= base_url('dist/plugins/animate-css/animate.css') ?>" rel="stylesheet" />

  <!-- Custom Css -->
  <link href="<?= base_url('dist/css/style.css') ?>" rel="stylesheet">

  <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
  <link href="<?= base_url('dist/css/themes/all-themes.css') ?>" rel="stylesheet" />

  <!-- SWEET ALERT -->
  <link rel="stylesheet" type="text/css" href="<?= base_url('dist/plugins/sweetalert/sweetalert.css') ?>">
 
  <?php render('css') ?>
</head>
    <?php $this->load->view('layouts/navbar_color') ?>
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-indigo">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <div class="overlay"></div>
    
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?= base_url() ?>"> <?= lang('tittle') ?> - <?= date('Y') ?> </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?= base_url() ?>"><i class="material-icons" title="dashboard">dashboard</i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">exit_to_app</i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header"><?= lang('keluar_dari_aplikasi') ?></li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="thumbnail">
                                                <?php if($data_user['url_photo']==null){ ?>
                                                    <a href="<?= base_url('account/edit') ?>"><img src="<?= base_url('dist/images/default.png') ?>"></a>
                                                <?php } else{ ?>
                                                    <a href="<?= base_url('account/edit') ?>"><img src="<?= base_url('dist/pp_users/'.$data_user['url_photo']) ?>"></a>
                                                <?php } ?>
                                            </div>
                                            <p class="text-center"><?= $data_user['full_name'] ?></p>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a class="btn btn-primary" style="color:white" href="<?= base_url('welcome/logout') ?>/<?= $this->session->userdata('id_user') ?>"><?= lang('logout') ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <!-- Side Bar -->
    <section>
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <?php $this->load->view('layouts/bg') ?>
                <div class="image">
                    <img src="<?= base_url('dist/images/logo.png') ?>" width="50" height="50" alt="UM" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= lang('anda_masuk_sebagai'); ucwords(role($this->session->userdata('role'))) ?></div>
                    <div class="email">Hai <?= $data_user['full_name'] ?> !</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?= base_url('account/edit') ?>"><i class="material-icons">person</i><?= lang('profil') ?></a></li>
                            <li role="seperator" class="divider"></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="<?= base_url('welcome/logout') ?>/<?= $this->session->userdata('id_user') ?>"><i class="material-icons">input</i>Keluar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header"><?= lang('navigasi_menu') ?></li>
                    <li id="home">
                        <a href="<?= base_url() ?>">
                            <i class="material-icons">dashboard</i>
                            <span><?= lang('home') ?></span>
                        </a>
                    </li>
                    <?php $this->load->view('layouts/menu_privilage') ?>
                    
                    <li class="header"><?= lang('hak_akses') ?></li>
                    <li id="privilege">
                        <a href="<?= base_url('account/privilege') ?>">
                            <i class="material-icons">account_box</i>
                            <span><?= lang('pengajuan_hak_akses') ?></span>
                        </a>
                    </li>
                    <li id="account">
                        <a href="<?= base_url("account/edit") ?>">
                            <i class="material-icons">account_box</i>
                            <span><?= lang('akun') ?></span>
                        </a>
                    </li>
                    
                    <?php render('toolbar') ?>
                </ul>
            </div>
            <!-- #Menu -->

            <!-- Footer -->
            <?php $this->load->view('layouts/footer') ?>
            <!-- #Footer -->
        </aside>
    </section>

    <section class="content">
      <div class="container-fluid">
        <?php render('content') ?>
      </div>
    </section>

<!-- Jquery Core Js -->
<script src="<?= base_url('dist/plugins/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap Core Js -->
<script src="<?= base_url('dist/plugins/bootstrap/js/bootstrap.js') ?>"></script>
<!-- Select Plugin Js -->
<script src="<?= base_url('dist/plugins/bootstrap-select/js/bootstrap-select.js') ?>"></script>
<!-- Waves Effect Plugin Js -->
<script src="<?= base_url('dist/plugins/node-waves/waves.js') ?>"></script>
<!-- Sweet Alert -->
<script src="<?= base_url('dist/plugins/sweetalert/sweetalert.min.js') ?>"></script>
<!-- Slimscroll Plugin Js -->
<script src="<?= base_url('dist/plugins/jquery-slimscroll/jquery.slimscroll.js') ?>"></script>
<!-- Custom Js -->
<script src="<?= base_url('dist') ?>/js/pages/ui/modals.js"></script>
<script src="<?= base_url('dist/js/admin.js') ?>"></script>

<?php render('js') ?>
<?php render('script')?>
</body>
</html>
