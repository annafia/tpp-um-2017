<div class="legal">
    <div class="copyright">
        &copy; 2017 <a href="<?= base_url() ?>">TPP UM - IT Developer</a>.
    </div>
    <div class="version">
        <b>Version: </b> 1.0.0
    </div>
</div>