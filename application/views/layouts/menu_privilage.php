<?php
$id_user = $this->session->userdata('id_user');  
$email = $this->session->userdata('email');
$role = $this->session->userdata('role');

$privilege = $this->db->where('id_user', $id_user)
					  ->where('status', 1)
                      ->get('data_privilage')
                      ->result_array();

// MESSAGE

// Admin-Plagiarism
$bt_plag = $this->db->select('count(*) as tot')
					->where('verified',2)
					->where('status_plagiarism',0)
					->get('submission s')
					->row_array();
$dt_plag = $this->db->select('count(*) as tot')
					->join('submission s','s.id_submission = p.id_submission')
					->where('active',1)
					->where('status_plag',1)
					->where('status_plagiarism',3)
					->get('plagiarism p')
					->row_array();
// Admin-Reviewer
$bt_rev = $this->db->select('count(*) as tot')
					->where('status_plagiarism',1)
					->where('status_reviewer',0)
					->get('submission')
					->row_array();
$dt_rev = $this->db->select('count(*) as tot')
					->where('status_reviewer',3)
					->get('submission')
					->row_array();

// Admin-Translator
$bt_trans = $this->db->select('count(*) as tot')
					->where('status_reviewer',1)
					->where('status_translator',0)
					->get('submission')
					->row_array();
$dt_trans = $this->db->select('count(*) as tot')
					->join('translation t','s.id_submission=t.id_submission')
					->where('status_reviewer',3)
					->where('status_translation',1)
					->get('submission s')
					->row_array();

// Admin-Submit
$msg_valid = $this->db->select('count(*) as tot')
					->where('status', 6)
					->get('submission')
					->row_array();

// Plagiarism
$job_plag = $this->db->select('count(*) as tot')
                      ->join('submission s', 's.id_submission = p.id_submission')
                      ->where('status_plag', 0)
                      ->where('active',1)
                      ->where('p.id_user',$id_user)
                      ->get('plagiarism p')
                      ->row_array();

// Reviewer
$get_notif_rev = $this->db->select('count(*) as tot')
                      ->where('id_user',$id_user)
                      ->where('status_review',0)
                      ->where('active',1)
                      ->get('review')
                      ->row_array();

// Translator
$get_notif_trans = $this->db->select('count(*) as tot')
					->where('status_translation',0)
					->where('t.id_user',$id_user)
					->join('translation t','t.id_submission=s.id_submission','left')
					->get('submission s')
					->row_array();

//Author Submit
$query = "select *from submission where (status = 1 OR status = 4) AND id_user = '".$this->session->userdata("id_user")."'";
$author_submit = $this->db->query($query)->num_rows();
$menu_author = 0;

foreach($privilege as $row){ ?>
	<!-- ADMIN -->
	<?php if($row['privilage_id'] == 2 || $row['privilage_id'] == 1){ ?>
		<li class="header"><?= lang('aktifitas_admin') ?></li>
		<li id="adm_job_plagiarism">
		    <a href="javascript:void(0);" class="menu-toggle">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('plagiarism_checker') ?></span>
		        <?php if($bt_plag['tot']+$dt_plag['tot'] > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px">
		        		<?= $bt_plag['tot']+$dt_plag['tot'] ?>
	        		</label>
		    	<?php } ?>
		    </a>
		    <ul class="ml-menu">
		        <li id="send_job_plagiarism">
		            <a href="<?= base_url('admin/to_plagiarism/send_job_plagiarism') ?>">
		            	<?= lang('beri_tugas') ?>
		            	<?php if($bt_plag['tot'] > 0){ ?>
				        	<label class="bg-red text-white" style="padding: 2px; margin-left: 10px">
				        		<?= $bt_plag['tot'] ?>
			        		</label>
				    	<?php } ?>
		            </a>
		        </li>
		        <li id="list_job_plagiarism">
		            <a href="<?= base_url('admin/to_plagiarism/administrators_plagiarism') ?>">
		            	<?= lang('daftar_tugas') ?>
		            	<?php if($dt_plag['tot'] > 0){ ?>
				        	<label class="bg-red text-white" style="padding: 2px; margin-left: 10px">
				        		<?= $dt_plag['tot'] ?>
			        		</label>
				    	<?php } ?>
		            </a>
		        </li>
		    </ul>
		</li>
		<li id="adm_job_reviewer">
		    <a href="javascript:void(0);" class="menu-toggle">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('reviewer') ?></span>
		        <?php if($bt_rev['tot']+$dt_rev['tot'] > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px">
		        		<?= $bt_rev['tot']+$dt_rev['tot'] ?>
		        	</label>
		        <?php } ?>
		    </a>
		    <ul class="ml-menu">
		        <li id="send_job_reviewer">
		            <a href="<?= base_url('admin/to_reviewer') ?>">
		            	<?= lang('beri_tugas') ?>
		            	<?php if($bt_rev['tot'] > 0){ ?>
				        	<label class="bg-red text-white" style="padding: 2px; margin-left: 10px">
				        		<?= $bt_rev['tot'] ?>
			        		</label>
				    	<?php } ?>
	            	</a>
		        </li>
		        <li id="list_job_reviewer">
		            <a href="<?= base_url('admin/to_reviewer/administrators_reviewer') ?>">
		            	<?= lang('daftar_tugas') ?>
		            	<?php if($dt_rev['tot'] > 0){ ?>
				        	<label class="bg-red text-white" style="padding: 2px; margin-left: 10px">
				        		<?= $dt_rev['tot'] ?>
			        		</label>
				    	<?php } ?>
	            	</a>
		        </li>
		    </ul>
		</li>
		<li id="adm_job_translator">
		    <a href="javascript:void(0);" class="menu-toggle">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('translator') ?></span>
		        <?php if($bt_trans['tot']+$dt_trans['tot'] > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px">
		        		<?= $bt_trans['tot']+$dt_trans['tot'] ?>
		        	</label>
		        <?php } ?>
		    </a>
		    <ul class="ml-menu">
		        <li id="send_job_translator">
		            <a href="<?= base_url('admin/to_translator') ?>">
		            	<?= lang('beri_tugas') ?>
	            		<?php if($bt_trans['tot'] > 0){ ?>
				        	<label class="bg-red text-white" style="padding: 2px; margin-left: 10px">
				        		<?= $bt_trans['tot'] ?>
			        		</label>
				    	<?php } ?>
	            	</a>
		        </li>
		        <li id="list_job_translator">
		            <a href="<?= base_url('admin/to_translator/administrators_translator') ?>">
		            	<?= lang('daftar_tugas') ?>
	            		<?php if($dt_trans['tot'] > 0){ ?>
				        	<label class="bg-red text-white" style="padding: 2px; margin-left: 10px">
				        		<?= $dt_trans['tot'] ?>
			        		</label>
				    	<?php } ?>
	            	</a>
		        </li>
		    </ul>
		</li>
		<li id="valid_submit">
		    <a href="<?= base_url('admin/activity/submitted_proof') ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('validasi_bukti_submit') ?></span>
		        <?php if($msg_valid['tot'] > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px"><?= $msg_valid['tot'] ?></label>
		        <?php } ?>
		    </a>
		</li>
		<li id="report">
		    <a href="<?= base_url('admin/activity') ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('cetak_laporan') ?></span>
		    </a>
		</li>
		<li id="list_employee">
		    <a href="<?= base_url('admin/activity/employee') ?>">
		        <i class="material-icons">group</i>
		        <span><?= lang('daftar_pegawai') ?></span>
		    </a>
		</li>
		<?php 
		    $proof = $this->db->join('data_privilage', 'data_privilage.id_user = user.id_user')
		                      ->where('data_privilage.status', 0)
		                      ->group_by('data_privilage.id_user')
		                      ->get('user')
		                      ->num_rows();
		?>
		<li id="adm_privilege">
		    <a href="<?= base_url('privilage/using') ?>">
		        <i class="material-icons">group</i>
		        <span><?= lang('kelola_hak_akses_pengguna') ?></span>
		        <?php if($proof != 0){ ?>
		            <label class="bg-red text-white" style="padding: 5px; margin-left: 10px"><?= $proof ?></label>
		        <?php } ?>
		    </a>
		</li>
		<li id="gallery">
		    <a href="<?= base_url('admin/gallery') ?>">
		        <i class="material-icons">photo_library</i>
		        <span><?= lang('kelola_galeri') ?></span>
		    </a>
		</li>
		<li class="header"><?= lang('master') ?></li>
		<li id="master_topic">
		    <a href="<?= base_url('master/topic') ?>">
		        <i class="material-icons">local_offer</i>
		        <span><?= lang('topic') ?></span>
		    </a>
		</li>
		<li id="master_privilege">
		    <a href="<?= base_url('master/privilage') ?>">
		        <i class="material-icons">local_offer</i>
		        <span><?= lang('hak_akses') ?></span>
		    </a>
		</li>
		
		<li id="kelola_prodi">
		    <a href="javascript:void(0);" class="menu-toggle">
		        <i class="material-icons">local_offer</i>
		        <span><?= lang('kelola_prodi') ?></span>
		    </a>
		    <ul class="ml-menu">
		    	<li id="master_fakultas">
				    <a href="<?= base_url('master/prodi/fakultas') ?>">
				        <span><?= lang('fakultas') ?></span>
				    </a>
				</li>
		    	<li id="master_jurusan">
				    <a href="<?= base_url('master/prodi/jurusan') ?>">
				        <span><?= lang('jurusan') ?></span>
				    </a>
				</li>
		        <li id="master_prodi">
				    <a href="<?= base_url('master/prodi') ?>">
				        <span><?= lang('program_studi') ?></span>
				    </a>
				</li>
		    </ul>
		</li>
	<?php } ?>

	<!-- AUTHOR -->
	<?php if($row['privilage_id'] == 3  || $row['privilage_id'] == 1 || $role == 4){  
		if($menu_author==0){ $menu_author=1; 
			$status_paper = $this->db->select("count(*) as tot")
							->where("id_user", $this->session->userdata("id_user"))
							->where("verified <",2)
							->get("submission")
							->row_array();
		?>
		<li class="header"><?= lang('aktifitas_author') ?></li>
		<li id="upload_paper">
		    <a href="<?= base_url('paper/submission') ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('artikel_anda') ?></span>
		    </a>
		</li>
		<!-- <li id="status_paper">
		    <a href="<?= base_url('paper/submission/status').'/'.$this->session->userdata('id_user') ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('status_karya') ?></span>
		        <?php if($status_paper['tot'] > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px"><?= $status_paper['tot'] ?></label>
		        <?php } ?>
		    </a>
		</li> -->

		<li id="submit_paper">
		    <a href="<?= base_url("paper/submission/submit") ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('unggah_bukti_submit') ?></span>
		        <?php if($author_submit > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px"><?= $author_submit ?></label>
		        <?php } ?>
		    </a>
		</li>
	<?php }} ?> 

	<!-- PLAGIARISM -->
	<?php if($row['privilage_id'] == 4  || $row['privilage_id'] == 1){ ?>
		<li class="header"><?= lang('aktifitas_plagiarism') ?></li>
		<li id="job_plagiarism">
		    <a href="<?= base_url('plagiarism/job') ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('tugas_plagiasi') ?></span>
		        <?php if($job_plag['tot'] > 0){ ?>
		            <label class="bg-red text-white" style="padding: 5px; margin-left: 10px"><?= $job_plag['tot'] ?></label>
		        <?php } ?>
		    </a>
		</li>
	<?php } ?>

	<!-- REVIEWER -->
	<?php if($row['privilage_id'] == 5  || $row['privilage_id'] == 1){ ?>
		<li class="header"><?= lang('aktifitas_reviewer') ?></li>
		<li id="job_reviewer">
		    <a href="<?= base_url('reviewer/job') ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('tugas_reviewer') ?></span>
		        <?php if($get_notif_rev['tot'] > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px"><?= $get_notif_rev['tot'] ?></label>
		        <?php } ?>
		    </a>
		</li>
	<?php } ?>

	<!-- TRANSLATOR -->
	<?php if($row['privilage_id'] == 6  || $row['privilage_id'] == 1){ ?>
		<li class="header"><?= lang('aktifitas_translator') ?></li>
		<li id="job_translator">
		    <a href="<?= base_url('translator/job') ?>">
		        <i class="material-icons">assignment</i>
		        <span><?= lang('tugas_translator') ?></span>
		        <?php if($get_notif_trans['tot'] > 0){ ?>
		        	<label class="bg-red text-white" style="padding: 5px; margin-left: 10px"><?= $get_notif_trans['tot'] ?></label>
		        <?php } ?>
		    </a>
		</li>
	<?php } ?>
<?php } ?>