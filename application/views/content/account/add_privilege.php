<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('daftar_pengajuan_hak_akses') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('daftar_pengajuan_hak_akses') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role($this->session->userdata('role'))) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('daftar_pengajuan_hak_akses') ?>
                    </h2>
                </div>
                <div class="body">
                    <!-- Modals create -->
                    <div class="modal fade" id="insert" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <form id="form_validation" method="POST" action="<?= base_url('account/privilege/create') ?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel"><?= lang('tambah_baru') ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="id_user" value="<?= $this->session->userdata('id_user') ?>">
                                            <p><b><?= lang('ajukan_hak_akses') ?></b></p>
                                            <select class="form-control show-tick" name="privilage_id[]" multiple required="">
                                                <?php foreach($get_privilege as $prev){ ?>
                                                    <option <?php if(in_array($prev['privilage_id'], $select_prev)){echo 'selected';} ?> value="<?= $prev['privilage_id'] ?>"><?= ucwords($prev['privilage_nm']) ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End modals -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('nm_pengguna') ?></th>
                                    <th class="text-center"><?= lang('nm_hak_akses') ?></th>
                                    <th class="text-center"><?= lang('status') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>   
                            <tbody>
                                <?php $no=1; foreach($list as $row){ ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                        <td class="text-center"><?= ucwords($row['privilage_nm']) ?></td>
                                        <td class="text-center"><?= privilege($row['status']) ?></td>
                                        <td align="right">
                                            <button type="button" data-color="red" class="btn bg-red waves-effect" onclick="del(<?= $row['id_user'] ?>, <?= $row['no'] ?>)"><i class="material-icons" title="<?= lang('title_del') ?>">delete</i></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>                                   
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('nm_pengguna') ?></th>
                                    <th class="text-center"><?= lang('nm_hak_akses') ?></th>
                                    <th class="text-center"><?= lang('status') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot> 
                        </table>
                    </div>
                    <div class="footer">
                        <br>
                        <p><b><?= lang('ket') ?></b></p>
                        <p><?= lang('ket1') ?></p>
                    </div>
                    <button style="margin-top: 30px" type="button" data-color="light-blue" data-toggle="modal" data-target="#insert" class="btn bg-blue waves-effect"><?= lang('ubah_data') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist') ?>/plugins/multi-select/js/jquery.multi-select.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#privilege").addClass("active");
    function del(id_user, no){
        swal({
        title: "Apakah anda yakin?",
        text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('account/privilege/delete_privilege') ?>"+'/'+id_user+'/'+no;
            swal("Terhapus!", "Data anda berhasil dihapus.", "success");
        });
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>