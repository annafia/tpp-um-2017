<?php section('css'); ?> 
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link href="<?= base_url('dist') ?>/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/dropzone/dropzone.css" rel="stylesheet">
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<?php $this->load->view('partials/message') ?>
<div class="block-header">
    <h2><?= lang('akun') ?></h2>
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('dashboard') ?></a></li>
        </ol>
    </div>
</div>
<?php foreach($user as $u){} ?>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <?= lang("unggah_foto") ?>
                    <small class="font-bold col-red"><?= lang('max_2mb') ?></small>
                </h2>
            </div>
            <div class="body text-center">
                <div class="col-sm-12 col-md-12">
                    <div class="thumbnail">
                        <?php if($u->url_photo!=null) { ?>
                            <img src="<?= base_url('dist/pp_users/'.$u->url_photo) ?>" width="200px" height="200px">
                        <?php } else{ 
                                if ($u->gender=="1") { ?>
                                    <img src="<?= base_url('dist/images/default.png') ?>" width="200px" height="200px">
                            <?php } else { ?>
                                    <img src="<?= base_url('dist/images/default.png') ?>" width="200px" height="200px">
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <form method="post" enctype="multipart/form-data" id="pp_frm">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input class="form-control" type="file" name="url_photo" required>
                            </div>
                        </div>
                    </div>

                    <?php if($u->url_photo!=null) { ?>
                        <input name="old_url_photo" type="hidden" value="<?= $u->url_photo ?>" multiple />
                    <?php } ?>
                    <div class="text-right">
                        <button type="submit" style="margin-top: 30px" data-color="light-blue" class="btn bg-blue waves-effect"><?= lang('save') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <!-- Task Info -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <h2><?= lang("data_akun") ?></h2>
            </div>
            <div class="body">
                <form method="post">
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="hidden" name="id_user" value="<?= $u->id_user ?>">
                                    <input type="text" value="<?= $u->full_name ?>" class="form-control" name="full_name" placeholder="<?= lang('ph_nama_lengkap') ?>" required>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">picture_in_picture</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" value="<?= $u->id_personal ?>" class="form-control" name="id_personal" placeholder="<?= lang('ph_nim_nidn') ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">date_range</i>
                                </span>
                                <div class="form-line">
                                    <input type="date" value="<?= $u->birth ?>" class="form-control" name="birth" placeholder="<?= lang('ph_birth') ?>" required>
                                </div>
                            </div>
                        </div>
                    <?php 
                        $data['data'] = array(
                            "id_fakultas" => $u->id_fakultas,
                            "id_jurusan" => $u->id_jurusan,
                            "id_prodi" => $u->id_prodi
                        );
                        $this->load->view("partials/select/program_studi_with_pre",$data);
                    ?>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">home</i>
                                </span>
                                <div class="form-line">
                                    <textarea name="address" rows="4" class="form-control no-resize" placeholder="<?= lang('ph_addr') ?>"><?= $u->address ?></textarea>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" value="<?= $u->phone_number ?>" class="form-control" name="phone_number" placeholder="<?= lang('ph_phone') ?>" required>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">group</i>
                                </span>
                                <div class="form-line">
                                    <select class="form-control show-tick" name="gender" required>
                                        <option disabled value="" <?= !$u->gender ? "selected":"" ?>><?= lang('ph_gender') ?></option>
                                        <option value="1" <?= $u->gender=="1"?"selected":"" ?> ><?= lang('ph_pria') ?></option>
                                        <option value="2" <?= $u->gender=="2"?"selected":"" ?> ><?= lang('ph_wanita') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">memory</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" value="<?= $u->bidang_review ?>" class="form-control" name="bidang_review" placeholder="<?= lang('bidang_review') ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-line" style="font-weight: bold">
                                Status Anda saat ini adalah sebagai <i>"<?php position($u->position) ?>"</i>. Jika ingin melakukan pergantian Status, silahkan hubungi admin melalui email tpp@um.ac.id atau datang langsung ke kantor kami di Graha Rektorat UM Lantai 5.
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" style="margin-top: 30px" data-color="light-blue" class="btn bg-blue waves-effect"><?= lang('save') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <?= lang("ubah_kata_sandi") ?>
                </h2>
                
            </div>
            <div class="body">
                <form id="change_password" method="post">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password_lama" placeholder="<?= lang('ph_password_lama') ?>" required>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input id="password" type="password" class="form-control" name="password" placeholder="<?= lang('ph_password') ?>" onchange="validatePassword()" required>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input id="repassword" type="password" class="form-control" name="repassword" placeholder="<?= lang('ph_konfirmasi') ?>" onchange="validatePassword()" required>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" style="margin-top: 30px" data-color="light-blue" class="btn bg-blue waves-effect"><?= lang('save') ?></button>
                        </div>
                    </div>
                    <div class="input-group"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <?= lang("ubah_email") ?>
                </h2>
                
            </div>
            <div class="body">
                <form id="change_password" method="post">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" value="<?= $u->email ?>" class="form-control" name="email" placeholder="<?= lang('ph_email') ?>" required>
                        </div><br>
                        <span style="color:red; font-size: 12px">Email harus mengandung @um.ac.id atau @students.um.ac.id</span>
                    </div>
                    <div class="text-right">
                        <button type="submit" style="margin-top: 30px" data-color="light-blue" class="btn bg-blue waves-effect"><?= lang('save') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!--  -->
<script src="<?= base_url('dist') ?>/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?= base_url('dist/plugins/dropzone/dropzone.js') ?>"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="<?= base_url('dist/app/api.js') ?>"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#account").addClass("active");
    $().ready(function(){
        $('#change_password').validate({
            rules: {
                password: {
                    minlength: 8
                },
                repassword: {
                    minlength: 8,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    minlength: "<?= lang('pass_char') ?>"
                },
                repassword: {
                    minlength: "<?= lang('pass_char') ?>",
                    equalTo: "<?= lang('pass_notmatch') ?>"
                }
            }
        });
        $("#pp_frm" ).validate({
            rules: {
                url_photo: {
                    required: true,
                    extension: "png|jpg|jpeg"
                }
            },
            messages: {
                url_photo: {
                    extension: "Format file harus PNG / JPG / JPEG !"
                }
            }
        });
    });
    /* Start Prodi Event*/
    $("#fakultas").change(function(){
        var id_fakultas = $("#fakultas").val();
        var ApiParam = {
            "id_fakultas":id_fakultas
        };
        var datajur = CallApi('prodi/get_jurusan', ApiParam);

        $("#jurusan").html(
            '<option disabled="" selected="">-- <?= lang("jurusan") ?> --</option>'
        );
        var jurusan = $("#jurusan").html();

        $.each(datajur, function (index,item) {
            jurusan += '<option value="' + item.id_jurusan + '">' + item.jurusan + '</option>';
        });

        $("#jurusan").html(jurusan);
        
        $("#prodi").html(
            '<option disabled="" selected="">-- <?= lang("prodi") ?> --</option>'
        );
        var prodi = $("#prodi").html();
        $('.selectpicker').selectpicker('refresh');
    });
    $("#jurusan").change(function(){
        var id_jurusan = $("#jurusan").val();
        var ApiParam = {
            "id_jurusan":id_jurusan
        };
        $("#prodi").html(
            '<option disabled="" selected="">-- <?= lang("prodi") ?> --</option>'
        );
        var prodi = $("#prodi").html();

        var dataprod = CallApi('prodi/get_prodi', ApiParam);

        $.each(dataprod, function (index,item) {
            prodi += '<option value="' + item.id_prodi + '">' + item.program_studi + '</option>';
        });
        
        $("#prodi").html(prodi);
        $('.selectpicker').selectpicker('refresh');
    });
    /* End Prodi Event*/
</script>

<?php endsection(); ?>

<?php getview('layouts/template') ?>