<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('list') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('topic') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                </div>
                <div class="body">
                    <!-- Modals create -->
                    <div class="modal fade" id="insert" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <form id="form_validation" method="POST" action="<?= base_url('master/topic/create') ?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel"><?= lang('tambah_baru') ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nm_topic" required>
                                                <label class="form-label"><?= lang('nm_topic') ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End modals -->

                    <!-- Modals Update  -->
                    <div class="modal fade" id="update" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <form id="form_validation" method="POST" action="<?= base_url('master/topic/update') ?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel"><?= lang('ubah_data') ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="hidden" class="form-control" name="id_topic" id="id_topic" required>
                                                <input type="text" class="form-control" placeholder="<?= lang('nm_topic') ?>" name="nm_topic" id="nm_topic" required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Modals -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('nm_topic') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($topic as $div){ ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td><?= ucwords($div['nm_topic']) ?></td>
                                        <td align="right">
                                            <button type="button" data-color="red" class="btn bg-red waves-effect" onclick="del(<?= $div['id_topic'] ?>)"><i class="material-icons" title="<?= lang('title_del') ?>">delete</i></button>
                                            <button type="button" data-color="blue" class="btn bg-blue waves-effect" data-toggle="modal" data-target="#update" onclick="edit('<?= $div['id_topic'] ?>', '<?= $div['nm_topic'] ?>')"><i class="material-icons" title="<?= lang('title_del') ?>" onclick="edit('<?= $div['id_topic'] ?>', '<?= $div['nm_topic'] ?>')">edit</i></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>                                   
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('nm_topic') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot> 
                        </table>
                    </div>
                    <button style="margin-top: 30px" type="button" data-color="light-blue" data-toggle="modal" data-target="#insert" class="btn bg-blue waves-effect"><?= lang('tambah_baru') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#master_topic").addClass("active");
    function del(id){
        swal({
        title: "Apakah anda yakin?",
        text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('master/topic/delete') ?>/"+id;
            swal("Terhapus!", "Data anda berhasil dihapus.", "success");
        });
    }

    function edit(id_topic, nm_topic){
        $('#id_topic').val(id_topic);
        $('#nm_topic').val(nm_topic);
        $('#id_topic').prop('readonly', true);
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>