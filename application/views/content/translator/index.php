<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><?= lang('list') ?></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('list') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(5)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="tbl_job">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('hasil_translasi') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>  
                                    <tbody>
                                        <?php $no=1; foreach($get_not as $not){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($not['nm_topic']) ?></td>
                                                <td class="text-center"><?= ucwords($not['tittle']) ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('dist/submissions/'.$not['file']) ?>" class="btn bg-orange waves-effect">
                                                        <i class="material-icons">get_app</i>
                                                        <?= lang('unduh_fp') ?>
                                                    </a><br><br>
                                                    <?php 
                                                        $res = $this->db->where('id_submission',$not['id_submission'])
                                                                        ->where('review_as',2)
                                                                        ->where('active',1)
                                                                        ->get('review')
                                                                        ->row_array();
                                                        if($res){
                                                    ?>
                                                        <a href="<?= base_url('dist/review/'.$res['review_file']) ?>" class="btn bg-green waves-effect">
                                                            <i class="material-icons">get_app</i>
                                                            <?= lang('unduh_hr') ?>
                                                        </a>
                                                    <?php } else{ ?>
                                                        <a class="btn bg-red waves-effect align-right">
                                                            <i class="material-icons">get_app</i>
                                                            <?= lang('unduh_hr')."<br>".lang('tidak_tersedia') ?>
                                                        </a><br><br>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php if($not['translation_file']!=""){ ?>
                                                        <a href="<?= base_url('dist/translation/'.$not['translation_file']) ?>" class="btn bg-cyan waves-effect">
                                                            <i class="material-icons">get_app</i>
                                                            <?= lang('unduh_ht') ?>
                                                        </a>
                                                    <?php }else{ ?>
                                                        <a class="btn btn-danger">
                                                            <?= lang('belum_tersedia') ?>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                                <td align="right">
                                                    <a class="btn btn-success waves-effect" data-toggle="modal" 
                                                    data-target="#job" data-row="<?= $no-2 ?>" data-file='<?= $not['translation_file'] ?>'
                                                    data-id='<?= $not['id_submission'] ?>'>
                                                        <?= lang('kerjakann') ?>
                                                    </a><br><br>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('hasil_translasi') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="sudah">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($get_finish as $ready){  ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($ready['nm_topic']) ?></td>
                                                <td class="text-center"><?= ucwords($ready['tittle']) ?></td>
                                                <td class="text-center"><?= reviewer($ready['status_translation']) ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('dist/translation/'.$ready['translation_file']) ?>" class="btn bg-cyan waves-effect">
                                                        <i class="material-icons">get_app</i>
                                                        <?= lang('unduh_ht') ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modals create -->
<div class="modal fade" id="job" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form enctype="multipart/form-data" id="job_frm" method="POST" action="<?= base_url('translator/job/save_translate') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">
                        <?= lang('tambah_baru') ?>
                        <div class="pull-right">
                            <button class="btn btn-link" data-dismiss="modal">X</button>
                        </div>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-10 col-md-offset-1 col-xs-12">
                        <br>
                        <div class="form-group form-float">
                            <label><?= lang('judul') ?></label>
                            <div class="form-line" id="judul"></div>
                        </div>
                        <div class="form-group form-float">
                            <label><?= lang('upload_translate') ?></label>
                            <div class="form-line">
                                <input type="hidden" name="id_submission" id="id_submission">
                                <input type="file" class="form-control" name="translation_file" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button name="submit" value="0" type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                    <button name="submit" value="1" type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan_dan_submit')) ?></button>
                    <span id="hanya_submit">
                        
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End modals -->
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#job_translator").addClass("active");
    $('#job').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var row = button.data('row');
        var id = button.data('id');
        var file = button.data('file');

        var modal = $(this)

        var table = $('#tbl_job').DataTable();

        var data = table
            .rows(row)
            .data();

        modal.find('.modal-body #id_submission').val(id)
        modal.find('.modal-body #judul').html(data[0][2])
        if(file != ""){
            modal.find('.modal-footer #hanya_submit').html('<button name="submit" value="2" id="hanya_submit" type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('hanya_submit')) ?></button>')
        }else{
            modal.find('.modal-footer #hanya_submit').html('')
        }
    })
    $("#job_frm" ).validate({
        rules: {
            translation_file: {
                required: true,
                extension: "doc|docx|pdf"
            }
        },
        messages: {
            translation_file: {
                extension: "Format file harus Doc / Docx / PDF !"
            }
        }
    });
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>