<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link href="<?= base_url('dist') ?>/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/dropzone/dropzone.css" rel="stylesheet">

<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<?php $post = $this->session->flashdata('post') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('paper/submission') ?>"><?= lang('list') ?></a></li>
            <li><?= lang('tambah_artikel') ?></li>
        </ol>
    </div>
</div>



<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('category') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(4)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<?php $this->load->view('partials/form_validation') ?>
            <div class="card">
                <div class="header">
                    <h2><?= lang('list') ?></h2>
                    <small><?= lang('silahkan_isi_form_pengajuan') ?></small>
                </div>
                <div class="body">
                	<form enctype="multipart/form-data" method="POST" action="<?= base_url('paper/submission/save') ?>" id="create_frm">
	                	<div class="row clearfix">
                            <div class="col-sm-12">
                            	<h2 class="card-inside-title"><?= lang('judul') ?></h2>
                                <div class="form-group">
                                    <div class="form-line">
                                    	<input type="text" name="tittle" class="form-control" placeholder="<?= lang('masukkan_judul') ?>" value="<?= isset($post['tittle'])?$post['tittle']:"" ?>" required />
                                    </div>
                                </div>
                            </div>
                            
                            <?php 
                                if(isset($post['id_topic'])){
                                    $this->load->view('partials/select/topic',array(
                                        "topic" => $post['id_topic']
                                    )); 
                                } else{
                                    $this->load->view('partials/select/topic', array(
                                        "topic" => ""
                                    )); 
                                }
                            ?>

                            <div class="col-sm-12">
	                        	<h2 class="card-inside-title"><?= lang('kata_kunci') ?></h2>
								<div class="form-group demo-tagsinput-area">
		                            <div class="form-line">
		                                <input type="text" name="keyword" class="form-control" data-role="tagsinput" value="<?= isset($post['keyword']) ? $post["keyword"]:'this,is,my,keyword' ?>" required>
		                            </div>
		                            <span style="color: red; font-size: 10px;  "><i><?= lang('pisahkan_dengan_enter') ?></i></span>
		                        </div>
	                        </div>

                            <div class="col-sm-12">
                            	<h2 class="card-inside-title"><?= lang('abstrak') ?></h2>
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea id="ckeditor" name="abstract" placeholder="<?= lang('abstrak') ?>" required><?= isset($post['abstract'])?$post["abstract"]:lang('isi_abstrak') ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <h2 class="card-inside-title"><?= lang('upload_karyamu') ?> </h2>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" name="file" class="form-control" required />
                                    </div>
                                    <span style="color:red; font-size: 10px"><i><?= lang("maks_10mb") ?></i></span>
                                </div>
                            </div>
                        </div>
                    	<button type="submit" data-color="light-blue" class="btn bg-blue waves-effect"><?= lang('save') ?></button>
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<script src="<?= base_url('dist') ?>/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?= base_url('dist') ?>/plugins/ckeditor/ckeditor.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/forms/editors.js"></script> 
<script src="<?= base_url('dist') ?>/plugins/dropzone/dropzone.js"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#upload_paper").addClass("active");
    $("#create_frm" ).validate({
        rules: {
            file: {
                required: true,
                extension: "doc|docx|pdf"
            }
        },
        messages: {
            file: {
                extension: "Format file harus Doc / Docx / PDF !"
            }
        }
    });
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>