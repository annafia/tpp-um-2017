<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>

<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('paper/submission') ?>"><?= lang('list') ?></a></li>
            <li><?= lang('detail_karya') ?></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('detail_karya') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(4)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <?php if($submission['verified']==0){ ?>
        <div class="alert bg-red alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= lang("unverified") ?>
        </div>
    <?php } ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('detail_karya') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#home_dt" data-toggle="tab">
                                        <?= lang('home') ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#profile_animation_1" data-toggle="tab">
                                        <?= lang("member_pembimbing") ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#messages_animation_1" data-toggle="tab">
                                        <?= lang("hasil_plagiarism") ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#settings_animation_1" data-toggle="tab">
                                        <?= lang("hasil_review") ?>
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane animated flipInX active" id="home_dt">
                                    <div class="row">
                                        <div class="col-sm-4 col-md-4 col-xs-12">
                                            <div class="thumbnail">
                                                <img src="<?= base_url('dist/images/doc.png') ?>">
                                                <div class="caption">
                                                    <a href="<?= base_url('dist/submissions/').'/'.$submission['file'] ?>" class="btn btn-block bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                    <?php if($trans){ ?>
                                                        <br><br>
                                                        <a href="<?= base_url('dist/translation/'.$trans['translation_file']) ?>" class="btn btn-block bg-purple waves-effect"><?= lang('unduh_ht') ?></a>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-8">
                                            <div class="col-sm-12">
                                                <div class="form-group form-float">
                                                    <label><?= lang('judul') ?></label>
                                                    <div class="form-line">
                                                        <?= $submission['tittle'] ?>    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-float">
                                                    <label><?= lang('abstrak') ?></label>
                                                    <div class="form-line">
                                                        <?= $submission['abstract'] ?>    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-float">
                                                    <label><?= lang('topik') ?></label>
                                                    <div class="form-line">
                                                        <?= $submission['nm_topic'] ?>    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-float">
                                                    <label><?= lang('kata_kunci') ?></label>
                                                    <div class="form-line">
                                                        <?= $submission['keyword'] ?>    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <label><?= lang('pengecekan_plagiasi') ?></label>
                                                <div class="form-line">
                                                    <?= plagiarism($submission['status_plagiarism']) ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <label><?= lang('pengecekan_revisi') ?></label>
                                                <div class="form-line">
                                                    <?= reviewer($submission['status_reviewer']) ?>    
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <label><?= lang('status_akhir') ?></label>
                                                <div class="form-line">
                                                    <?= status_paper($submission['status']) ?>    
                                                </div>
                                            </div>
                                            <div  class="col-md-12">
                                                <?php if($submission['verified']!=2 && !$plagiarism && $submission['status_plagiarism']==0){ ?>
                                                    <a href="<?= base_url("paper/submission/update/".$submission['id_submission']) ?>" class="btn btn-block btn-primary btn-lg">
                                                        <i class="material-icons">edit</i>
                                                        <?= strtoupper(lang('edit_article')) ?>
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <a class="btn btn-block btn-danger btn-lg" onclick="del_article(<?= $submission['id_submission'] ?>)">
                                                        <i class="material-icons">delete_sweep</i>
                                                        <?= strtoupper(lang('hapus_article')) ?>
                                                        <i class="material-icons">delete_sweep</i>
                                                    </a>
                                                <?php } ?>

                                                <?php if($submission['status_plagiarism']==2){ ?>
                                                    <a href="<?= base_url("paper/submission/revision_plagiarism/".$submission['id_submission']) ?>" data-color="amber" class="btn btn-block btn-warning btn-lg"><?= lang('revisi_sekarang') ?></a>
                                                <?php } else if($submission['status_reviewer']==2){ ?>
                                                    <a href="<?= base_url("paper/submission/revision_reviewer/".$submission['id_submission']) ?>" class="btn btn-block btn-warning btn-lg"><?= lang('revisi_sekarang') ?></a>
                                                <?php } else if($submission['verified']==1){ ?>
                                                    <a onclick="send_to_admin()" class="btn btn-block bg-green btn-lg"><?= strtoupper(lang('kirim_ke_admin')) ?></a>
                                                    <br>
                                                    <h4><?= lang('catatan') ?></h4>
                                                    <ol type="1">
                                                        <li><?= lang('catatan_all3') ?></li>
                                                    </ol>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane animated flipInX" id="profile_animation_1">
                                    <div class="header">
                                        <h2>
                                            <?= lang('daftar_member_pembimbing') ?>
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="tbl_member">
                                                <thead>
                                                    <tr>
                                                        <th width="3%" class="text-center"><?= lang('no') ?></th>
                                                        <th width="15%" class="text-center"><?= lang('member_name') ?></th>
                                                        <th width="15%" class="text-center"><?= lang('member_affiliation') ?></th>
                                                        <th width="15%" class="text-center"><?= lang('member_email') ?></th>
                                                        <th width="10%" class="text-center"><?= lang('member_status') ?></th>
                                                        <th width="10%" class="text-center"><?= lang('aksi') ?></th>
                                                    </tr>
                                                </thead>   
                                                <tbody>
                                                    <?php 
                                                        $i=0; $pembimbing=0; foreach ($member as $m) { 
                                                            if($m['member_status']==2){ $pembimbing=1; }
                                                    ?>
                                                    <tr class="align-center">
                                                        <td><?= ++$i ?></td>
                                                        <td><?= $m['member_name'] ?></td>
                                                        <td><?= $m['member_affiliation'] ?></td>
                                                        <td><?= $m['member_email'] ?></td>
                                                        <td><?php status_member($m['member_status']) ?></td>
                                                        <td>
                                                            <?php if($submission['verified']=="0"){ ?>
                                                            <a class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit" data-row="<?= $i-1 ?>" data-id="<?= $m['id_member'] ?>" data-status="<?= $m['member_status'] ?>">
                                                                <i class="material-icons">edit</i>
                                                            </a>
                                                            <a class="btn btn-danger btn-xs" onclick="del(<?= $m['id_member'] ?>)">
                                                                <i class="material-icons">delete_sweep</i>
                                                            </a>
                                                            <?php }else{ ?>
                                                                Tidak Tersedia
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center"><?= lang('no') ?></th>
                                                        <th class="text-center"><?= lang('member_name') ?></th>
                                                        <th class="text-center"><?= lang('member_affiliation') ?></th>
                                                        <th class="text-center"><?= lang('member_email') ?></th>
                                                        <th class="text-center"><?= lang('member_status') ?></th>
                                                        <th class="text-center"><?= lang('aksi') ?></th>
                                                    </tr>
                                                </tfoot> 
                                            </table>
                                        </div><br>
                                        <div class="row">
                                            <div class="pull-right">
                                                <?php if($submission['verified']=="0"){ ?>
                                                <a class="btn bg-blue waves-effect" data-toggle="modal" data-target="#create">
                                                    <?= lang('tambah_baru') ?>
                                                </a>
                                                <?php } if($pembimbing==1 && $submission['verified']==0){ ?>
                                                    <a class="btn bg-red waves-effect" onclick="send()">
                                                        <?= lang('kirim_kode_verifikasi') ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <br>
                                        <?php if($this->session->userdata("position")==1){ ?>
                                            <h4><?= lang('catatan') ?></h4>
                                            <ol type="1">
                                                <li><?= lang('catatan_mhs1') ?></li>
                                                <li><?= lang('catatan_mhs2') ?></li>
                                            </ol>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane animated flipInX" id="messages_animation_1">
                                    <div class="header">
                                        <h2>
                                            <?= lang('hasil_plagiarism') ?>
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('no') ?></th>
                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                        <th class="text-center"><?= lang('komentar') ?></th>
                                                        <th class="text-center"><?= lang('persentase_plag') ?></th>
                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center"><?= lang('no') ?></th>
                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                        <th class="text-center"><?= lang('komentar') ?></th>
                                                        <th class="text-center"><?= lang('persentase_plag') ?></th>
                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                    </tr>
                                                </tfoot>  
                                                <tbody>
                                                    <?php $no=1; foreach($plagiarism as $row){ ?>
                                                        <tr>
                                                            <td class="text-center"><?= $no++ ?></td>
                                                            <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                            <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                            <td class="text-center"><?= $row['comment'] ?></td>
                                                            <td class="text-center"><?= $row['persen_plag'] ?></td>
                                                            <td align="right">
                                                                <a href="<?= base_url('dist/plagiarism/').'/'.$row['plagiarism_file'] ?>" class="btn bg-blue waves-effect"><?= lang('unduh_hp') ?></a>
                                                                <br><br>
                                                                <a href="<?= base_url('dist/submissions/').'/'.$row['fp_file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_1">
                                    <div class="header">
                                        <h2>
                                            <?= lang('hasil_review') ?>
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="tbl_review">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('no') ?></th>
                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                        <th class="text-center"><?= lang('komentarUmum') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('abstrak') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('pengantar') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('metodologi') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('hasil') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('diskusi') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('refrensi') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('lainnya') ?></th>
                                                        <th class="text-center"><?= lang('keaslian') ?></th>
                                                        <th class="text-center"><?= lang('kontribusi') ?></th>
                                                        <th class="text-center"><?= lang('kualitas') ?></th>
                                                        <th class="text-center"><?= lang('kedalaman') ?></th>
                                                        <th class="text-center"><?= lang('keterbaruan') ?></th>
                                                        <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                                        <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                        <th class="text-center">
                                                            <?= lang('aksi') ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center"><?= lang('no') ?></th>
                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                        <th class="text-center"><?= lang('komentarUmum') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('abstrak') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('pengantar') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('metodologi') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('hasil') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('diskusi') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('refrensi') ?></th>
                                                        <th class="text-center"><?= lang('komentar').lang('lainnya') ?></th>
                                                        <th class="text-center"><?= lang('keaslian') ?></th>
                                                        <th class="text-center"><?= lang('kontribusi') ?></th>
                                                        <th class="text-center"><?= lang('kualitas') ?></th>
                                                        <th class="text-center"><?= lang('kedalaman') ?></th>
                                                        <th class="text-center"><?= lang('keterbaruan') ?></th>
                                                        <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                                        <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                        <th class="text-center">
                                                            <?= lang('aksi') ?>
                                                        </th>
                                                    </tr>
                                                </tfoot>  
                                                <tbody>
                                                    <?php $no=1; foreach($hst_review as $row){ ?>
                                                    <?php if($row['review_file']!=""){ ?>
                                                        <tr>
                                                            <td class="text-center"><?= $no++ ?></td>
                                                            <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                            <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                            <td class="text-center"><?= $row['general_comment'] ?></td>
                                                            <td class="text-center"><?= $row['abstract_comment'] ?></td>
                                                            <td class="text-center"><?= $row['intro_comment'] ?></td>
                                                            <td class="text-center"><?= $row['methodology_comment'] ?></td>
                                                            <td class="text-center"><?= $row['result_comment'] ?></td>
                                                            <td class="text-center"><?= $row['discussion_comment'] ?></td>
                                                            <td class="text-center"><?= $row['reference'] ?></td>
                                                            <td class="text-center"><?= $row['other_comment'] ?></td>
                                                            <td class="text-center"><?= measurement($row['originality']) ?></td>
                                                            <td class="text-center"><?= measurement($row['contribution_in_science']) ?></td>
                                                            <td class="text-center"><?= measurement($row['writing_technique']) ?></td>
                                                            <td class="text-center"><?= measurement($row['depth_of_research']) ?></td>
                                                            <td class="text-center"><?= measurement($row['novelty_reference']) ?></td>
                                                            <td class="text-center">
                                                                <?= date('d M Y H:i:s',strtotime($row['create_at'])) ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?= date('d M Y H:i:s',strtotime($row['finish_at'])) ?>
                                                            </td>
                                                            <td align="right">
                                                                <a href="<?= base_url('dist/review/').'/'.$row['review_file'] ?>" class="btn bg-green waves-effect"><?= lang('unduh_hr') ?></a>
                                                                <br><br>
                                                                <a href="<?= base_url('dist/submissions/').'/'.$row['fp_file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                            </td>
                                                            <td align="right">
                                                                <a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#detail_job" data-row="<?= $no-2 ?>">
                                                                    <?= lang('lihat_detail') ?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modals create -->
<div class="modal fade" id="create" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="add_member_frm" method="POST" action="<?= base_url('paper/submission/add_member') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel"><?= lang('tambah_member_pembimbing') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="form-group form-float">
                                <label><?= lang('member_name') ?></label>
                                <div class="form-line">
                                    <input type="hidden" name="id_submission" value="<?= $submission['id_submission'] ?>">
                                    <input type="text" class="form-control" name="member_name" required placeholder="Contoh: Annafia Oktafian">
                                </div>
                            </div>
                            <?php
                                $member_status = $this->db->distinct('member_status')->where('member_status != 1')->where('id_submission',$submission['id_submission'])->get('member')->result_array();
                                $pembimbing = array(false,false,false);
                                foreach ($member_status as $ms) {
                                    $pembimbing[$ms['member_status']-2] = true;
                                }
                            ?>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label><?= lang('member_status') ?></label>
                                    <select class="form-control" name="member_status" id="add_member_status" required>
                                        <option disabled selected><?=  lang('pilih_member_status') ?></option>
                                        <option value="1"><?php status_member(1) ?></option>
                                        <?php for($i = 0;$i<3;$i++){ if(!$pembimbing[$i]){ ?>
                                            <option value="<?= $i+2 ?>"><?php status_member($i+2) ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label><?= lang('member_affiliation') ?></label>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="member_affiliation" required placeholder="Contoh: Universitas Negeri Malang">
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label><?= lang('member_email') ?></label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="email" class="form-control" name="member_email" required placeholder="Contoh: annafia.ft@um.ac.id">
                                    </div><br>
                                    <div class="pull-right">
                                        <span style="font-size: 12px;color: red; font-weight: bold;">
                                            Email Pembimbing 1 harus @um.ac.id !
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End modals -->
<!-- Modals edit -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="edit_member_frm" method="POST" action="<?= base_url('paper/submission/edit_member') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel"><?= lang('edit_member_pembimbing') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="form-group form-float">
                                <label><?= lang('member_name') ?></label>
                                <div class="form-line">
                                    <input type="hidden" name="id_member" id="id_member">
                                    <input type="hidden" name="id_submission" value="<?= $submission['id_submission'] ?>">
                                    <input type="text" class="form-control" name="member_name" id="member_name" required>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label><?= lang('member_status') ?></label>
                                    <select class="form-control" name="member_status" id="member_status" required>
                                        <option value="1"><?php status_member(1) ?></option>
                                        <option value="2"><?php status_member(2) ?></option>
                                        <option value="3"><?php status_member(3) ?></option>
                                        <option value="4"><?php status_member(4) ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label><?= lang('member_affiliation') ?></label>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="member_affiliation" id="member_affiliation" required>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label><?= lang('member_email') ?></label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="email" class="form-control" name="member_email" id="member_email" required>
                                    </div><br>
                                    <div class="pull-right">
                                        <span style="font-size: 12px;color: red; font-weight: bold;">
                                            Email Pembimbing 1 harus @um.ac.id !
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End modals -->
<!-- Modals Detail -->
<div class="modal fade" id="detail_job" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">
                    <?= lang('riwayat_review') ?>
                    <div class="pull-right">
                        <button class="btn btn-link waves-effect" data-dismiss="modal">
                            X
                        </button>
                    </div>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('reviewer') ?></label>
                                <div class="form-line" id="reviewer"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentarUmum') ?></label>
                                <div class="form-line" id="general_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('abstract') ?></label>
                                <div class="form-line" id="abstract_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('pengantar') ?></label>
                                <div class="form-line" id="intro_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('methodology') ?></label>
                                <div class="form-line" id="methodology_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('hasil') ?></label>
                                <div class="form-line" id="result_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('diskusi') ?></label>
                                <div class="form-line" id="discussion_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('refrensi') ?></label>
                                <div class="form-line" id="reference"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('lainnya') ?></label>
                                <div class="form-line" id="other_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('keaslian') ?></label>
                                <div class="form-line" id="originality"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('kontribusi') ?></label>
                                <div class="form-line" id="contribution_in_science"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('kualitas') ?></label>
                                <div class="form-line" id="writing_technique">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('kedalaman') ?></label>
                                <div class="form-line" id="depth_of_research"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('keterbaruan') ?></label>
                                <div class="form-line" id="novelty_reference"></div>
                            </div>
                        </div><br><br>
                        <div class="col-sm-12">
                            <div class="form-group form-float"><br>
                                <label><?= lang('ditugaskan_sejak') ?></label>
                                <div id="create_at"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float"><br>
                                <label><?= lang('diselesaikan_pada') ?></label>
                                <div id="finish_at"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
            </div>
        </div>
    </div>
</div>
<!-- End modals -->
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#upload_paper").addClass("active");
    $(".block-header").focus();
    function del(id){
        swal({
            title: "Apakah anda yakin?",
            text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, saya yakin !",
            closeOnConfirm: false
        },
        function(){
            window.location.href = "<?= base_url('paper/submission/del_member/'.$submission['id_submission']) ?>/"+id;
        });
    }
    function del_article(id){
        swal({
            title: "Apakah anda yakin?",
            text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, saya yakin !",
            closeOnConfirm: false
        },
        function(){
            window.location.href = "<?= base_url('paper/submission/del/'.$submission['id_submission']) ?>/";
        });
    }
    function send(){
        swal({
            title: "Apakah anda yakin?",
            text: "Mengirim kode verifikasi ke Pembimbing 1 !",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, saya yakin !",
            closeOnConfirm: false
        },
        function(){
            window.location.href = "<?= base_url('paper/submission/send_verification/'.$submission['id_submission']) ?>";
        });
    }
    function send_to_admin(){
        swal({
            title: "Apakah anda yakin?",
            text: "Mengirim artikel ke Admin untuk Diproses !",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, saya yakin !",
            closeOnConfirm: false
        },
        function(){
            window.location.href = "<?= base_url('paper/submission/send_to_admin/'.$submission['id_submission']) ?>";
        });
    }
    $('#add_member_status').change(function(){
        var val = $('#add_member_status').val();
        if(val!="2"){
            var validator = $('#add_member_frm').validate();
            validator.destroy();
            $('#add_member_frm').validate({
                rules:{
                    member_email: {
                        required: true,
                        email: true
                    }
                }
            });
        }else{
            var validator = $('#add_member_frm').validate();
            validator.destroy();
            jQuery.validator.addMethod("EmailUM", function(value, element) {
                var re = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
                if (re.test(value)) {
                    if (value.indexOf("@um.ac.id", value.length - "@um.ac.id".length) !== -1) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }, "Email Pembimbing 1 harus @um.ac.id !");
            $('#add_member_frm').validate({
                rules:{
                    member_email: {
                        required: true,
                        email: true,
                        EmailUM: true
                    }
                }
            });
        }
    });
    function edit_member_validation(){
        var val = $('#member_status').val();
        if(val!="2"){
            var validator = $('#edit_member_frm').validate();
            validator.destroy();
            $('#edit_member_frm').validate({
                rules:{
                    member_email: {
                        required: true,
                        email: true
                    }
                }
            });
        }else{
            var validator = $('#edit_member_frm').validate();
            validator.destroy();
            jQuery.validator.addMethod("EmailUM", function(value, element) {
                var re = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
                if (re.test(value)) {
                    if (value.indexOf("@um.ac.id", value.length - "@um.ac.id".length) !== -1) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }, "Email Pembimbing 1 harus @um.ac.id !");
            $('#edit_member_frm').validate({
                rules:{
                    member_email: {
                        required: true,
                        email: true,
                        EmailUM: true
                    }
                }
            });
        }
    }
    $('#member_status').change(function(){
        edit_member_validation();
    });
    $('#edit').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var row = button.data('row')
        var id = button.data('id')
        var status = button.data('status')

        var modal = $(this)

        var table = $('#tbl_member').DataTable();

        var data = table
        .rows(row)
        .data();

        modal.find('.modal-body #id_member').val(id)
        modal.find('.modal-body #member_name').val(data[0][1])
        modal.find('.modal-body #member_affiliation').val(data[0][2])
        modal.find('.modal-body #member_email').val(data[0][3])

        <?php for($i = 0; $i < 3; $i++){ if($pembimbing[$i]){ ?>
            if(status != <?= $i+2 ?>){
                $('#member_status').find('[value=<?= $i+2 ?>]').prop('disabled',true);
            }
        <?php }} ?>
        
        $("#member_status").val(status);
        $("#member_status").selectpicker('render');
        edit_member_validation();
    })
    $(document).ready(function(){
        var table = $('#tbl_review').DataTable();
        table.columns([4,5,6,7,8,9,10,11,12,13,14,15,16,17]).visible(false);
    })
    $('#detail_job').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var row = button.data('row') // Extract info from data-* attributes

        var modal = $(this)

        var table = $('#tbl_review').DataTable();

        var data = table
            .rows(row)
            .data();

        var i = 0;
        for(;i<17;i++){
            if(data[0][i] == ""){
                data[0][i]="Belum Tersedia";
            }
        }

        modal.find('.modal-body #reviewer').html(data[0][1])
        modal.find('.modal-body #general_comment').html(data[0][2])
        modal.find('.modal-body #abstract_comment').html(data[0][3])
        modal.find('.modal-body #intro_comment').html(data[0][4])
        modal.find('.modal-body #methodology_comment').html(data[0][5])
        modal.find('.modal-body #result_comment').html(data[0][6])
        modal.find('.modal-body #discussion_comment').html(data[0][7])
        modal.find('.modal-body #reference').html(data[0][8])
        modal.find('.modal-body #other_comment').html(data[0][9])
        modal.find('.modal-body #originality').html(data[0][10])
        modal.find('.modal-body #contribution_in_science').html(data[0][11])
        modal.find('.modal-body #writing_technique').html(data[0][12])
        modal.find('.modal-body #depth_of_research').html(data[0][13])
        modal.find('.modal-body #novelty_reference').html(data[0][14])
        modal.find('.modal-body #create_at').html(data[0][16])
        modal.find('.modal-body #finish_at').html(data[0][17])
    })
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>