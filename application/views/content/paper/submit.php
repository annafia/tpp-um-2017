<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><?= lang('unggah_bukti_submit') ?></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('category') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(4)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('unggah_bukti_submit') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="table_submit_paper">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('unduh_ht') ?></th>
                                    <th class="text-center"><?= lang('status_submit') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>   
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('unduh_ht') ?></th>
                                    <th class="text-center"><?= lang('status_submit') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $no=1; foreach($submission as $sta){ ?>
                                <?php 
                                    $trans = $this->db->where('id_submission',$sta['id_submission'])
                                                      ->get('translation')
                                                      ->row_array();
                                ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td><?= ucwords($sta['tittle']) ?></td>
                                        <th class="text-center">
                                            <?php if($trans){ ?>
                                                <a class="btn bg-purple" href="<?= base_url('dist/translation/'.$trans['translation_file']) ?>">
                                                    <?= lang('unduh_ht') ?>
                                                </a>
                                            <?php }else{ ?>
                                                <a class="btn btn-danger" disabled>
                                                    <?= lang('tidak_tersedia') ?>
                                                </a>
                                            <?php } ?>
                                        </th>
                                        <td class="text-center"><?= status_paper($sta['status']) ?></td>
                                        <td class="text-right">
                                            <?php if($sta['status']!=3){ ?>
                                                <a data-toggle="modal" data-target="#upload" class="btn btn-primary" data-row="<?= $no-2 ?>" data-id="<?= $sta['id_submission'] ?>">
                                                    <?= lang('unggah_bukti_submit') ?>
                                                </a>
                                            <?php } else{ ?>
                                                <a class="btn btn-primary" disabled>
                                                    <?= lang('unggah_bukti_submit') ?>
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>  
                        </table>
                    </div><br>
                    <span>
                        <span style="font-size: 15px; font-weight: bold"><?= lang('catatan') ?>: </span>
                        <span style="font-size: 15px"><?= lang('ctt_submit') ?></span>
                    <span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Default Size -->
<div class="modal fade" id="upload" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" enctype="multipart/form-data" action="<?= base_url("paper/submission/submit") ?>" id="submit_frm">
                <div class="modal-header">
                    <h4 class="modal-title"><?= lang("unggah_bukti_submit") ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="form-group">
                            <div class="form-line">
                                <label><?= lang("judul") ?></label>
                                <input type="hidden" id="id_submission" name="id_submission">
                                <input type="text" id="title" class="form-control" disabled="">
                            </div>
                        </div>
                        <label class="card-inside-title"><?= lang('bukti_submit') ?> </label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="file" name="submitted_proof" class="form-control" required />
                            </div>
                            <span style="color:red; font-size: 10px"><i><?= lang("maks_2mb") ?></i></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect"><?= lang("save") ?></button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= lang("close") ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#submit_paper").addClass("active");
    $('#upload').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var row = button.data('row');
        var id = button.data('id');

        var modal = $(this)

        var table = $('#table_submit_paper').DataTable();

        var data = table
        .rows(row)
        .data();

        modal.find('.modal-body #id_submission').val(id);
        modal.find('.modal-body #title').val(data[0][1]);
    })
    $("#submit_frm" ).validate({
        rules: {
            submitted_proof: {
                required: true,
                extension: "png|jpg|jpeg|doc|docx|pdf"
            }
        },
        messages: {
            submitted_proof: {
                extension: "Format file harus PNG / JPG / JPEG / Doc / Docx / PDF !"
            }
        }
    });
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>