<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><?= lang('list') ?></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('category') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(4)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                        <div class="pull-right">
                            <a href="<?= base_url('paper/submission/create') ?>" data-color="light-blue" class="btn bg-blue waves-effect"><?= lang('tambah_artikel') ?></a>
                        </div>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('topik') ?></th>
                                    <th class="text-center"><?= lang('kata_kunci') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_plagiasi') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_revisi') ?></th>
                                    <th class="text-center"><?= lang('status_akhir') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>   
                            <tbody>
                                <?php $no=1; foreach($get as $gt){ ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td><?= ucwords($gt['tittle']) ?></td>
                                        <td><?= ucwords($gt['nm_topic']) ?></td>
                                        <td><?= ucwords($gt['keyword']) ?></td>
                                        <td><?= plagiarism($gt['status_plagiarism']) ?></td>
                                        <td><?= reviewer($gt['status_reviewer']) ?></td>
                                        <td><?= status_paper($gt['status']) ?></td>
                                        <td>
                                            <a class="btn btn-info" href="<?= base_url("paper/submission/show_detail/".$gt['id_submission']) ?>"> <?= lang('lihat_detail') ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>                                   
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('topik') ?></th>
                                    <th class="text-center"><?= lang('kata_kunci') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_plagiasi') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_revisi') ?></th>
                                    <th class="text-center"><?= lang('status_akhir') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot> 
                        </table>
                    </div>
                    <br>
                    <h4><?= lang('catatan') ?></h4>
                    <ol type="1">
                        <li><?= lang('catatan_mhs1') ?></li>
                        <li><?= lang('catatan_all1') ?></li>
                        <li><?= lang('catatan_all2') ?></li>
                        <li><?= lang('klik_lihat') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#upload_paper").addClass("active");
    function del(id){
        swal({
        title: "Apakah anda yakin?",
        text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('paper/submission/delete') ?>/"+id;
            swal("Terhapus!", "Data anda berhasil dihapus.", "success");
        });
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>