<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>

<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('reviewer/job') ?>"><?= lang('list') ?></a></li>
            <li><a href="#"><?= lang('detail_pekerjaan') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('detail_karya') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(2)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('detail_karya') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#home_dt" data-toggle="tab">
                                        <?= lang('detail_artikel') ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#admin_review" data-toggle="tab">
                                        <?= lang("review_sekarang") ?>
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane animated flipInX active" id="home_dt">
                                    <div class="col-md-12">
                                        <div class="panel-group" id="dt_artikel" role="tablist">
                                            <div class="panel">
                                                <div class="panel-heading" role="tab" id="hd_dt_artikel">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="dt_artikel" href="#cl_dt_artikel" aria-expanded='false' aria-controls="cl_dt_artikel">
                                                            <?= lang('detail_artikel') ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cl_dt_artikel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hd_dt_artikel">
                                                    <div class="panel-body">
                                                        <div class="col-sm-4 col-md-4 col-xs-12">
                                                            <div class="thumbnail">
                                                                <img src="<?= base_url('dist/images/doc.png') ?>">
                                                                <div class="caption">
                                                                    <a href="<?= base_url('dist/submissions/').'/'.$submission['file'] ?>" class="btn btn-block bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-8">
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-float">
                                                                    <label><?= lang('author') ?></label>
                                                                    <div class="form-line">
                                                                        <?= $submission['full_name'] ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-float">
                                                                    <label><?= lang('judul') ?></label>
                                                                    <div class="form-line">
                                                                        <?= $submission['tittle'] ?>    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-float">
                                                                    <label><?= lang('abstrak') ?></label>
                                                                    <div class="form-line">
                                                                        <?= $submission['abstract'] ?>    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-float">
                                                                    <label><?= lang('topik') ?></label>
                                                                    <div class="form-line">
                                                                        <?= $submission['nm_topic'] ?>    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-float">
                                                                    <label><?= lang('kata_kunci') ?></label>
                                                                    <div class="form-line">
                                                                        <?= $submission['keyword'] ?>    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-12">
                                                                <label><?= lang('pengecekan_plagiasi') ?></label>
                                                                <div class="form-line">
                                                                    <?= plagiarism($submission['status_plagiarism']) ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-12">
                                                                <label><?= lang('pengecekan_revisi') ?></label>
                                                                <div class="form-line">
                                                                    <?= reviewer($submission['status_reviewer']) ?>    
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-12">
                                                                <label><?= lang('status_akhir') ?></label>
                                                                <div class="form-line">
                                                                    <?= status_paper($submission['status']) ?>    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading" role="tab" id="hd_dm_artikel">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="dt_artikel" href="#cl_dm_artikel" aria-expanded='false' aria-controls="cl_dm_artikel">
                                                            <?= lang('daftar_member_pembimbing') ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cl_dm_artikel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hd_dm_artikel">
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="tbl_member">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="3%" class="text-center"><?= lang('no') ?></th>
                                                                        <th width="15%" class="text-center"><?= lang('member_name') ?></th>
                                                                        <th width="15%" class="text-center"><?= lang('member_affiliation') ?></th>
                                                                        <th width="15%" class="text-center"><?= lang('member_email') ?></th>
                                                                        <th width="10%" class="text-center"><?= lang('member_status') ?></th>
                                                                    </tr>
                                                                </thead>   
                                                                <tbody>
                                                                    <?php 
                                                                        $i=0; $pembimbing=0; foreach ($member as $m) { 
                                                                            if($m['member_status']==2){ $pembimbing=1; }
                                                                    ?>
                                                                    <tr class="align-center">
                                                                        <td><?= ++$i ?></td>
                                                                        <td><?= $m['member_name'] ?></td>
                                                                        <td><?= $m['member_affiliation'] ?></td>
                                                                        <td><?= $m['member_email'] ?>@um.ac.id</td>
                                                                        <td><?php status_member($m['member_status']) ?></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th class="text-center"><?= lang('no') ?></th>
                                                                        <th class="text-center"><?= lang('member_name') ?></th>
                                                                        <th class="text-center"><?= lang('member_affiliation') ?></th>
                                                                        <th class="text-center"><?= lang('member_email') ?></th>
                                                                        <th class="text-center"><?= lang('member_status') ?></th>
                                                                    </tr>
                                                                </tfoot> 
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading" role="tab" id="hd_rp_artikel">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="dt_artikel" href="#cl_rp_artikel" aria-expanded='true' aria-controls="cl_rp_artikel">
                                                            <?= lang('riwayat_plagiasi') ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cl_rp_artikel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hd_rp_artikel">
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center"><?= lang('no') ?></th>
                                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                                        <th class="text-center"><?= lang('komentar') ?></th>
                                                                        <th class="text-center"><?= lang('persentase_plag') ?></th>
                                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th class="text-center"><?= lang('no') ?></th>
                                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                                        <th class="text-center"><?= lang('komentar') ?></th>
                                                                        <th class="text-center"><?= lang('persentase_plag') ?></th>
                                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                                    </tr>
                                                                </tfoot>  
                                                                <tbody>
                                                                    <?php $no=1; foreach($plagiarism as $row){ ?>
                                                                        <tr>
                                                                            <td class="text-center"><?= $no++ ?></td>
                                                                            <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                                            <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                                            <td class="text-center"><?= $row['comment'] ?></td>
                                                                            <td class="text-center"><?= $row['persen_plag'] ?></td>
                                                                            <td align="right">
                                                                                <a href="<?= base_url('dist/plagiarism/').'/'.$row['plagiarism_file'] ?>" class="btn bg-blue waves-effect"><?= lang('unduh_hp') ?></a>
                                                                                <br><br>
                                                                                <a href="<?= base_url('dist/submissions/').'/'.$row['fp_file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading" role="tab" id="hd_rr_artikel">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="dt_artikel" href="#cl_rr_artikel" aria-expanded='false' aria-controls="cl_rr_artikel">
                                                            <?= lang('riwayat_review') ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="cl_rr_artikel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hd_rr_artikel">
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="tbl_review">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center"><?= lang('no') ?></th>
                                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                                        <th class="text-center"><?= lang('komentarUmum') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('abstrak') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('pengantar') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('metodologi') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('hasil') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('diskusi') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('refrensi') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('lainnya') ?></th>
                                                                        <th class="text-center"><?= lang('keaslian') ?></th>
                                                                        <th class="text-center"><?= lang('kontribusi') ?></th>
                                                                        <th class="text-center"><?= lang('kualitas') ?></th>
                                                                        <th class="text-center"><?= lang('kedalaman') ?></th>
                                                                        <th class="text-center"><?= lang('keterbaruan') ?></th>
                                                                        <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                                                        <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                                        <th class="text-center">
                                                                            <?= lang('aksi') ?>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th class="text-center"><?= lang('no') ?></th>
                                                                        <th class="text-center"><?= lang('petugas') ?></th>
                                                                        <th class="text-center"><?= lang('judul') ?></th>
                                                                        <th class="text-center"><?= lang('komentarUmum') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('abstrak') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('pengantar') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('metodologi') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('hasil') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('diskusi') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('refrensi') ?></th>
                                                                        <th class="text-center"><?= lang('komentar').lang('lainnya') ?></th>
                                                                        <th class="text-center"><?= lang('keaslian') ?></th>
                                                                        <th class="text-center"><?= lang('kontribusi') ?></th>
                                                                        <th class="text-center"><?= lang('kualitas') ?></th>
                                                                        <th class="text-center"><?= lang('kedalaman') ?></th>
                                                                        <th class="text-center"><?= lang('keterbaruan') ?></th>
                                                                        <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                                                        <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                                                        <th class="text-center"><?= lang('unduh') ?></th>
                                                                        <th class="text-center">
                                                                            <?= lang('aksi') ?>
                                                                        </th>
                                                                    </tr>
                                                                </tfoot>  
                                                                <tbody>
                                                                    <?php $no=1; foreach($hst_review as $row){ ?>
                                                                    <?php if($row['review_file']!=""){ ?>
                                                                        <tr>
                                                                            <td class="text-center"><?= $no++ ?></td>
                                                                            <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                                            <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                                            <td class="text-center"><?= $row['general_comment'] ?></td>
                                                                            <td class="text-center"><?= $row['abstract_comment'] ?></td>
                                                                            <td class="text-center"><?= $row['intro_comment'] ?></td>
                                                                            <td class="text-center"><?= $row['methodology_comment'] ?></td>
                                                                            <td class="text-center"><?= $row['result_comment'] ?></td>
                                                                            <td class="text-center"><?= $row['discussion_comment'] ?></td>
                                                                            <td class="text-center"><?= $row['reference'] ?></td>
                                                                            <td class="text-center"><?= $row['other_comment'] ?></td>
                                                                            <td class="text-center"><?= measurement($row['originality']) ?></td>
                                                                            <td class="text-center"><?= measurement($row['contribution_in_science']) ?></td>
                                                                            <td class="text-center"><?= measurement($row['writing_technique']) ?></td>
                                                                            <td class="text-center"><?= measurement($row['depth_of_research']) ?></td>
                                                                            <td class="text-center"><?= measurement($row['novelty_reference']) ?></td>
                                                                            <td class="text-center">
                                                                                <?= date('d M Y H:i:s',strtotime($row['create_at'])) ?>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <?= date('d M Y H:i:s',strtotime($row['finish_at'])) ?>
                                                                            </td>
                                                                            <td align="right">
                                                                                <a href="<?= base_url('dist/review/').'/'.$row['review_file'] ?>" class="btn bg-green waves-effect"><?= lang('unduh_hr') ?></a>
                                                                                <br><br>
                                                                                <a href="<?= base_url('dist/submissions/').'/'.$row['fp_file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                                            </td>
                                                                            <td align="right">
                                                                                <a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#detail_job" data-row="<?= $no-2 ?>">
                                                                                    <?= lang('lihat_detail') ?>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane animated flipInX" id="admin_review">
                                    <?php $r=$submission; ?>
                                    <div class="row">
                                        <div class="col-sm-4 col-md-4 col-xs-12">
                                            <div class="thumbnail">
                                                <img src="<?= base_url('dist/images/doc.png') ?>">
                                                <div class="caption">
                                                    <?php if($r['review_file']!=""){ ?>
                                                        <a href="<?= base_url('dist/review/'.$r['review_file']) ?>" class="btn btn-block bg-green waves-effect"><?= lang('unduh_hr') ?></a>
                                                    <?php } else{ ?>
                                                        <a class="btn btn-block bg-red waves-effect">
                                                            <?= lang('unduh_hr').'<br>'.lang('tidak_ada') ?>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <form action="<?= base_url('reviewer/job/save_review') ?>" method="POST" enctype="multipart/form-data" id="review_frm">
                                            <div class="col-sm-12 col-md-8">
                                                <div class="form-group form-float">
                                                    <label><?= lang('komentarUmum') ?></label>
                                                    <span style="color: red">*</span>
                                                    <div class="form-line">
                                                        <input type="hidden" name="id_submission" value="<?= $r['id_submission'] ?>" required>
                                                        <textarea  rows="4" name="general_comment" class="form-control no-resize" placeholder="<?= lang('tulis_komentar') ?>" required><?= $r['general_comment'] ?></textarea>
                                                    </div><br>
                                                    <label><?= lang('putusan') ?></label>
                                                    <span style="color: red">*</span>
                                                    <div class="form-line">
                                                        <select class="form-control show-tick" name="decission_status" required >
                                                            <option disabled <?= $r['decission_status']==""?"selected":"" ?>>-- <?= lang('pilih') ?> --</option>
                                                            <option value="1" <?= $r['decission_status']==1?"selected":"" ?>><?= lang('revisi') ?></option>
                                                            <option value="2" <?= $r['decission_status']==2?"selected":"" ?>><?= lang('tanpa_revisi') ?></option>
                                                        </select>
                                                    </div><br>
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <label><?= lang('unggah_hasil_review') ?></label>
                                                            <span style="color: red">*</span>
                                                            <input type="file" name="review_file">
                                                        </div>
                                                    </div>
                                                    <label><?= lang('abstrak') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div class="form-line">
                                                        <textarea  rows="4" name="abstract_comment" class="form-control no-resize" placeholder="<?= lang('tulis_abstrak') ?>"><?= $r['abstract_comment'] ?></textarea>
                                                    </div><br>
                                                    <label><?= lang('pengantar') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div class="form-line">
                                                        <textarea  rows="4" name="intro_comment" class="form-control no-resize" placeholder="<?= lang('tulis_pengantar') ?>"><?= $r['intro_comment'] ?></textarea>
                                                    </div><br>
                                                    <label><?= lang('metodologi') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div class="form-line">
                                                        <textarea  rows="4" name="methodology_comment" class="form-control no-resize" placeholder="<?= lang('tulis_metodologi') ?>"><?= $r['methodology_comment'] ?></textarea>
                                                    </div><br>
                                                    <label><?= lang('hasil') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div class="form-line">
                                                        <textarea  rows="4" name="result_comment" class="form-control no-resize" placeholder="<?= lang('tulis_hasil') ?>"><?= $r['result_comment'] ?></textarea>
                                                    </div><br>
                                                    <label><?= lang('diskusi') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div class="form-line">
                                                        <textarea  rows="4" name="discussion_comment" class="form-control no-resize" placeholder="<?= lang('tulis_diskusi') ?>"><?= $r['discussion_comment'] ?></textarea>
                                                    </div>
                                                     <br>
                                                     <label><?= lang('refrensi') ?></label>
                                                     <span style="color: red"><?= lang('optional') ?></span>
                                                     <div class="form-line">
                                                        <textarea  rows="4" name="reference" class="form-control no-resize" placeholder="<?= lang('tulis_refrensi') ?>"><?= $r['reference'] ?></textarea>
                                                    </div><br>
                                                    <label><?= lang('lainnya') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div class="form-line">
                                                        <textarea  rows="4" name="other_comment" class="form-control no-resize" placeholder="<?= lang('tulis_lainnya') ?>"><?= $r['other_comment'] ?></textarea>
                                                    </div><br>
                                                    <label><?= lang('keaslian') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div>
                                                    <select name="originality" class="form-control" >
                                                        <option disabled <?= $r['originality']==""?"selected":"" ?>>-- <?= lang('pilih') ?> --</option>  
                                                        <option value="1" <?= $r['originality']==1?"selected":"" ?>>Sangat Bagus</option>
                                                        <option value="2" <?= $r['originality']==2?"selected":"" ?>>Bagus</option>
                                                        <option value="3" <?= $r['originality']==3?"selected":"" ?>>Cukup</option>
                                                        <option value="4" <?= $r['originality']==4?"selected":"" ?>>Jelek</option>
                                                    </select>
                                                    </div><br>
                                                    <label><?= lang('kontribusi') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div>
                                                    <select name="contribution_in_science" class="form-control">
                                                        <option disabled <?= $r['contribution_in_science']==""?"selected":"" ?>>-- <?= lang('pilih') ?> --</option>  
                                                        <option value="1" <?= $r['contribution_in_science']==1?"selected":"" ?>>Sangat Bagus</option>
                                                        <option value="2" <?= $r['contribution_in_science']==2?"selected":"" ?>>Bagus</option>
                                                        <option value="3" <?= $r['contribution_in_science']==3?"selected":"" ?>>Cukup</option>
                                                        <option value="4" <?= $r['contribution_in_science']==4?"selected":"" ?>>Jelek</option>
                                                    </select>
                                                    </div><br>
                                                    <label><?= lang('kualitas') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div>
                                                    <select name="writing_technique" class="form-control">
                                                        <option disabled="" <?= $r['writing_technique']==""?"selected":"" ?>>-- <?= lang('pilih') ?> --</option>  
                                                        <option value="1" <?= $r['writing_technique']==1?"selected":"" ?>>Sangat Bagus</option>
                                                        <option value="2" <?= $r['writing_technique']==2?"selected":"" ?>>Bagus</option>
                                                        <option value="3" <?= $r['writing_technique']==3?"selected":"" ?>>Cukup</option>
                                                        <option value="4" <?= $r['writing_technique']==4?"selected":"" ?>>Jelek</option>
                                                    </select>
                                                    </div><br>
                                                    <label><?= lang('kedalaman') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div>
                                                    <select name="depth_of_research" class="form-control">
                                                        <option disabled="" <?= $r['depth_of_research']==""?"selected":"" ?>>-- <?= lang('pilih') ?> --</option>  
                                                        <option value="1" <?= $r['depth_of_research']==1?"selected":"" ?>>Sangat Bagus</option>
                                                        <option value="2" <?= $r['depth_of_research']==2?"selected":"" ?>>Bagus</option>
                                                        <option value="3" <?= $r['depth_of_research']==3?"selected":"" ?>>Cukup</option>
                                                        <option value="4" <?= $r['depth_of_research']==4?"selected":"" ?>>Jelek</option>
                                                    </select>
                                                    </div><br>
                                                    <label><?= lang('keterbaruan') ?></label>
                                                    <span style="color: red"><?= lang('optional') ?></span>
                                                    <div>
                                                        <select name="novelty_reference" class="form-control">
                                                            <option disabled="" <?= $r['novelty_reference']==""?"selected":"" ?>>-- <?= lang('pilih') ?> --</option>  
                                                            <option value="1" <?= $r['novelty_reference']==1?"selected":"" ?>>Sangat Bagus</option>
                                                            <option value="2" <?= $r['novelty_reference']==2?"selected":"" ?>>Bagus</option>
                                                            <option value="3" <?= $r['novelty_reference']==3?"selected":"" ?>>Cukup</option>
                                                            <option value="4" <?= $r['novelty_reference']==4?"selected":"" ?>>Jelek</option>
                                                        </select>
                                                    </div><br>
                                                    <div class="pull-right">
                                                        <button name="submit" value="0" type="submit" class="btn btn-primary">
                                                            <?= lang('save') ?>
                                                        </button>
                                                        <button name="submit" value="1" type="submit" class="btn btn-primary">
                                                            <?= lang('simpan_dan_submit') ?>
                                                        </button>
                                                        <?php if($r['review_file']!=""){ ?>
                                                            <button name="submit" value="2" type="submit" class="btn btn-primary">
                                                                <?= lang('hanya_submit') ?>
                                                            </button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modals Detail -->
<div class="modal fade" id="detail_job" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">
                    <?= lang('riwayat_review') ?>
                    <div class="pull-right">
                        <button class="btn btn-link waves-effect" data-dismiss="modal">
                            X
                        </button>
                    </div>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('reviewer') ?></label>
                                <div class="form-line" id="reviewer"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentarUmum') ?></label>
                                <div class="form-line" id="general_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('abstract') ?></label>
                                <div class="form-line" id="abstract_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('pengantar') ?></label>
                                <div class="form-line" id="intro_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('methodology') ?></label>
                                <div class="form-line" id="methodology_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('hasil') ?></label>
                                <div class="form-line" id="result_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('diskusi') ?></label>
                                <div class="form-line" id="discussion_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('refrensi') ?></label>
                                <div class="form-line" id="reference"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <label><?= lang('komentar')." ".lang('lainnya') ?></label>
                                <div class="form-line" id="other_comment"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('keaslian') ?></label>
                                <div class="form-line" id="originality"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('kontribusi') ?></label>
                                <div class="form-line" id="contribution_in_science"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('kualitas') ?></label>
                                <div class="form-line" id="writing_technique">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('kedalaman') ?></label>
                                <div class="form-line" id="depth_of_research"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <label><?= lang('keterbaruan') ?></label>
                                <div class="form-line" id="novelty_reference"></div>
                            </div>
                        </div><br><br>
                        <div class="col-sm-12">
                            <div class="form-group form-float"><br>
                                <label><?= lang('ditugaskan_sejak') ?></label>
                                <div id="create_at"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float"><br>
                                <label><?= lang('diselesaikan_pada') ?></label>
                                <div id="finish_at"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
            </div>
        </div>
    </div>
</div>
<!-- End modals -->
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#job_reviewer").addClass("active");
    $(document).ready(function(){
        var table = $('#tbl_review').DataTable();
        table.columns([4,5,6,7,8,9,10,11,12,13,14,15,16,17]).visible(false);
        $("#review_frm" ).validate({
            rules: {
                review_file: {
                    required: true,
                    extension: "doc|docx|pdf"
                }
            },
            messages: {
                review_file: {
                    extension: "Format file harus Doc / Docx / PDF !"
                }
            }
        });
    })
    $('#detail_job').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var row = button.data('row') // Extract info from data-* attributes

        var modal = $(this)

        var table = $('#tbl_review').DataTable();

        var data = table
            .rows(row)
            .data();

        var i = 0;
        for(;i<17;i++){
            if(data[0][i] == ""){
                data[0][i]="Belum Tersedia";
            }
        }

        modal.find('.modal-body #reviewer').html(data[0][1])
        modal.find('.modal-body #general_comment').html(data[0][2])
        modal.find('.modal-body #abstract_comment').html(data[0][3])
        modal.find('.modal-body #intro_comment').html(data[0][4])
        modal.find('.modal-body #methodology_comment').html(data[0][5])
        modal.find('.modal-body #result_comment').html(data[0][6])
        modal.find('.modal-body #discussion_comment').html(data[0][7])
        modal.find('.modal-body #reference').html(data[0][8])
        modal.find('.modal-body #other_comment').html(data[0][9])
        modal.find('.modal-body #originality').html(data[0][10])
        modal.find('.modal-body #contribution_in_science').html(data[0][11])
        modal.find('.modal-body #writing_technique').html(data[0][12])
        modal.find('.modal-body #depth_of_research').html(data[0][13])
        modal.find('.modal-body #novelty_reference').html(data[0][14])
        modal.find('.modal-body #create_at').html(data[0][16])
        modal.find('.modal-body #finish_at').html(data[0][17])
    })
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>