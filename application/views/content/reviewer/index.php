<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><?= lang('list') ?></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('list') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(2)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <?php $this->load->view('partials/form_validation') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>  
                                    <tbody>
                                        <?php $no=1; foreach($get as $not){ ?>
                                        <?php if($not['status_review']==0){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($not['nm_topic']) ?></td>
                                                <td class="text-center"><?= ucwords($not['tittle']) ?></td>
                                                <td class="text-center"><?= reviewer($not['status_review']) ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('dist/submissions/').'/'.$not['fp_file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                </td>
                                                <td align="right">
                                                    <a href='<?= base_url('reviewer/job/detail/'.$not['id_submission']) ?>' class="btn btn-sm btn-primary">
                                                        <?= lang('review_sekarang') ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php } ?>
                                    </tbody>                                   
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="sudah">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <?php $no=1; foreach($get as $ready){  ?>
                                        <?php if($ready['status_review']==1){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($ready['nm_topic']) ?></td>
                                                <td class="text-center"><?= ucwords($ready['tittle']) ?></td>
                                                <td class="text-center"><?= reviewer($ready['status_review']) ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('dist/review/'.$ready['review_file']) ?>" class="btn bg-green waves-effect"><?= lang('unduh_hr') ?></a>
                                                    <br><br>
                                                    <a href="<?= base_url('dist/submissions/'.$ready['fp_file']) ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php } ?>
                                    </tbody>                                   
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>
                    </div>              
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/ui/tooltips-popovers.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#job_reviewer").addClass("active");
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>