<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('list') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('category') ?>
            <small><?= lang('status') ?> <a href="#" target="_blank"><?= ucwords(role($this->session->userdata('role'))) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('email') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <?php $no=1; foreach($proof as $row){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= $row['email'] ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('privilage/using/detail') ?>/<?= $row['id_user'] ?>" class="btn bg-blue waves-effect"><?= lang('detail') ?></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>                                   
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('nm_kategori') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="sudah">
                            <div class="body">
                                <!-- Modals create -->
                                <div class="modal fade" id="insert" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <form id="form_validation" method="POST" action="<?= base_url('master/category/create') ?>">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="defaultModalLabel"><?= lang('tambah_baru') ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" name="category_nm" required>
                                                            <label class="form-label"><?= lang('nm_kategori') ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- End modals -->

                                <!-- Modals Update  -->
                                <div class="modal fade" id="update" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <form id="form_validation" method="POST" action="<?= base_url('master/category/update') ?>">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="defaultModalLabel"><?= lang('ubah_data') ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="hidden" class="form-control" name="id_user" id="id_user" required>
                                                            <input type="text" class="form-control" placeholder="<?= lang('category_nm') ?>" name="category_nm" id="category_nm" required />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- End Modals -->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                        <thead>
                                            <tr>
                                                <th class="text-center"><?= lang('no') ?></th>
                                                <th class="text-center"><?= lang('email') ?></th>
                                                <th class="text-center"><?= lang('aksi') ?></th>
                                            </tr>
                                        </thead>   
                                        <tbody>
                                            <?php $no=1; foreach($use as $row){ ?>
                                                <tr>
                                                    <td class="text-center"><?= $no++ ?></td>
                                                    <td class="text-center"><?= $row['email'] ?></td>
                                                    <td align="right">
                                                        <a href="<?= base_url('privilage/using/access') ?>/<?= $row['id_user'] ?>" class="btn bg-blue waves-effect" onclick="del(<?= $row['id_user'] ?>)" title="<?= lang('tambah_hak_akses') ?>"><i class="material-icons">add</i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>                                   
                                        <tfoot>
                                            <tr>
                                                <th class="text-center"><?= lang('no') ?></th>
                                                <th class="text-center"><?= lang('nm_kategori') ?></th>
                                                <th class="text-center"><?= lang('aksi') ?></th>
                                            </tr>
                                        </tfoot> 
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#adm_privilege").addClass("active");
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>