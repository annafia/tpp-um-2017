<?php section('css'); ?>
<!-- Bootstrap Select Css -->
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('list') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('category') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role($this->session->userdata('role'))) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                </div>
                <div class="body">
                    <?php foreach($get as $row){  ?>
                    <form method="POST" action="<?= base_url('privilage/using/create') ?>">
                        <p style="margin-bottom: 30px; font-size: 18px; font-weight: bold;" class="text-center"><?= lang('hak_akses_untuk') ?> <?= $row['email'] ?></p>
                        <div class="col-sm-12">
                            <p><b><?= lang('pilih_posisi') ?></b></p>
                            <select class="form-control show-tick" name="role" required="">
                                <option value="" disabled="" selected="">-- <?= lang('pilih_posisi') ?> --</option>
                                <option value="0" <?php if(in_array(0, $select_role)){echo 'selected';} ?>><?= lang('superuser') ?></option>
                                <option value="1" <?php if(in_array(1, $select_role)){echo 'selected';} ?>><?= lang('admin') ?></option>
                                <option value="2" <?php if(in_array(2, $select_role)){echo 'selected';} ?>><?= lang('reviewer') ?></option>
                                <option value="3" <?php if(in_array(3, $select_role)){echo 'selected';} ?>><?= lang('check_plagiasi') ?></option>
                                <option value="4" <?php if(in_array(4, $select_role)){echo 'selected';} ?>><?= lang('author') ?></option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <input type="hidden" name="id_user" value="<?= $this->uri->segment(4) ?>">
                            <p><b><?= lang('pilih_hak_akses') ?></b></p>
                            <select class="form-control show-tick" name="privilage_id[]" multiple>
                                <?php foreach($get_privilage as $prev){ ?>
                                    <option <?php if(in_array($prev['privilage_id'], $select_prev)){echo 'selected';} ?> value="<?= $prev['privilage_id'] ?>"><?= ucwords($prev['privilage_nm']) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <button style="margin-top: 30px" class="btn btn-primary waves-effect" type="submit"><?= ucwords(lang('save')) ?></button>
                        <a href="<?= base_url('privilage/using') ?>" style="margin-top: 30px" class="btn btn-default waves-effect" type="submit"><?= ucwords(lang('back')) ?></a>
                    </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>

<!-- Multi Select Plugin Js -->
<script src="<?= base_url('dist') ?>/plugins/multi-select/js/jquery.multi-select.js"></script>

<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#adm_privilege").addClass("active");
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>