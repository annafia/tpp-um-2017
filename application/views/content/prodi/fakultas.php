<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('program_studi') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('program_studi') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('fakultas') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="tbl_fakultas">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('id_fakultas') ?></th>
                                    <th class="text-center"><?= lang('nama_fakultas') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>   
                            <tbody>
                                <?php $no=0; foreach($fakultas as $f){ ?>
                                    <tr>
                                        <td class="text-center"><?= $f['id_fakultas'] ?></td>
                                        <td><?= ucwords($f['fakultas']) ?></td>
                                        <td align="right">
                                            <button class="btn bg-red waves-effect" onclick="del(<?= $f['id_fakultas'] ?>)">
                                                <i class="material-icons">delete</i>
                                            </button>
                                            <button data-toggle="modal" data-target="#update" data-row="<?= $no++ ?>"  class="btn bg-blue waves-effect">
                                                <i class="material-icons">edit</i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('id_fakultas') ?></th>
                                    <th class="text-center"><?= lang('nama_fakultas') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot> 
                        </table>
                    </div><br>
                    <div class="pull-right">
                        <button data-color="light-blue" data-toggle="modal" data-target="#insert" class="btn bg-blue waves-effect"><?= lang('tambah_baru') ?></button>
                    </div><br><br>
                    <h4><?= lang('catatan') ?></h4>
                    <ul>
                        <li>
                            <span style="color: red"><?= lang('catatan_prodi1') ?></span>
                        </li>
                        <li>
                            <span style="color: red"><?= lang('catatan_prodi2') ?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modals create -->
<div class="modal fade" id="insert" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="form_validation" method="POST" action="<?= base_url('master/prodi/create_fakultas') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel"><?= lang('tambah_baru') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="id_fakultas" required>
                                <label class="form-label"><?= lang('id_fakultas') ?></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="fakultas" required>
                                <label class="form-label"><?= lang('nama_fakultas') ?></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End modals -->

<!-- Modals Update  -->
<div class="modal fade" id="update" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="form_validation" method="POST" action="<?= base_url('master/prodi/edit_fakultas') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel"><?= lang('ubah_data') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label><?= lang('id_fakultas') ?></label>
                                <input disabled type="text" class="form-control" id="id_fakultas" required>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label><?= lang('nama_fakultas') ?></label>
                                <input type="hidden" name="id_fakultas" id="id_fakultas" required>
                                <input type="text" class="form-control" name="fakultas" id="nama_fakultas" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Modals -->
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#master_fakultas").addClass("active");
    $("#kelola_prodi").addClass("active");
    function del(id){
        swal({
        title: "Apakah anda yakin?",
        text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('master/prodi/del_fakultas') ?>/"+id;
            swal("Terhapus!", "Data anda berhasil dihapus.", "success");
        });
    }

    $('#update').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var row = button.data('row'); // Extract info from data-* attributes

        var modal = $(this);
        var table = $('#tbl_fakultas').DataTable();
        var data = table.rows(row).data();

        modal.find('.modal-body #id_fakultas').val(data[0][0]);
        modal.find('.modal-body #nama_fakultas').val(data[0][1]);
    })
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>