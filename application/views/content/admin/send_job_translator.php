<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('daftar_tugas_reviewer') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('beri_tugas_translator') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('beri_tugas_translator') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="tbl_send">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('penulis') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('status') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>   
                            <tbody>
                                <?php $no=1; foreach($get_not_yet as $not){  ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td class="text-center"><?= ucwords($not['full_name']) ?></td>
                                        <td class="text-center"><?= ucwords($not['tittle']) ?></td>
                                        <td class="text-center"><?= translator($not['status_translator']) ?></td>
                                        <td align="right">
                                            
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <?= lang('menu_action') ?> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a data-toggle="modal" data-target="#send" data-id="<?= $not['id_submission'] ?>" data-row='<?= $no-2 ?>'>
                                                            <?= lang('send') ?>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a onclick='next(<?= $not['id_submission'] ?>)'>
                                                            <?= lang('lewati_translasi') ?>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('penulis') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('status') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot> 
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="send" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="POST" action="<?= base_url('admin/to_translator/send_job_translator') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="defaultModalLabel"><?= lang('kirim_pekerjaan_translator') ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id_submission" id="id_submission">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?= lang('judul') ?></label>
                            <div class="form-line" id="judul">
                                
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('partials/select/employee_translator') ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('simpan')) ?></button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('close')) ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist') ?>/plugins/multi-select/js/jquery.multi-select.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<!--  -->
<script type="text/javascript">
    $("#adm_job_translator").addClass("active");
    $("#send_job_translator").addClass("active");
    function next(id_submission){
        swal({
        title: "Apakah anda yakin mengubah?",
        text: "Mengubah status karya menjadi selesai!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_translator/next') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }
    $('#send').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var row = button.data('row') // Extract info from data-* attributes
        var id = button.data('id')

        var modal = $(this)

        var table = $('#tbl_send').DataTable();

        var data = table
            .rows(row)
            .data();

        modal.find('.modal-body #id_submission').val(id);
        modal.find('.modal-body #judul').html(data[0][2]);
        
    })
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>