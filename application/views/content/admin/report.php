<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('cetak_laporan') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('cetak_laporan') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <?php $data = $this->input->get(); ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('cetak_laporan') ?> (Jumlah Artikel = <?= $list->num_rows() ?>)
                    </h2><br>
                    <ul>
                        <li>
                            <h4>Tanggal <?= date('d M Y',strtotime($data['tgl1'])) ?>
                            Sampai <?= date('d M Y',strtotime($data['tgl2'])) ?></h4>
                        </li>
                        <?php if($data['id_topic']!=""){ ?>
                            <li>
                                <?php $topic = $this->db->where('id_topic',$data['id_topic'])->get('topic')->row_array(); ?>
                                <h4><?= lang('topic') ?>: <?= $topic['nm_topic'] ?></h4>
                            </li>
                        <?php } if($data['id_fakultas']!=""){ ?>
                            <li>
                                <?php $fakultas = $this->db->where('id_fakultas',$data['id_fakultas'])->get('fakultas')->row_array(); ?>
                                <h4><?= lang('fakultas') ?>: <?= $fakultas['fakultas'] ?></h4>
                            </li>
                        <?php } if($data['id_jurusan']!=""){ ?>
                            <li>
                                <?php $jurusan = $this->db->where('id_jurusan',$data['id_jurusan'])->get('jurusan')->row_array(); ?>
                                <h4><?= lang('jurusan') ?>: <?= $jurusan['jurusan'] ?></h4>
                            </li>
                        <?php } if($data['id_prodi']!=""){ ?>
                            <li>
                                <?php $prodi = $this->db->where('id_prodi',$data['id_prodi'])->get('prodi')->row_array(); ?>
                                <h4><?= lang('prodi') ?>: <?= $prodi['program_studi'] ?></h4>
                            </li>
                        <?php } if($data['reviewer']!=""){ ?>
                            <li>
                                <?php $fil_rev = $this->db->where('id_user',$data['reviewer'])->get('personal_data')->row_array(); ?>
                                <h4><?= lang('reviewer') ?>: <?= $fil_rev['full_name'] ?></h4>
                            </li>
                        <?php } if($data['translator']!=""){ ?>
                            <li>
                                <?php $fil_trans = $this->db->where('id_user',$data['translator'])->get('personal_data')->row_array(); ?>
                                <h4><?= lang('translator') ?>: <?= $fil_trans['full_name'] ?></h4>
                            </li>
                        <?php } if($data['author']!=""){ ?>
                            <li>
                                <?php $fil_author = $this->db->where('id_user',$data['author'])->get('personal_data')->row_array(); ?>
                                <h4><?= lang('author') ?>: <?= $fil_author['full_name'] ?></h4>
                            </li>
                        <?php } if($data['status']!=""){ ?>
                            <li>
                                <h4><?= lang('status') ?>: <?= status_paper($data['status']) ?></h4>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tbl_subm">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('penulis') ?></th>
                                    <th class="text-center"><?= lang('email') ?></th>
                                    <th class="text-center"><?= lang('topic') ?></th>
                                    <th class="text-center"><?= lang('dibuat') ?></th>
                                    <th class="text-center"><?= lang('reviewer') ?></th>
                                    <th class="text-center"><?= lang('translator') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_plagiasi') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_revisi') ?></th>
                                    <th class="text-center"><?= lang('status_karya') ?></th>
                                    <th class="text-center"><?= lang('download') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('penulis') ?></th>
                                    <th class="text-center"><?= lang('email') ?></th>
                                    <th class="text-center"><?= lang('topic') ?></th>
                                    <th class="text-center"><?= lang('dibuat') ?></th>
                                    <th class="text-center"><?= lang('reviewer') ?></th>
                                    <th class="text-center"><?= lang('translator') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_plagiasi') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_revisi') ?></th>
                                    <th class="text-center"><?= lang('status_karya') ?></th>
                                    <th class="text-center"><?= lang('download') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $i=1; foreach($list->result_array() as $row){ ?>
                                <?php 
                                    $rev2 = $this->db->distinct()
                                                    ->select('id_user')
                                                    ->where('id_submission',$row['id_submission'])
                                                    ->where('review_as',1)
                                                    ->where('id_user',$reviewer)
                                                    ->get('review r')->row_array();
                                    if(!$rev2 && $reviewer!=""){
                                        continue;
                                    }
                                    $trans2 = $this->db->select('id_user')
                                                    ->where('id_submission',$row['id_submission'])
                                                    ->where('id_user',$translator)
                                                    ->get('translation t')->row_array();
                                    if(!$trans2 && $translator!=""){
                                        continue;
                                    }
                                ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td><?= ucwords($row['tittle']) ?></td>
                                        <td><?= ucwords($row['full_name']) ?></td>
                                        <td><?= ucwords($row['email']) ?></td>
                                        <td><?= ucwords($row['nm_topic']) ?></td>
                                        <td><?= to_date_time($row['create_at']) ?></td>
                                        <td>
                                            <?php
                                                $rev = $this->db->distinct()
                                                                ->select('full_name')
                                                                ->where('id_submission',$row['id_submission'])
                                                                ->where('review_as',1)
                                                                ->join('personal_data pd','pd.id_user = r.id_user')
                                                                ->get('review r')->result_array();
                                                foreach ($rev as $r) {
                                                    echo $r['full_name'].', ';
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $trans = $this->db->select('full_name')
                                                                ->where('id_submission',$row['id_submission'])
                                                                ->join('personal_data pd','pd.id_user = t.id_user')
                                                                ->get('translation t')->result_array();
                                                foreach ($trans as $t) {
                                                    echo $t['full_name'].', ';
                                                }
                                            ?>
                                        </td>
                                        <td><?= plagiarism($row['status_plagiarism']) ?></td>
                                        <td><?= reviewer($row['status_reviewer']) ?></td>
                                        <td class="text-center"><?= status_paper($row['status']) ?></td>
                                        <td class="text-center"><a href="<?= base_url('dist/submissions').'/'.$row['file'] ?>" class="btn bg-orange"><?= lang('unduh_fp') ?></a></td>
                                        <td>
                                            <a class="btn btn-info" href="<?= base_url('admin/to_reviewer/show_detail/'.$row['id_submission']) ?>"><?= lang('lihat_detail') ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#report").addClass("active");
    $(document).ready(function(){
        var table = $('#tbl_subm').DataTable({
            destroy: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'A4',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
                    }
                },
                {
                    extend: 'print',
                    orientation: 'landscape',
                    pageSize: 'A4',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                    }
                },
            ]
        });
    })
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>