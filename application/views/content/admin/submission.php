<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><?= lang('validasi_bukti_submit') ?></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('validasi_bukti_submit') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('validasi_bukti_submit') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('email') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('dibuat') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('email') ?></th>    
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('dibuat') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php $i=1; foreach($list as $row){ ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= ucwords($row['tittle']) ?></td>
                                                <td><?= ucwords($row['full_name']) ?></td>
                                                <td><?= ucwords($row['email']) ?></td>
                                                <td><?= ucwords($row['nm_topic']) ?></td>
                                                <td><?= to_date_time($row['create_at']) ?></td>
                                                <td class="text-center">
                                                    <a href="<?= base_url('dist/submitted_proof/'.$row['submitted_proof']) ?>" class="btn btn-block btn-primary"><?= strtoupper(lang('unduh_bukti_submit')) ?></a> 
                                                    <button onclick="valid('<?= $row['id_submission'] ?>')" class="btn btn-block btn-success"><?= lang('data_valid') ?></button>
                                                    <button onclick="not_valid('<?= $row['id_submission'] ?>')" class="btn btn-block btn-danger"><?= lang('data_tidak_valid') ?></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="sudah">
                            <div class="table-responsive">
                                 <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('email') ?></th>
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('dibuat') ?></th>
                                            <th class="text-center"><?= lang('status_karya') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('email') ?></th>    
                                            <th class="text-center"><?= lang('topic') ?></th>
                                            <th class="text-center"><?= lang('dibuat') ?></th>
                                            <th class="text-center"><?= lang('status_karya') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php $i=1; foreach($finish as $data){ ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= ucwords($data['tittle']) ?></td>
                                                <td><?= ucwords($data['full_name']) ?></td>
                                                <td><?= ucwords($data['email']) ?></td>
                                                <td><?= ucwords($data['nm_topic']) ?></td>
                                                <td><?= to_date_time($data['create_at']) ?></td>
                                                <td class="text-center"><?= status_paper($data['status']) ?></td>
                                                <td>
                                                    <a href="<?= base_url('dist/submitted_proof/'.$data['submitted_proof']) ?>" class="btn btn-block btn-primary"><?= strtoupper(lang('unduh_bukti_submit')) ?></a> 
                                                    <button class="btn btn-block btn-danger" onclick="back('<?= $data['id_submission'] ?>')"><?=lang('kembalikan_status_awal')?></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#valid_submit").addClass("active");
    function valid(id_submission){
        swal({
        title: "Apakah anda yakin?",
        text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/activity/valid') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }

    function not_valid(id_submission){
        swal({
        title: "Apakah anda yakin?",
        text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/activity/not_valid') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }

    function back(id_submission){
        swal({
        title: "Apakah anda yakin?",
        text: "Data yang telah diproses tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/activity/back') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }

</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>