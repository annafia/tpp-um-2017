<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('admin/to_plagiarism') ?>"><?= lang('list') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('list') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th class="text-center"><?= lang('no') ?></th>
                                        <th class="text-center"><?= lang('penulis') ?></th>
                                        <th class="text-center"><?= lang('judul') ?></th>
                                        <th class="text-center"><?= lang('status') ?></th>
                                        <th class="text-center"><?= lang('aksi') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; foreach($list as $not){  ?>
                                        <tr>
                                            <td class="text-center"><?= $no++ ?></td>
                                            <td class="text-center">
                                                <?= ucwords($not['full_name']) ?><br>
                                                <?php if($not['position']==1){ ?>
                                                    <span style="color: #FFC107; font-weight: bold">
                                                        (<?= position($not['position']) ?>)<br>
                                                    </span>
                                                <?php } else if($not['position']==2){ ?>
                                                    <span style="color: orange; font-weight: bold">
                                                        (<?= position($not['position']) ?>)<br>
                                                    </span>
                                                <?php } ?>
                                                <span style="color: red; font-weight: bold">
                                                    (Proses: <?= $not['proccess_time'] ?>, Selesai: <?= $not['finished_submission'] ?>)
                                                </span>
                                            </td>
                                            <td class="text-center"><?= ucwords($not['tittle']) ?></td>
                                            <td class="text-center"><?= plagiarism($not['status_plagiarism']) ?></td>
                                            <td class="text-center">
                                                <?php if($not['status_plagiarism'] == 0){ ?>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <?= lang('menu_action') ?> <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#defaultModal"><?= lang('send') ?></a></li>
                                                        <li><a href="<?= base_url('dist/submissions/').'/'.$not['file'] ?>"><?= lang('unduh_fp') ?></a></li>
                                                        <li><a onclick='next(<?= $not['id_submission'] ?>)'><?= lang('lewati_plagiasi') ?></a></li>
                                                    </ul>
                                                </div>
                                                <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <form method="POST" action="<?= base_url('admin/to_plagiarism/save_plagiarism') ?>">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title text-center" id="defaultModalLabel"><?= lang('kirim_pekerjaan_palgiarism') ?></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <input type="hidden" name="id_submission" value="<?= $not['id_submission'] ?>">
                                                                    <?php $this->load->view('partials/select/employee_plagiarism') ?>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-link waves-effect"><?= strtoupper(lang('send')) ?></button>
                                                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('close')) ?></button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center"><?= lang('no') ?></th>
                                        <th class="text-center"><?= lang('penulis') ?></th>
                                        <th class="text-center"><?= lang('judul') ?></th>
                                        <th class="text-center"><?= lang('status') ?></th>
                                        <th class="text-center"><?= lang('aksi') ?></th>
                                    </tr>
                                </tfoot> 
                            </table>
                        </div>
                        <br>
                        <h4><?= lang('catatan') ?></h4>
                        <ol type="1">
                            <li><?= lang('ctt_send_job1') ?></li>
                            <li><?= lang('ctt_send_job2') ?></li>
                            <li><?= lang('ctt_send_job_plag1') ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#send_job_plagiarism").addClass("active");
    $("#adm_job_plagiarism").addClass("active");
    function next(id_submission){
        swal({
        title: "Apakah anda yakin?",
        text: "Melewati tahap pemeriksaan plagiasi !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_plagiarism/next') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>