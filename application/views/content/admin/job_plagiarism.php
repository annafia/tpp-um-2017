<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('list/account') ?>"><?= lang('daftar_tugas_cek_plagiarism') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('daftar_tugas_cek_plagiarism') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('daftar_tugas_cek_plagiarism') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('petugas') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('petugas') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot>  
                                    <tbody>
                                        <?php $no=1; foreach($list as $row){ ?>
                                        <?php if($row['status_plag']==0){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                <td class="text-center"><?= to_date_time($row['date_rev']) ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('dist/submissions/').'/'.$row['file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                </td>
                                                <td align="right">
                                                    <a onclick="cancel(<?= $row['id_submission'] ?>)" class="btn btn-danger waves-effect"><?= lang('batalkan_tugas') ?></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <h4><?= lang('catatan') ?></h4>
                            <ol type="1">
                                <li><?= lang('ctt_job1') ?></li>
                                <li><?= lang('ctt_job2') ?></li>
                            </ol>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="sudah">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('petugas') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                            <th class="text-center"><?= lang('komentar') ?></th>
                                            <th class="text-center"><?= lang('persentase_plag') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('petugas') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                            <th class="text-center"><?= lang('komentar') ?></th>
                                            <th class="text-center"><?= lang('persentase_plag') ?></th>
                                            <th class="text-center"><?= lang('unduh') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot>  
                                    <tbody>
                                        <?php $no=1; foreach($list as $row){ ?>
                                        <?php if($row['status_plagiarism']==3 && $row['status_plag']==1){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                <td class="text-center"><?= to_date_time($row['date_rev']) ?></td>
                                                <td class="text-center"><?= to_date_time($row['finish']) ?></td>
                                                <td class="text-center"><?= $row['comment'] ?></td>
                                                <td class="text-center"><?= $row['persen_plag'] ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('dist/plagiarism/').'/'.$row['plagiarism_file'] ?>" class="btn bg-blue waves-effect"><?= lang('unduh_hp') ?></a>
                                                    <br><br>
                                                    <a href="<?= base_url('dist/submissions/').'/'.$row['fp_file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                </td>
                                                <td>
                                                    <a onclick="next(<?= $row['id_submission'] ?>)" class="btn bg-teal waves-effect"><?= lang('lanjut_ke_tahap_review') ?></a>
                                                    <br><br>
                                                    <a onclick="back_author(<?= $row['id_submission'] ?>)" class="btn bg-orange waves-effect"><?= lang('kembalikan_ke_author') ?></a>
                                                    <br><br>
                                                    <a onclick="back_check(<?= $row['id_submission'] ?>)" class="btn bg-red waves-effect"><?= lang('kembalikan_ke_petugas') ?></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <h4><?= lang('catatan') ?></h4>
                            <ol type="1">
                                <li><?= lang('ctt_job3') ?></li>
                                <li><?= lang('ctt_job4') ?></li>
                                <li><?= lang('ctt_job5') ?></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#list_job_plagiarism").addClass("active");
    $("#adm_job_plagiarism").addClass("active");
    function cancel(id){
        swal({
        title: "Apakah anda yakin ?",
        text: "Apabila ada data hasil pemeriksaan plagiasi akan dihapus !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_plagiarism/cancel') ?>/"+id;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }
    function back_check(id_submission){
        swal({
        title: "Apakah anda yakin ?",
        text: "Mengembalikan artikel ke petugas pengecekan plagiarism !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_plagiarism/back_check') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }

    function back_author(id_submission){
        swal({
        title: "Apakah anda yakin ?",
        text: "Mengubah status karya menjadi Revisi !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_plagiarism/back_author') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }

    function next(id_submission){
        swal({
        title: "Apakah anda yakin ?",
        text: "Melanjutkan ke tahap review !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_plagiarism/next') ?>/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>