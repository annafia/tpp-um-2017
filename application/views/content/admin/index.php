<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('admin/to_plagiarism') ?>"><?= lang('list') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('list') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#revisi" data-toggle="tab">
                                <i class="material-icons">assignment_late</i> <?= lang('rev') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <?php $no=1; foreach($get_not_yet as $not){  ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($not['email']) ?></td>
                                                <td class="text-center"><?= ucwords($not['tittle']) ?></td>
                                                <td class="text-center"><?= plagiarism($not['status_plagiarism']) ?></td>
                                                <td align="right">
                                                    <?php if($not['status_plagiarism'] == 0){ ?>
                                                        <button type="button" title="<?= lang('sudah') ?>" data-color="green" class="btn bg-green waves-effect" data-toggle="modal" data-target="#check" onclick="check('<?= $not['id_user'] ?>', '<?= $not['id_submission'] ?>')"><i class="material-icons">done</i></button>
                                                        <button type="button" title="<?= lang('revisi') ?>" data-color="amber" class="btn bg-amber waves-effect" data-toggle="modal" data-target="#revision" onclick="revision('<?= $not['id_user'] ?>', '<?= $not['id_submission'] ?>')"><i class="material-icons">assignment_late</i></button>
                                                    <?php } ?>
                                                    <button type="button" data-color="teal" class="btn bg-teal waves-effect" title="<?= lang('download_karya') ?>"><i class="material-icons">file_download</i></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>                                   
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="revisi">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <?php $no=1; foreach($get_revision as $rev){  ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($rev['email']) ?></td>
                                                <td class="text-center"><?= ucwords($rev['tittle']) ?></td>
                                                <td class="text-center"><?= plagiarism($rev['status_plagiarism']) ?></td>
                                                <td align="right">
                                                    <button type="button" data-color="teal" class="btn bg-teal waves-effect" title="<?= lang('download_karya') ?>"><i class="material-icons">file_download</i></button>
                                                    <?php if($rev['status_plagiarism'] == 2){ ?>
                                                        <button type="button" title="<?= lang('kembalikan_status_awal') ?>" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#back" onclick="back('<?= $rev['id_user'] ?>', '<?= $rev['id_submission'] ?>')"><i class="material-icons">autorenew</i></button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>                                   
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="sudah">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <?php $no=1; foreach($get_already as $ready){  ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($ready['email']) ?></td>
                                                <td class="text-center"><?= ucwords($ready['tittle']) ?></td>
                                                <td class="text-center"><?= plagiarism($ready['status_plagiarism']) ?></td>
                                                <td align="right">
                                                     <button type="button" data-color="teal" class="btn bg-teal waves-effect" title="<?= lang('download_karya') ?>"><i class="material-icons">file_download</i></button>
                                                    <?php if($ready['status_plagiarism'] == 1){ ?>
                                                        <button type="button" title="<?= lang('kembalikan_status_awal') ?>" data-color="red" class="btn bg-red waves-effect" data-toggle="modal" data-target="#back" onclick="back('<?= $ready['id_user'] ?>', '<?= $ready['id_submission'] ?>')"><i class="material-icons">autorenew</i></button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>                                   
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('penulis') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    function check(id_user, id_submission){
        swal({
        title: "Apakah anda yakin mengubah?",
        text: "Mengubah status karya menjadi selesai!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_plagiarism/check') ?>/"+id_user+"/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }

    function revision(id_user, id_submission){
        swal({
        title: "Apakah anda yakin?",
        text: "Mengubah status karya menjadi revisi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
             window.location.href = "<?= base_url('admin/to_plagiarism/revision') ?>/"+id_user+"/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }

    function back(id_user, id_submission){
        swal({
        title: "Apakah anda yakin?",
        text: "Mengembalikan setatus karya seperti awal!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
             window.location.href = "<?= base_url('admin/to_plagiarism/back') ?>/"+id_user+"/"+id_submission;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>