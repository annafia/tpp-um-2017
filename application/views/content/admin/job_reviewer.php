<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('daftar_tugas_reviewer') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('daftar_tugas_reviewer') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('daftar_tugas_reviewer') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('status_karya') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>   
                            <tbody>
                                <?php $no=1; foreach($list as $row){ ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                        <td class="text-center">
                                            <?= status_karya_reviewer($row['status_reviewer']) ?>
                                        </td>
                                        <td align="right">
                                            <a class="btn btn-sm btn-info" href="<?= base_url('admin/to_reviewer/show_detail_review/'.$row['id_submission']) ?>">
                                                <?= lang('lihat_detail_pekerjaan') ?>
                                            </a><!-- <br><br>
                                            <a onclick="cancel(<?= $row['id_submission'] ?>)" class="btn btn-danger waves-effect"><?= strtoupper(lang('batalkan_tugas')) ?></a> -->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>                           
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('status_karya') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot> 
                        </table>
                    </div>
                    <br>
                    <h4><?= lang('catatan') ?></h4>
                    <ol type="1">
                        <li><?= lang('tombol_belum_diperiksa_rev') ?></li>
                        <li><?= lang('ket_status') ?></li>
                        <li><?= lang('status_cek_plagiasi_selesai') ?></li>
                        <li><?= lang('plagiari_revisi') ?></li>
                        <li><?= lang('tombol_unduh') ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#adm_job_reviewer").addClass("active");
    $("#list_job_reviewer").addClass("active");
    function cancel(id){
        swal({
        title: "Apakah anda yakin ?",
        text: "Semua data hasil review dari artikel ini akan dihapus !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_reviewer/cancel') ?>/"+id;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>