<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="#"><?= lang('cetak_laporan') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('cetak_laporan') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('atur_cetak_laporan') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix text-center">
                        <form method="GET" action="<?= base_url('admin/activity/report') ?>">
                            <div class="col-sm-12 col-md-5 text-center">
                                <h2 class="card-inside-title"><?= lang('tanggal1') ?></h2>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="date" name="tgl1" class="form-control" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-2 text-center">
                                <h2 class="card-inside-title"><?= lang('hingga') ?></h2>
                            </div>
                            <div class="col-sm-12 col-md-5 text-center">
                                <h2 class="card-inside-title"><?= lang('tanggal2') ?></h2>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="date" name="tgl2" class="form-control" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <h2 class="card-inside-title"><?= lang('bidang_riset') ?></h2>
                                <select class="form-control show-tick" name="id_topic" data-live-search="true">
                                    <option value="" selected>-- Semua Bidang Riset --</option>
                                    <?php foreach($this->db->get('topic')->result_array() as $data){ ?>
                                        <option value="<?= $data['id_topic'] ?>"><?= ucwords($data['nm_topic']) ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php $this->load->view('partials/select/program_studi_report') ?>
                            <div class="col-sm-12 col-md-6">
                                <h2 class="card-inside-title"><?= lang('reviewer') ?></h2>
                                <select class="form-control show-tick" data-live-search="true" name="reviewer">
                                    <option value="" selected>-- Semua Reviewer --</option>
                                    <?php foreach($this->db->join('data_privilage dp', 'dp.id_user = pd.id_user')->where('privilage_id', 5)->where('status', 1)->get('personal_data pd')->result_array() as $user){ ?>
                                        <option  value="<?= $user['id_user'] ?>"><?= $user['full_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <h2 class="card-inside-title"><?= lang('translator') ?></h2>
                                <select class="form-control show-tick" data-live-search="true" name="translator">
                                    <option value="" selected>-- Semua Translator --</option>
                                     <?php foreach($this->db->join('data_privilage dp', 'dp.id_user = pd.id_user')->where('privilage_id', 6)->where('status', 1)->get('personal_data pd')->result_array() as $user){ ?>
                                        <option value="<?= $user['id_user'] ?>"><?= $user['full_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <h2 class="card-inside-title"><?= lang('author') ?></h2>
                                <select class="form-control show-tick" data-live-search="true" name="author">
                                    <option value="" selected>-- Semua Author --</option>
                                     <?php foreach($this->db->join('data_privilage dp', 'dp.id_user = pd.id_user')->where('privilage_id', 3)->where('status', 1)->get('personal_data pd')->result_array() as $user){ ?>
                                        <option value="<?= $user['id_user'] ?>"><?= $user['full_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <h2 class="card-inside-title"><?= lang('status') ?></h2>
                                <select class="form-control show-tick" data-live-search="true" name="status">
                                    <option value="" selected>-- Semua Status --</option>
                                    <option value="0"><?= lang('proses_pemeriksaan') ?></option>
                                    <option value="1"><?= lang('lolos_semua_seleksi') ?></option>
                                    <option value="2"><?= lang('rev') ?></option>
                                    <option value="3"><?= lang('sudah_submit') ?></option>
                                    <option value="4"><?= lang('bukti_tidak_valid') ?></option>
                                    <option value="6"><?= lang('menunggu_persetujuan') ?></option>
                                    <option value="7"><?= lang('proses_plagiarism') ?></option>
                                    <option value="8"><?= lang('proses_review') ?></option>
                                    <option value="9"><?= lang('proses_translasi') ?></option>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-lg btn-info"><?= lang('cetak') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('cetak_semua_laporan') ?>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tbl_subm">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('penulis') ?></th>
                                    <th class="text-center"><?= lang('email') ?></th>
                                    <th class="text-center"><?= lang('topic') ?></th>
                                    <th class="text-center"><?= lang('dibuat') ?></th>
                                    <th class="text-center"><?= lang('reviewer') ?></th>
                                    <th class="text-center"><?= lang('translator') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_plagiasi') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_revisi') ?></th>
                                    <th class="text-center"><?= lang('status_karya') ?></th>
                                    <th class="text-center"><?= lang('download') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th class="text-center"><?= lang('no') ?></th>
                                    <th class="text-center"><?= lang('judul') ?></th>
                                    <th class="text-center"><?= lang('penulis') ?></th>
                                    <th class="text-center"><?= lang('email') ?></th>
                                    <th class="text-center"><?= lang('topic') ?></th>
                                    <th class="text-center"><?= lang('dibuat') ?></th>
                                    <th class="text-center"><?= lang('reviewer') ?></th>
                                    <th class="text-center"><?= lang('translator') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_plagiasi') ?></th>
                                    <th class="text-center"><?= lang('pengecekan_revisi') ?></th>
                                    <th class="text-center"><?= lang('status_karya') ?></th>
                                    <th class="text-center"><?= lang('download') ?></th>
                                    <th class="text-center"><?= lang('aksi') ?></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $i=1; foreach($list as $row){ ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td><?= ucwords($row['tittle']) ?></td>
                                        <td><?= ucwords($row['full_name']) ?></td>
                                        <td><?= ucwords($row['email']) ?></td>
                                        <td><?= ucwords($row['nm_topic']) ?></td>
                                        <td><?= to_date_time($row['create_at']) ?></td>
                                        <td>
                                            <?php
                                                $reviewer = $this->db->distinct()
                                                                    ->select('full_name')
                                                                    ->where('id_submission',$row['id_submission'])
                                                                    ->where('review_as',1)
                                                                    ->join('personal_data pd','pd.id_user = r.id_user')
                                                                    ->get('review r')->result_array();
                                                foreach ($reviewer as $r) {
                                                    echo $r['full_name'].', ';
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $translator = $this->db->select('full_name')
                                                                        ->where('id_submission',$row['id_submission'])
                                                                        ->where('status_translation',1)
                                                                        ->join('personal_data pd','pd.id_user = t.id_user')
                                                                        ->get('translation t')->result_array();
                                                foreach ($translator as $t) {
                                                    echo $t['full_name'].', ';
                                                }
                                            ?>
                                        </td>
                                        <td><?= plagiarism($row['status_plagiarism']) ?></td>
                                        <td><?= reviewer($row['status_reviewer']) ?></td>
                                        <td class="text-center"><?= status_paper($row['status']) ?></td>
                                        <td class="text-center"><a href="<?= base_url('dist/submissions').'/'.$row['file'] ?>" class="btn bg-orange"><?= lang('unduh_fp') ?></a></td>
                                        <td>
                                            <a class="btn btn-info" href="<?= base_url('admin/to_reviewer/show_detail/'.$row['id_submission']) ?>"><?= lang('lihat_detail') ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<script src="<?= base_url('dist') ?>/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist/app/api.js') ?>"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#report").addClass("active");
    $(document).ready(function(){
        var table = $('#tbl_subm').DataTable({
            destroy: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'A4',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
                    }
                },
                {
                    extend: 'print',
                    orientation: 'landscape',
                    pageSize: 'A4',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
                    }
                },
            ]
        });
    })
    /* Start Prodi Event*/
    $("#fakultas").change(function(){
        var id_fakultas = $("#fakultas").val();
        var ApiParam = {
            "id_fakultas":id_fakultas
        };
        var datajur = CallApi('prodi/get_jurusan', ApiParam);

        $("#jurusan").html(
            '<option value="" selected="">-- <?= lang("semua_jurusan") ?> --</option>'
        );
        var jurusan = $("#jurusan").html();

        $.each(datajur, function (index,item) {
            jurusan += '<option value="' + item.id_jurusan + '">' + item.jurusan + '</option>';
        });

        $("#jurusan").html(jurusan);
        
        $("#prodi").html(
            '<option value="" selected="">-- <?= lang("semua_prodi") ?> --</option>'
        );
        var prodi = $("#prodi").html();
        $('.selectpicker').selectpicker('refresh');
    });
    $("#jurusan").change(function(){
        var id_jurusan = $("#jurusan").val();
        var ApiParam = {
            "id_jurusan":id_jurusan
        };
        $("#prodi").html(
            '<option value="" selected="">-- <?= lang("semua_prodi") ?> --</option>'
        );
        var prodi = $("#prodi").html();

        var dataprod = CallApi('prodi/get_prodi', ApiParam);

        $.each(dataprod, function (index,item) {
            prodi += '<option value="' + item.id_prodi + '">' + item.program_studi + '</option>';
        });
        
        $("#prodi").html(prodi);
        $('.selectpicker').selectpicker('refresh');
    });
    /* End Prodi Event*/
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>