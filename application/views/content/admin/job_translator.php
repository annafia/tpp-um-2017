<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><a href="<?= base_url('list/account') ?>"><?= lang('daftar_tugas_reviewer') ?></a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('daftar_tugas_translator') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(1)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('daftar_tugas_translator') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('pengurus') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status_kerja') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($list as $row){ ?>
                                        <?php if($row['status_translation']==0){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                <td class="text-center"><?= reviewer($row['status_translation']) ?></td>
                                                <td class="text-center"><?= to_date_time($row['create_at']) ?></td>
                                                <td align="right">
                                                    <a onclick="cancel(<?= $row['id_submission'] ?>)" class="btn btn-danger waves-effect"><?= lang('batalkan_tugas') ?></a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('pengurus') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status_kerja') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="sudah">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('pengurus') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status_kerja') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                            <th class="text-center"><?= lang('download') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($list as $row){ ?>
                                        <?php if($row['status_translation']==1){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= ucwords($row['full_name']) ?></td>
                                                <td class="text-center"><?= ucwords($row['tittle']) ?></td>
                                                <td class="text-center"><?= reviewer($row['status_translation']) ?></td>
                                                <td class="text-center"><?= to_date_time($row['create_at']) ?></td>
                                                <td class="text-center"><?= to_date_time($row['finish_at']) ?></td>
                                                <td>
                                                    <a class="btn bg-cyan" href="<?= base_url('dist/translation/'.$row['translation_file']) ?>">
                                                        <i class="material-icons">get_app</i>
                                                        <?= lang('unduh_ht') ?>
                                                    </a><br><br>
                                                    <a class="btn bg-orange" href="<?= base_url('dist/submissions/'.$row['file']) ?>">
                                                        <i class="material-icons">get_app</i>
                                                        <?= lang('unduh_fp') ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" onclick="next(<?= $row['id_submission'] ?>)">
                                                        <?= lang('lanjut') ?>
                                                    </a><br><br>
                                                    <a class="btn btn-danger" onclick="back(<?= $row['id_submission'] ?>)">
                                                        <?= lang('kembalikan_ke_petugas') ?>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('pengurus') ?></th>
                                            <th class="text-center"><?= lang('judul') ?></th>
                                            <th class="text-center"><?= lang('status_kerja') ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('diselesaikan_pada') ?></th>
                                            <th class="text-center"><?= lang('download') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#adm_job_translator").addClass("active");
    $("#list_job_translator").addClass("active");
    function next(id){
        swal({
        title: "Apakah anda yakin?",
        text: "Melanjutkan ke Tahap Selanjutnya !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_translator/next') ?>/"+id;
            swal("Terhapus!", "Data anda berhasil dihapus.", "success");
        });
    }
    function back(id){
        swal({
        title: "Apakah anda yakin?",
        text: "Lakukan aksi ini apabila pekerjaan yang dilakukan oleh petugas Kurang Sesuai !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_translator/back') ?>/"+id;
            swal("Terhapus!", "Data anda berhasil dihapus.", "success");
        });
    }
    function cancel(id){
        swal({
        title: "Apakah anda yakin ?",
        text: "Apabila ada data hasil translasi yang disimpan, maka akan dihapus juga !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, saya yakin !",
        closeOnConfirm: false
    },
        function(){
            window.location.href = "<?= base_url('admin/to_translator/cancel') ?>/"+id;
            swal("Sukses!", "Data anda berhasil disimpan.", "success");
        });
    }
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>