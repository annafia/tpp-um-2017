<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= base_url('dist') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <div class="body right">
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
            <li><?= lang('list') ?></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="block-header">
        <h2>
            <?= lang('list') ?>
            <small><?= lang('status') ?> <a href="#"><?= ucwords(role(3)) ?></a></small>
        </h2>
    </div>
    <?php $this->load->view('partials/message') ?>
    <?php $this->load->view('partials/form_validation') ?>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?= lang('list') ?>
                    </h2>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#belum" data-toggle="tab">
                                <i class="material-icons">assignment</i> <?= lang('belum') ?>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#sudah" data-toggle="tab">
                                <i class="material-icons">done</i> <?= lang('sudah') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="belum">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul')." / ".lang("topic") ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                        <?php $no=1; foreach($get_not as $not){ ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center">
                                                    <?= ucwords($not['tittle'])." / ".ucwords($not['nm_topic']) ?>
                                                </td>
                                                <td class="text-center"><?= to_date_time($not['create_at']) ?></td>
                                                <td align="right">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <?= lang('menu_action') ?> <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#detail_job"><?= lang('detail_pekerjaan') ?></a></li>
                                                            <li><a href="<?= base_url('dist/submissions/').'/'.$not['file'] ?>"><?= lang('unduh_fp') ?></a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
<!-- Modals create -->
<div class="modal fade" id="detail_job" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">
                    <?= lang('detail_pekerjaan') ?>
                    <div class="pull-right">
                        <button class="btn btn-link waves-effect" data-dismiss="modal">
                            X
                        </button>
                    </div>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home_dt" data-toggle="tab">
                                    <?= lang('detail_artikel') ?>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#pekerjaan" data-toggle="tab">
                                    <?= lang("kerjakann") ?>
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane animated flipInX active" id="home_dt">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-xs-12">
                                        <div class="thumbnail">
                                            <img src="<?= base_url('dist/images/doc.png') ?>">
                                            <div class="caption">
                                                <a title="<?= lang('unduh_fp') ?>" href="<?= base_url('dist/submissions/').'/'.$not['file'] ?>" class="btn btn-block bg-orange waves-effect"><?= lang('unduh_fp') ?></a><br><br>
                                                <?php if($not['plagiarism_file']!=""){ ?>
                                                    <a title="<?= lang('unduh_hp') ?>" href="<?= base_url('dist/plagiarism/'.$not['plagiarism_file']) ?>" class="btn btn-block bg-blue waves-effect">
                                                        <?= lang('unduh_hp') ?>
                                                    </a>
                                                <?php } else{ ?>
                                                    <a class="btn btn-block bg-red waves-effect">
                                                        <?= lang("belum_tersedia") ?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="col-sm-12">
                                            <div class="form-group form-float">
                                                <label><?= lang('judul') ?></label>
                                                <div class="form-line">
                                                    <?= $not['tittle'] ?>    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-float">
                                                <label><?= lang('abstrak') ?></label>
                                                <div class="form-line">
                                                    <?= $not['abstract'] ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-float">
                                                <label><?= lang('topik') ?></label>
                                                <div class="form-line">
                                                    <?= $not['nm_topic'] ?>    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-float">
                                                <label><?= lang('kata_kunci') ?></label>
                                                <div class="form-line">
                                                    <?= $not['keyword'] ?>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane animated flipInX" id="pekerjaan">
                                <form method="post" action="<?= base_url("plagiarism/job/save") ?>" enctype="multipart/form-data" id="job_frm">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group form-float">
                                                <label><?= lang('persentase_plag') ?></label>
                                                <div class="input-group">
                                                    <div class="form-line">
                                                        <input type="hidden" name="id_plagiarism" value="<?= $not['id_plagiarism'] ?>">
                                                        <input type="hidden" name="id_submission" value="<?= $not['id_submission'] ?>">
                                                        <input type="text" class="form-control" name="persen_plag" required value="<?= $not['persen_plag'] ?>">
                                                    </div>
                                                    <span class="input-group-addon">persen(%)</span>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <label>
                                                    <?= lang('komentar') ?> 
                                                    <span style="color: red; font-size: 10px"> (Optional)</span>
                                                </label>
                                                <div class="form-line">
                                                    <textarea rows="4" class="form-control" name="comment"><?= $not['comment'] ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <label><?= lang('hasil_plagiarism') ?></label>
                                                <div class="form-line">
                                                    <input type="file" class="form-control" name="plagiarism_file" required>
                                                </div>
                                            </div>
                                            <div class="pull-right">
                                                <div class="row">
                                                    <button name="submit" type="submit" class="btn btn-link waves-effect" value="0">
                                                        <?= strtoupper(lang('simpan')) ?>
                                                    </button>
                                                    <button name="submit" type="submit" class="btn btn-link waves-effect" value="1">
                                                        <?= strtoupper(lang('simpan_kirim')) ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?= strtoupper(lang('tutup')) ?></button>
            </div>
        </div>
    </div>
</div>
<!-- End modals -->
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul')." / ".lang("topic") ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="sudah">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul')." / ".lang("topic") ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('selesai') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($get_finish as $ready){  ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center">
                                                    <?= ucwords($ready['tittle'])." / ".ucwords($ready['nm_topic']) ?>
                                                </td>
                                                <td class="text-center"><?= to_date_time($ready['create_at']) ?></td>
                                                <td class="text-center"><?= to_date_time($ready['finish']) ?></td>
                                                <td align="right">
                                                    <a href="<?= base_url('dist/plagiarism/').'/'.$ready['plagiarism_file'] ?>" class="btn bg-blue waves-effect"><?= lang('unduh_hp') ?></a>
                                                    <br><br>
                                                    <a href="<?= base_url('dist/submissions/').'/'.$ready['file'] ?>" class="btn bg-orange waves-effect"><?= lang('unduh_fp') ?></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center"><?= lang('no') ?></th>
                                            <th class="text-center"><?= lang('judul')." / ".lang("topic") ?></th>
                                            <th class="text-center"><?= lang('ditugaskan_sejak') ?></th>
                                            <th class="text-center"><?= lang('selesai') ?></th>
                                            <th class="text-center"><?= lang('aksi') ?></th>
                                        </tr>
                                    </tfoot> 
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- 
                    <br>
                    <h4><?= lang('catatan') ?></h4>
                    <ol type="1">
                        <li><?= lang('tombol_selesai') ?></li>
                        <li><?= lang('tombol_revisi') ?></li>
                        <li><?= lang('tombol_unduh') ?></li>
                    </ol> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php endsection()?>

<?php section('js'); ?>
<!-- Data Table -->
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= base_url('dist') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url('dist') ?>/js/pages/tables/jquery-datatable.js"></script>
<script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<?php endsection(); ?>

<?php section('script'); ?>
<script type="text/javascript">
    $("#job_plagiarism").addClass("active");
    $("#job_frm" ).validate({
        rules: {
            plagiarism_file: {
                required: true,
                extension: "doc|docx|pdf"
            }
        },
        messages: {
            plagiarism_file: {
                extension: "Format file harus Doc / Docx / PDF !"
            }
        }
    });
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>