<?php section('css'); ?>
<link href="<?= base_url('dist') ?>/plugins/light-gallery/css/lightgallery.css" rel="stylesheet" />
<?php endsection(); ?>

<?php section('toolbar') ?>
<!--  -->
<?php endsection() ?>

<?php section('content') ?>

<div class="block-header">
    <h2><?= lang('dashboard') ?></h2>
    <div class="body right">
        <ol class="breadcrumb">
            <li class="active"><a href="<?= base_url('dashboard') ?>"><?= lang('home') ?></a></li>
        </ol>
    </div>
</div>
<div class="row clearfix">
    <!-- Task Info -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <h2><?= lang("visi_misi") ?></h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="card">
                            <div class="body bg-blue-grey">
                                <div class="font-bold font-24 m-b--35"><?= lang("visi") ?></div>
                                <ul class="dashboard-stat-list">
                                    <li>
                                        TODAY
                                        <span class="pull-right"><b>12</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        YESTERDAY
                                        <span class="pull-right"><b>15</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        LAST WEEK
                                        <span class="pull-right"><b>90</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        LAST MONTH
                                        <span class="pull-right"><b>342</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        LAST YEAR
                                        <span class="pull-right"><b>4 225</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        ALL
                                        <span class="pull-right"><b>8 752</b> <small>TICKETS</small></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="card">
                            <div class="body bg-teal">
                                <div class="font-bold font-24 m-b--35"><?= lang("misi") ?></div>
                                <ul class="dashboard-stat-list">
                                    <li>
                                        TODAY
                                        <span class="pull-right"><b>12</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        YESTERDAY
                                        <span class="pull-right"><b>15</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        LAST WEEK
                                        <span class="pull-right"><b>90</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        LAST MONTH
                                        <span class="pull-right"><b>342</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        LAST YEAR
                                        <span class="pull-right"><b>4 225</b> <small>TICKETS</small></span>
                                    </li>
                                    <li>
                                        ALL
                                        <span class="pull-right"><b>8 752</b> <small>TICKETS</small></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <h2><?= lang("data_pengunjung") ?></h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div style="background: #F5F5F5" class="info-box-4 hover-zoom-effect hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons" style="color: #03A9F4">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text"><?= lang("admin") ?></div>
                                <div class="number count-to" data-from="0" data-to="<?= $vis['admin'] ?>" data-speed="15" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div style="background: #F5F5F5" class="info-box-4 hover-zoom-effect hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons" style="color: #03A9F4">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text"><?= lang("plagiarism_checker") ?></div>
                                <div class="number count-to" data-from="0" data-to="<?= $vis['plag'] ?>" data-speed="15" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div style="background: #F5F5F5" class="info-box-4 hover-zoom-effect hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons" style="color: #03A9F4">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text"><?= lang("reviewer") ?></div>
                                <div class="number count-to" data-from="0" data-to="<?= $vis['rev'] ?>" data-speed="15" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div style="background: #F5F5F5" class="info-box-4 hover-zoom-effect hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons" style="color: #03A9F4">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text"><?= lang("author") ?></div>
                                <div class="number count-to" data-from="0" data-to="<?= $vis['auth'] ?>" data-speed="15" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div style="background: #F5F5F5" class="info-box-4 hover-zoom-effect hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons" style="color: #03A9F4">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text"><?= lang("translator") ?></div>
                                <div class="number count-to" data-from="0" data-to="<?= $vis['trans'] ?>" data-speed="15" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div style="background: #F5F5F5" class="info-box-4 hover-zoom-effect hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons" style="color: #03A9F4">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text"><?= lang("dashboard") ?></div>
                                <div class="number count-to" data-from="0" data-to="<?= $vis['dash'] ?>" data-speed="15" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <h2><?= lang("galeri") ?></h2>
            </div>
            <div class="body">
                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                    <?php $i=0; foreach ($gallery as $g) { ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a href="<?= base_url("dist/images/gallery/".$g['url_photo']) ?>" data-sub-html="<?= $g['caption'] ?>">
                                <img class="img-responsive thumbnail" src="<?= base_url("dist/images/gallery/tumb/tumb_".$g['url_photo']) ?>">
                            </a>
                        </div>
                    <?php $i++; } ?>
                    <?php if($i==0){ ?>
                        <div class="text-center">
                            <br><br><h4> Tidak Ada Foto di Galeri ! </h4>
                        </div>
                    <?php } ?>
                </div>

                <nav style="text-align: right">
                    <ul class="pagination">
                        <?php echo $this->pagination->create_links(); ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>


<?php endsection()?>

<?php section('js'); ?>
<!--  -->
<script src="<?= base_url('dist/plugins/jquery-countto/jquery.countTo.js') ?>"></script>
<script src="<?= base_url('dist/plugins/light-gallery/js/lightgallery-all.js') ?>"></script>
<script src="<?= base_url('dist/js/pages/medias/image-gallery.js') ?>"></script>
<script src="<?= base_url('dist/js/pages/index.js') ?>"></script>

<?php endsection(); ?>

<?php section('script'); ?>
<script>
    $("#home").addClass("active");
</script>
<?php endsection(); ?>

<?php getview('layouts/template') ?>