<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Verifikasi Pembimbingan Artikel | TPP UM</title>
    <link rel="icon" type="image/png" href="<?= base_url('dist/images/logo-fix.png') ?>">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url('dist/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url('dist/plugins/node-waves/waves.css') ?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url('dist/plugins/animate-css/animate.css') ?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= base_url('dist/css/style.css') ?>" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Akses<b>TPP UM</b></a>
            <small><?= lang('silahkan_login') ?></small>
        </div>
        <div class="card">
            <div class="body">
                <?php $this->load->view('partials/message') ?>
                <div class="msg align-center">
                    <hr width="100%" style="color: green; height: 4px; background: green">
                    Verifikasi pembimbing artikel dengan judul '<i><?= $subm['tittle'] ?></i>' berhasil !
                    <br><br>
                    Author sudah diperbolehkan untuk melanjutkan ke langkah selanjutnya.
                    <hr width="100%" style="color: green; height: 4px; background: green">
                </div>
                <div class="row m-t-15 m-b--20">
                    <div class="col-xs-12 align-center">
                        <a href="<?= base_url() ?>"><?= lang('login_disini') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?= base_url('dist/plugins/jquery/jquery.min.js') ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url('dist/plugins/bootstrap/js/bootstrap.js') ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url('dist/plugins/node-waves/waves.js') ?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>

    <!-- Custom Js -->
    <script src="<?= base_url('dist/js/admin.js') ?>"></script>
    <script src="<?= base_url('dist/js/pages/examples/sign-in.js') ?>"></script>
</body>

</html>