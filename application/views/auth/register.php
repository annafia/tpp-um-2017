<?php $post = $this->session->flashdata('post') ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Daftar | TPP UM</title>
    <link rel="icon" type="image/png" href="<?= base_url('dist/images/logo-fix.png') ?>">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url('dist/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url('dist/plugins/node-waves/waves.css') ?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url('dist/plugins/animate-css/animate.css') ?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= base_url('dist/css/style.css') ?>" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Akses<b>TPP UM</b></a>
            <small><?= lang('silahkan_login') ?></small>
        </div>
        <div class="card">
            <div class="body">
                <?php $this->load->view('partials/message') ?>
                <form id="sign_up" method="POST">
                    <div class="msg"><?= lang('silahkan_daftar') ?></div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" value="<?= isset($post['full_name'])?$post['full_name']:"" ?>" class="form-control" name="full_name" placeholder="<?= lang('ph_nama_lengkap') ?>" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">picture_in_picture</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" value="<?= isset($post['id_personal'])?$post['id_personal']:"" ?>" class="form-control" name="id_personal" placeholder="<?= lang('ph_nim_nidn') ?>" pattern="[0-9]" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">business_center</i>
                                </span>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <select class="form-control show-tick" name="position" required id="select_status">
                                            <option disabled <?php if(empty($post['position'])) echo "selected"?>>
                                                <?= lang('ph_jabatan') ?>
                                            </option>
                                            <option value="1" 
                                            <?php if(isset($post['position']))
                                                if($post['position']==1) echo "selected";
                                            ?>>
                                                <?= lang('mahasiswa') ?>
                                            </option>
                                            <option value="2" 
                                            <?php if(isset($post['position']))
                                                if($post['position']==2) echo "selected";
                                            ?>>
                                                <?= lang('dosen') ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" value="<?= isset($post['email'])?$post['email']:"" ?>" class="form-control" name="email" placeholder="<?= lang('ph_email') ?>" required>
                                </div>
                                <span class="input-group-addon" id="addon_email">@um.ac.id</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input id="password" type="password" class="form-control" name="password" placeholder="<?= lang('ph_password') ?>" onchange="validatePassword()" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input id="repassword" type="password" class="form-control" name="repassword" placeholder="<?= lang('ph_konfirmasi') ?>" onchange="validatePassword()" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">date_range</i>
                                </span>
                                <div class="form-line">
                                    <input id="birth" type="date" value="<?= isset($post['birth'])?$post['birth']:"" ?>" class="form-control" name="birth" placeholder="<?= lang('ph_birth') ?>" required>
                                </div>
                            </div>
                        </div>
                        
                        <?php 
                            if(isset($post['id_prodi'])){
                                $data['data'] = array(
                                    "id_prodi" => $post['id_prodi'],
                                    "id_jurusan" => $post['id_jurusan'],
                                    "id_fakultas" => $post['id_fakultas']
                                );
                                $this->load->view("partials/select/program_studi_with_pre",$data);
                            }else{
                                $this->load->view("partials/select/program_studi"); 
                            }
                        ?>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">home</i>
                                </span>
                                <div class="form-line">
                                    <textarea name="address" rows="4" class="form-control no-resize" placeholder="<?= lang('ph_addr') ?>"><?= isset($post['address'])?$post['address']:"" ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">wc</i>
                                </span>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <select class="form-control show-tick" name="gender" required>
                                            <option disabled <?php if(empty($post['gender'])) echo "selected"?>>
                                                <?= lang('ph_gender') ?>
                                            </option>
                                            <option value="1"
                                            <?php if(isset($post['gender']))
                                                if($post['gender']==1) echo "selected";
                                            ?>>
                                                <?= lang('ph_pria') ?>
                                            </option>
                                            <option value="2"
                                            <?php if(isset($post['gender']))
                                                if($post['gender']==2) echo "selected";
                                            ?>>
                                                <?= lang('ph_wanita') ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" value="<?= isset($post['phone_number'])?$post['phone_number']:"" ?>" class="form-control" name="phone_number" placeholder="<?= lang('ph_phone') ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <small><?= lang('terms') ?></small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-lg btn-block bg-pink waves-effect" type="submit"><?= lang('daftar') ?></button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-12">
                            <?= lang('punya_akun') ?><a href="./"><?= lang('disini') ?></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?= base_url('dist/plugins/jquery/jquery.min.js') ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url('dist/plugins/bootstrap/js/bootstrap.js') ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url('dist/plugins/node-waves/waves.js') ?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?= base_url('dist/plugins/jquery-validation/jquery.validate.js') ?>"></script>

    <!-- Custom Js -->
    <script src="<?= base_url('dist/js/admin.js') ?>"></script>
    <script src="<?= base_url('dist/js/pages/examples/sign-in.js') ?>"></script>

    <script src="<?= base_url('dist/app/api.js') ?>"></script>

    <script type="text/javascript">
        $().ready(function(){
            $('#sign_up').validate({
                rules: {
                    password: {
                        minlength: 8
                    },
                    repassword: {
                        minlength: 8,
                        equalTo: "#password"
                    }
                },
                messages: {
                    password: {
                        minlength: "<?= lang('pass_char') ?>"
                    },
                    repassword: {
                        minlength: "<?= lang('pass_char') ?>",
                        equalTo: "<?= lang('pass_notmatch') ?>"
                    }
                }
            });
        });

        $('#select_status').change(function(){
            var stt = $('#select_status').val();
            if(stt == 1){
                $('#addon_email').html('@students.um.ac.id');
            }else{
                $('#addon_email').html('@um.ac.id');
            }
        })

        /* Start Prodi Event*/
        $("#fakultas").change(function(){
            var id_fakultas = $("#fakultas").val();
            var ApiParam = {
                "id_fakultas":id_fakultas
            };
            var datajur = CallApi('prodi/get_jurusan', ApiParam);

            $("#jurusan").html(
                '<option disabled="" selected="">-- <?= lang("jurusan") ?> --</option>'
            );
            var jurusan = $("#jurusan").html();

            $.each(datajur, function (index,item) {
                jurusan += '<option value="' + item.id_jurusan + '">' + item.jurusan + '</option>';
            });

            $("#jurusan").html(jurusan);
            
            $("#prodi").html(
                '<option disabled="" selected="">-- <?= lang("prodi") ?> --</option>'
            );
            var prodi = $("#prodi").html();
        });
        $("#jurusan").change(function(){
            var id_jurusan = $("#jurusan").val();
            var ApiParam = {
                "id_jurusan":id_jurusan
            };
            $("#prodi").html(
                '<option disabled="" selected="">-- <?= lang("prodi") ?> --</option>'
            );
            var prodi = $("#prodi").html();

            var dataprod = CallApi('prodi/get_prodi', ApiParam);

            $.each(dataprod, function (index,item) {
                prodi += '<option value="' + item.id_prodi + '">' + item.program_studi + '</option>';
            });
            
            $("#prodi").html(prodi);
        });
        /* End Prodi Event*/
    </script>
</body>

</html>