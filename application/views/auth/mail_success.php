<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LTDC <?= date('Y') ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url('dist/plugins/bootstrap/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('dist/plugins/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('dist/css/AdminLTE.min.css') ?>">
  <!-- Icon -->
  <link rel="icon" type="image/png" href="<?= base_url('dist/img/icon.png') ?>">
</head>
<body class="hold-transition login-page">

<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="<?= base_url('peserta/validasi/login') ?>"><b>YOUR ACCOUNT IS REGISTERED <br> PLEASE CONFIRM YOUR EMAIL</b></a>
  </div>
  <div class="help-block text-center col-md-12">
    <p style="font-size: 150px; border-bottom: 1px solid #000"><i class="fa fa-check"></i></p>
  </div>
  <div class="text-center">
    <a href="<?= base_url('./') ?>">Login to your account</a>
  </div>
  <div class="lockscreen-footer text-center">
    <strong>Copyright &copy; 2017 <a href="http://www.um.ac.id/" target="_blank">Workshop Elektro</a>.</strong> IT Developer.
  </div>
</div>

<!-- jQuery 2.2.0 -->
<script src="<?= base_url('dist/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url('dist/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>

</body>
</html>
