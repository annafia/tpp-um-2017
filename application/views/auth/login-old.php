<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LTDC <?= date('Y') ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url('dist/plugins/bootstrap/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('dist/plugins/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('dist/css/AdminLTE.min.css') ?>">
  <!-- Icon -->
  <link rel="icon" type="image/png" href="<?= base_url('dist/img/icon.png') ?>">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <h1 class="text-center">Sign-In for Admin Line Tracer Design & Contest</h1>
  <div class="login-logo">
    <img class="profile-user-img img-responsive img-circle" src="<?= base_url('dist/img/logo.png') ?>" alt="User profile picture">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <?php $this->load->view('partials/message') ?>
    <?= $this->form->open('welcome/cek_login') ?>
      <div class="form-group has-feedback">
        <?= $this->form->text('email', null, 'required class="form-control" placeholder="Email"') ?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?= $this->form->password('password', null, 'required class="form-control" placeholder="Password"') ?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    <?= $this->form->close() ?>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="<?= base_url('dist/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url('dist/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?= base_url('dist/plugins/iCheck/icheck.min.js') ?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
