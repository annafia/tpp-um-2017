/* function deleteClass() {
            swal({   title: "Menghapus Data",   
            text: "Data Tidak Dapat dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false }, 
            function(isConfirm){ 
              if (isConfirm) {
                window.location.href="./model/proses.php?del_siswa="+id;
                swal("Terhapus!", "Data Anda Telah Berhasil Dihapus.", "success");   
              } else { 
                swal("Dibatalkan", "Data Anda Tidak Dihapus. ", "error");   
              } });
        }
         function deleteEmploye() {
            swal({   title: "Menghapus Data",   
            text: "Data Tidak Dapat dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false }, 
            function(isConfirm){ 
              if (isConfirm) {
                window.location.href="./model/proses.php?del_siswa="+id;
                swal("Terhapus!", "Data Anda Telah Berhasil Dihapus.", "success");   
              } else { 
                swal("Dibatalkan", "Data Anda Tidak Dihapus. ", "error");   
              } });
        }
        function deleteShift() {
            swal({   title: "Menghapus Data",   
            text: "Data Tidak Dapat dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false }, 
            function(isConfirm){ 
              if (isConfirm) {
                window.location.href="./model/proses.php?del_siswa="+id;
                swal("Terhapus!", "Data Anda Telah Berhasil Dihapus.", "success");   
              } else { 
                swal("Dibatalkan", "Data Anda Tidak Dihapus. ", "error");   
              } });
        }
        function deleteSalary() {
            swal({   title: "Menghapus Data",   
            text: "Data Tidak Dapat dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false }, 
            function(isConfirm){ 
              if (isConfirm) {
                window.location.href="./model/proses.php?del_siswa="+id;
                swal("Terhapus!", "Data Anda Telah Berhasil Dihapus.", "success");   
              } else { 
                swal("Dibatalkan", "Data Anda Tidak Dihapus. ", "error");   
              } });
        }
function deletePeriod() {
            swal({   title: "Menghapus Data",   
            text: "Data Tidak Dapat dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false }, 
            function(isConfirm){ 
              if (isConfirm) {
                window.location.href="./model/proses.php?del_siswa="+id;
                swal("Terhapus!", "Data Anda Telah Berhasil Dihapus.", "success");   
              } else { 
                swal("Dibatalkan", "Data Anda Tidak Dihapus. ", "error");   
              } });
        }
         function outEmploye() {
            swal({   title: "Keluarkan Karyawan",   
            text: "Apa anda yakin untuk mengeluarkan karyawan ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Keluarkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false }, 
            function(isConfirm){ 
              if (isConfirm) {
                window.location.href="./model/proses.php?del_siswa="+id;
                swal("Telah dikeluarkan!", "Karyawan telah dikeluarkan.", "success");   
              } else { 
                swal("Dibatalkan", "Karyawan tidak dikeluarkan. ", "error");   
              } });
        }

        function deletePeriodSallary() {
            swal({   title: "Menghapus Data",   
            text: "Data Tidak Dapat dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false }, 
            function(isConfirm){ 
              if (isConfirm) {
                window.location.href="./model/proses.php?del_siswa="+id;
                swal("Terhapus!", "Data Anda Telah Berhasil Dihapus.", "success");   
              } else { 
                swal("Dibatalkan", "Data Anda Tidak Dihapus. ", "error");   
              } });
        }
*/
        function confirmDelete() {
            swal({
                title: "Menghapus Data",   
                text: "Data Tidak Dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus Data!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm){
                if (isConfirm){
                    $("#successMessage").html("<div class='alert alert-danger flat' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> Data Berhasil di Hapus !</div>");
                }
            });
        }